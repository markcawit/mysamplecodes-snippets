// NOTICE!! DO NOT USE ANY OF THIS JAVASCRIPT
// IT'S JUST JUNK FOR OUR DOCS!
// ++++++++++++++++++++++++++++++++++++++++++
/*!
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Creative Commons Attribution 3.0 Unported License. For
 * details, see http://creativecommons.org/licenses/by/3.0/.
 */
// Intended to prevent false-positive bug reports about responsive styling supposedly not working in IE8.
if (window.location.protocol == 'file:')
	alert("We recommend you use Google Chrome to navigate well on Postfor.Us\nSee www.google.com/intl/en/chrome/browser/ to install.")
  //alert("ERROR: Bootstrap's responsive CSS is disabled!\nSee getbootstrap.com/getting-started/#respond-file-proto for details.")
