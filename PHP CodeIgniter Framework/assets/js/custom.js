$(window).load(function(){
	 	var fadeSpd = 150;
	

	$('html').click( function(){
		if($('#user-login-li').hasClass("active"))
	    {
	    	$('#login_nav').click();
	    }
	});

	$('#login_nav, #login-block').click(function(e){
		e.stopPropagation();
	});

	$('#login_nav').click(function(){
	    if($('#user-login-li').hasClass("active"))
	    { 
	        $('#login-block').fadeOut(fadeSpd);
	        $('#user-login-li').removeClass("active");
	    }
	    else
	    {
	        $('#login-block').fadeIn(fadeSpd);
	        $('#user-login-li').addClass("active");
	    }
	});



	jQuery('#registration_form').validate({
	 errorClass: "invalid-pass",
	 rules: {
      field_firstname: {
        required: true,
      },
      field_lastname: {
        required: true,
      },
      field_email: {
        required: true,
        email: true
      },
      field_username: {
        required: true
      },
	  field_password: {
	    required: true,
	    minlength: 6
	  },
	  field_cpassword: {
	    required: true,
	    minlength: 6,
	    equalTo: '#field_password'
	  }
	 },
	 messages: {
       field_firstname: {
         required: "Please enter your first name.",
       },
       field_lastname: {
         required: "Please enter your last name.",
       },
       field_email: {
         required: "Please enter your email address.<br>We'll send you an email for verification!",
         email: "Please enter a valid email address.<br>We'll send you an email for verification!"
       },
       field_username: {
         required: "Please enter your desired username.",
       },
	   field_password: {
	     required: "Please enter your password.",
	     minlength: "Your password must be atleast 6 characters long."
	   },
	   field_cpassword: {
	     required: "Re-type your password for confirmation.",
	     minlength: "Your password must be atleast 6 characters long.",
	     equalTo: "This should match the password."
	   }
	 }
	});


	$( "#subscription").change(function() {
		$('#mon1').hide();
		$('#mon6').hide();
		$('#mon12').hide();

		$('#mon'+$(this).val()).show();
	});


});


$(document).ready(function() {
	$( '#update_accountemail' ).click( function() {
		var email = $( '#new_email' ).val();

    	$( '#errMsg' ).html('Updating your email.. please wait.');
		update_email(email);
	});
});

/*$(document).ready(function() {
    var visitortime = new Date();
    var visitortimezone = "" + -visitortime.getTimezoneOffset()/60;

    $.post(site_url + 'front/get_timezone', {time: visitortimezone},
    function(response)
    {
        if(response.sucess)
        {
            $('#field_username').val(response.timezone)
        }
    }, "json");
});*/

function color_change(id, color)
{
	if(color == 'red')
	{
		$('#'+id).removeClass("text-green");
		$('#'+id).addClass("text-red");
	}
	else
	{
		$('#'+id).removeClass("text-red");
		$('#'+id).addClass("text-green");
	}
}

function check_email(email)
{
    $.post(site_url + 'register/check_email', {email: email},
    function(response)
    {
        if(response.available)
        {
        	color_change('field_email_check', response.color);
        }
    }, "json");
}

function update_email(email)
{
    $.post(site_url + 'client_area/update_email', { email: email },
    function(response)
    {
        if(response.success)
        {
        	$( '#errMsg' ).html(response.msg);
        	$( '#account_email' ).html(response.email+' <i class="glyphicon glyphicon-edit" title="Update your email"></i>');
        }
        else
        {
        	$( '#errMsg' ).html(response.msg);
        }
    }, "json");
}


function paypal_button(id)
{
}