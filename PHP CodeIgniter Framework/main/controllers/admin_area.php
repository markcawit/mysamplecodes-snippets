<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_area extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	function index($response = null)
	{
		$data['page_active'] 	  = 'supp';
		$data['critical_ticket']  = $this->subscribe->opened_tickets(1);
		$data['moderate_ticket']  = $this->subscribe->opened_tickets(0);
		$data['page_icon']		  = '<i class="glyphicon glyphicon-bullhorn"></i> ';
		$data['page_description'] = $page_description = 'Admin Support';
		$data['page_details']	  = 'admin-support';

		$this->layouts->set_title('Support');
		$this->layouts->set_description($page_description);
		$this->layouts->view('admin_area', $data, 'main');
	}

	function ticket($response = null)
	{
		$ticket_id = $response;
		$this->subscribe->submit_ticket_reply($ticket_id);

		$data['ticket'] 		  = $this->subscribe->get_ticket($ticket_id);
		$data['ticket_messages']  = $this->subscribe->get_ticket_messages($ticket_id);
		$data['page_active'] 	  = 'supp';
		$data['page_icon']		  = '<i class="glyphicon glyphicon-bullhorn"></i> ';
		$data['page_description'] = $page_description = 'Support';
		$data['page_details']	  = 'admin-support-ticket';

		$this->layouts->set_title('Support');
		$this->layouts->set_description($page_description);
		$this->layouts->view('admin_area', $data, 'main');
	}

	function close_ticket($response = null)
	{
		$ticket_id = $response;
		$this->subscribe->close_ticket($ticket_id);
		redirect('admin_area');
	}

}