<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_area extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	function index($response = null)
	{
		$data['page_active'] 	  = 'bill';
		$data['page_icon']		  = '<i class="glyphicon glyphicon-usd"></i> ';
		$data['page_description'] = $page_description = 'Billing';
		$data['payments']  		  = $this->billings->payment_history();
		$data['page_details']	  = (!$this->general->admin()) ? 'billing-details' : 'admin-area';

		$this->layouts->set_title('Billing');
		$this->layouts->set_description($page_description);
		$this->layouts->view('client_area', $data, 'main');
	}

	function payment_methods($response = null)
	{
		$data['page_active'] 	  = 'bill';
		$data['page_icon']		  = '<i class="glyphicon glyphicon-usd"></i> ';
		$data['page_description'] = $page_description = 'Payment Methods';
		$data['page_details']	  = 'billing-payment-methods';

		$this->layouts->set_title('Payment Methods');
		$this->layouts->set_description($page_description);
		$this->layouts->view('client_area', $data, 'main');
	}

	function account($response = null)
	{
		$data['acct_info']		  = $this->billings->account_update();
		$data['page_active'] 	  = 'acct';
		$data['page_icon']		  = '<i class="glyphicon glyphicon-cog"></i> ';
		$data['page_description'] = $page_description = 'Account Settings';
		$data['page_details']	  = 'account-details';

		$this->layouts->set_title('Account');
		$this->layouts->set_description($page_description);
		$this->layouts->view('client_area', $data, 'main');
	}

	function support($response = null)
	{
		$data['page_active'] 	  = 'supp';
		$data['otickets']	  	  = $this->subscribe->get_tickets('open');
		$data['ctickets']	  	  = $this->subscribe->get_tickets('close');
		$data['page_icon']		  = '<i class="glyphicon glyphicon-bullhorn"></i> ';
		$data['page_description'] = $page_description = 'Support';
		$data['page_details']	  = 'support';

		$this->layouts->set_title('Support');
		$this->layouts->set_description($page_description);
		$this->layouts->view('client_area', $data, 'main');
	}

	function resend_email_verification()
	{
		$this->subscribe->resend_verification_code();
	}

	function update_email()
	{
		echo $this->subscribe->update_email();
	}

}