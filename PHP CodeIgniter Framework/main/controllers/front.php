<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	public function index($response = null)
	{
		redirect('front/home');
	}

	public function home($response = null)
	{
		$data['page_active'] = 'home';

		$this->layouts->set_title('Auto Post with');
		$this->layouts->set_description('where you can easily manage your campaign post to all your facebook pages & groups in one place. It allows you to schedule posts or advertisements you want to post automatically on all your selected groups and pages.');
		$this->layouts->view('home', $data, 'main');
	}

	public function features($response = null)
	{
		$data['page_active'] = 'feat';

		$this->layouts->set_title('Features');
		$this->layouts->set_description('Features Page');
		$this->layouts->view('features', $data, 'main');
	}

	public function thankyou($response = null)
	{
		$response 				= isset($_POST) ? json_decode( json_encode($_POST) ) : 0;
		$data['response'] 		= $this->billings->update_payment($response);
		$data['page_active'] 	= '';

		$this->layouts->set_title('Thank You');
		$this->layouts->set_description('Thank You page');
		$this->layouts->view('thank-you', $data, 'main');
	}

	public function facebook_login($response = null)
	{
		$data['subscription'] 	= $subscription = 'premium';
		$fbaccess				= $this->faceboook->access('register/plan/'.$subscription.'/facebook_add');
		$data['fbaccess']		= $fbaccess;

		$data['page_active'] = 'fb_login';
		$logged_in = $this->faceboook->facebook_check_registered();

		$this->layouts->set_title('Loging in through Facebook');
		$this->layouts->set_description('Facebook Login Page');
		$this->layouts->view('facebook_login', $data, 'main');
	}

	function not_found($response = null)
	{
		$data['page_active'] = '';

		$this->layouts->set_title('Oops!');
		$this->layouts->set_description('Not found page');
		$this->layouts->view('not-found', $data, 'main');
	}

}
?>