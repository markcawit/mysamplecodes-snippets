<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privacy_policy extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	public function index($response = null)
	{
		$data['page_active'] = 'home';

		$this->layouts->set_title('Privacy Policy');
		$this->layouts->set_description('PostforUS Privacy Policy');
		$this->layouts->view('privacy', $data, 'main');
	}

}