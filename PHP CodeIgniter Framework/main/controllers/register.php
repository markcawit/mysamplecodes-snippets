<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	function index($response = null)
	{
		redirect('register/plan/premium');
	}

	function plan($subscription = 'premium', $process = null, $response = null)
	{
		$data['page_active'] 	= 'regs';
		$data['subscription'] 	= $subscription;
		$data['registered']		= false;

		$fbaccess				= $this->faceboook->access('register/plan/'.$subscription.'/facebook_add');
		$data['fbaccess']		= $fbaccess;

		if($this->general->id_user())
		{
			$this->messages->add('You have already registered and logged in..', 'warning');
			redirect('client_area/account');
		}

		if($process == 'add') #register using registration form
		{
			$reg = $this->subscribe->add_subscriber($this->general->get_subscription_id($subscription));

			if($reg['success'])
			{
				$this->messages->add('<strong>Congratulations!</strong> You have successfully registered.<br>Please check your email for <u>verification of your account</u> <br>and be sure to update your timezone from My Account once you have logged in.', 'success');
				$data['registered']		= $reg['success'];
			}
			else
			{
				$data['registration'] = $reg;
			}
		}
		else if($process == 'facebook_add') #register through facebook
		{
			$reg = $this->subscribe->fb_add_subscriber($this->general->get_subscription_id($subscription), $fbaccess['user_info']);

			if($reg['success'])
			{
				$email_message = ($reg['email_sent_tru'] == 'fb_inbox') ? 'facebook inbox or other message' : 'email';
				$this->messages->add('<strong>Congratulations!</strong> You have successfully registered.<br>Please check your '.$email_message.' for your <u>username</u> and <u>password</u> or you may login through Facebook <br>and be sure to update your timezone from My Account once you have logged in.', 'success');
				$data['registered']		= $reg['success'];
			}
		}

		$this->layouts->set_title('Register');
		$this->layouts->set_description('Registration Page');
		$this->layouts->view('register', $data, 'main');
	}

	function subscribe_success()
	{
		// for the notification of the payment from paypal
	}

	function forgot_password($response = null)
	{
		$data 					= $this->subscribe->password_retrieval();
		$data['page_active'] 	= 'regs';


		$this->layouts->set_title('Forgot Password');
		$this->layouts->set_description('Forgot Password Page');
		$this->layouts->view('forgot_password', $data, 'main');
	}

	function new_password($verification_code, $response = null)
	{
		$data['page_active'] 		= 'regs';
		$data['verification_code'] 	= $verification_code;
		$data['email'] 				= $email = $_GET['email'];
		$data['verify'] 			= $this->subscribe->retrieval_verification($verification_code, $email);


		$this->layouts->set_title('New Password');
		$this->layouts->set_description('New Password Page');
		$this->layouts->view('new_password', $data, 'main');
	}

	function new_password_success()
	{
		$data['page_active'] = 'regs';

		$this->layouts->set_title('Password Retrieved');
		$this->layouts->set_description('Password Retrieved Page');
		$this->layouts->view('new_password_success', $data, 'main');
	}

	function verify_email($verification_code = '', $response = null)
	{
		$data['verification_code'] 		= $verification_code;
		$data['email'] 					= $email = $_GET['email'];
		$data['invalid_verification'] 	= $this->subscribe->email_verification( $verification_code, $email );
		$data['page_active'] = '';

		$this->layouts->set_title('Email Verification');
		$this->layouts->set_description('Email verification Page');
		$this->layouts->view('verification', $data, 'main');
	}

	function check_email()
	{
		$available = $this->general->check_available_email();

		$result['available'] = $available;
		if($available) $result['color'] = 'green';
		else $result['color'] = 'red';

		echo json_encode($result);
	}

}