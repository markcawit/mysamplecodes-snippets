<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	function a($shortlink = '')
	{	
		$this->core->shortlink_redirect($shortlink);
	}

}