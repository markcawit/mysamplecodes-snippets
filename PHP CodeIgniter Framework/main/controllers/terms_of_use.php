<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms_of_use extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
	}

	public function index($response = null)
	{
		$data['page_active'] = 'home';

		$this->layouts->set_title('Terms of Use');
		$this->layouts->set_description('PostforUS Terms of Use');
		$this->layouts->view('terms', $data, 'main');
	}

}