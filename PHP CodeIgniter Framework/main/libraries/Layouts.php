<?php
class Layouts
{
	private $CI;
	private $layout_title = NULL;
	private $layout_description = NULL;
	private $layout_right = NULL;
	private $layout_breadcrumb = NULL;
	private $layout_header = 'includes/header';
	private $includes = array();
	
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	public function view($view_name, $params = array(), $layout)
	{		
		$this->CI->load->model('faceboook');
		$fblogin = $this->CI->faceboook->access('front/facebook_login');

		//Get the logged in user if we have one.
		/*$this->CI->load->model('user_model');
		$my = $this->CI->user_model->get_user();
		$params['my'] = $my;*/
		
		//This will allow us to inject system messages into the main template by calling the variable $messages in the template!
		$rendered_view = $this->CI->load->view($view_name, $params, true);
		
		$this->CI->load->library('messages');
		$messages = $this->CI->messages->get();
		
		$data_array = array(
			'layout_content' => $rendered_view,
			'layout_title' => $this->layout_title,
			'layout_description' => $this->layout_description,
			'layout_header' => $this->layout_header,
			'layout_breadcrumb' => $this->layout_breadcrumb,
			'messages' => $messages,
			'fblogin' => $fblogin/*,
			'my' => $my*/
		);
		$this->CI->load->view('layouts/'.$layout, $data_array);		
	}
	public function set_title($title)
	{
		$this->layout_title = $title;
	}
	public function set_description($description)
	{
		$this->layout_description = $description;
	}
	public function set_header($header) 
	{
		$this->layout_header = $header;
	}
	public function set_breadcrumb($link, $name, $placement)
	{
		$this->layout_breadcrumb[$placement] = array('link' => $link, 'name' => $name);
	}
	public function add_include($path, $prepend_base_url = true)
	{
		if($prepend_base_url)
		{
			$this->CI->load->helper('url'); // Just in case!
			$this->includes[] = array(
				'path' => base_url() . $path,
				'prepend_base_url' => $prepend_base_url
			); 
		}
		else
		{
			$this->includes[] = array(
				'path' => $path,
				'prepend_base_url' => $prepend_base_url
			);
		}
		
		return $this; // $this->layouts->add_include('blabla')->add_include('blablabla');
	}
	public function print_includes()
	{
		$final_includes = '';
		
		foreach($this->includes as $include)
		{
			if($include['prepend_base_url'])
			{
				if(preg_match('/js$/', $include['path']))
				{
					$final_includes .= '<script src="' . $include['path'] . '?version='. universal_version() .'"></script>';
				}
				elseif (preg_match('/css$/', $include['path']))
				{
					$final_includes .= '<link rel="stylesheet" href="' . $include['path'] . '?version='. universal_version() .'" />';
				}
			}
			else
			{
				$final_includes .= '<script src="' . $include['path'] . '"></script>';	
			}
		}
		return $final_includes;
	}

}