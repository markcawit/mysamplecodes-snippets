<?php
class Billings extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function account_update()
    {
    	$data['id_user'] = $id_user = $this->general->id_user();
		$field_firstname = $field_lastname = $field_address = $field_url = "";
		$field_company = $field_title = $field_industry = $field_phone = $email = "";

        if($this->input->post('update_info') == "submit")
        {
            $field_firstname= $this->security->sanitize_filename($this->input->post('field_firstname'), TRUE);
            $field_lastname	= $this->security->sanitize_filename($this->input->post('field_lastname'), TRUE);
            $field_address	= $this->security->sanitize_filename($this->input->post('field_address'), TRUE);
            $field_url		= $this->security->sanitize_filename($this->input->post('field_url'), TRUE);
            $field_company	= $this->security->sanitize_filename($this->input->post('field_company'), TRUE);
            $field_title	= $this->security->sanitize_filename($this->input->post('field_title'), TRUE);
            $field_industry	= $this->security->sanitize_filename($this->input->post('field_industry'), TRUE);
            $field_phone	= $this->security->sanitize_filename($this->input->post('field_phone'), TRUE);

            $this->db->query("UPDATE `subscribers` SET `firstname`='".$field_firstname."', `lastname`='".$field_lastname."', `address`='".$field_address."', `url`='".$field_url."', `company_name`='".$field_company."', `title`='".$field_title."', `industry`='".$field_industry."', `phone`='".$field_phone."' WHERE `id`='".$id_user."'");
        }

        $current_time_timestamp     = $this->current_timestamp();
        $accts = $this->db->query("SELECT firstname, lastname, address, url, company_name, title, industry, phone, email, subscription_status, registered_timestamp, expire_timestamp, verification_code FROM subscribers WHERE id='".$id_user."' LIMIT 1");

        $data['field_firstname']	= $accts->row()->firstname;
        $data['field_lastname']		= $accts->row()->lastname;
        $data['field_address']		= $accts->row()->address;
        $data['field_url']			= $accts->row()->url;
		$data['field_company']		= $accts->row()->company_name;
		$data['field_title']		= $accts->row()->title;
		$data['field_industry']		= $accts->row()->industry;
		$data['field_phone']		= $accts->row()->phone;
		$data['email']				= $accts->row()->email;

		$data['subscription_status']	= $accts->row()->subscription_status;
		$data['registered_timestamp']	= $accts->row()->registered_timestamp;
        $data['expire_timestamp']       = $accts->row()->expire_timestamp;
        $data['expired']                = (($accts->row()->expire_timestamp - $current_time_timestamp) > 0) ? false : true;
		$data['verification_code']		= $accts->row()->verification_code;

    	return $data;
    }

    function payment_history()
    {
        $payments = $this->db->query("SELECT * FROM subscribers_payments WHERE user_id='".$this->general->id_user()."'");
        return $payments;
    }

    function update_payment($response)
    {
        if( count($response) )
        {
            $user_id        = url_decode($response->custom);
            $item_id        = $response->item_number;
            $description    = $response->item_name;
            $amount         = $response->payment_gross;
            $paypal_fee     = $response->payment_fee;
            $txn_id         = $response->txn_id;
            $payment_email  = $response->payer_email;

            $payment_date   = $this->general->current_time();

            $current_expiration = $this->general->info( 'expire_timestamp','subscribers' );
            $with_remaining = ( ( $current_expiration - $payment_date ) > 0 ) ?1 :0;

            $remaining        = ($with_remaining) ?$this->general->time_difference( $current_expiration, $payment_date )->days :0;

            if($with_remaining)
                $new_expiration = date( 'Y-m-d H:i:s', strtotime( "+".$item_id." month", $current_expiration ) );
            else
                $new_expiration = date( 'Y-m-d H:i:s', strtotime( "+".$item_id." month" ) );

            $expiry_timestamp = strtotime( $new_expiration );

            $this->db->query("INSERT INTO subscribers_payments(user_id,item_id,description,amount,paypal_fee,txn_id,payment_email,payment_date,remaining,expiry_timestamp) 
                            VALUES('".$user_id."', '".$item_id."', '".$description."', '".$amount."', '".$paypal_fee."', '".$txn_id."', '".$payment_email."', '".$payment_date."', '".$remaining."', '".$expiry_timestamp."')");
            $insert_id = $this->db->insert_id();
            $this->db->query("UPDATE subscribers_payments SET response='".str_replace ( "'", "&#39;", json_encode( $response ) )."' WHERE `id`='".$insert_id."'");

            $this->db->query("UPDATE `subscribers` SET `expire_timestamp`='".$expiry_timestamp."' WHERE `id`='".$user_id."'");

            $this->email_receipt($response);
        }

        return $response;
    }

    function email_receipt()
    {
    }

    function current_timestamp()
    {
        $qusers = $this->db->query("SELECT gmt FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
        $gmt    = ($qusers->num_rows() > 0)? $qusers->row()->gmt:0;

        return strtotime(gmdate("M d Y H:i:s", time())) + ($gmt * 60 * 60);
    }

}
?>