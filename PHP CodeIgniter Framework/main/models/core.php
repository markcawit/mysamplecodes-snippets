<?php

class Core extends CI_Model {

    var $timezones = array(
        1   => '(GMT-12:00) International Date Line West',
        2   => '(GMT-11:00) Midway Island',
        3   => '(GMT-11:00) Samoa',
        4   => '(GMT-10:00) Hawaii',
        5   => '(GMT-09:00) Alaska',
        6   => '(GMT-08:00) Pacific Time (US &amp; Canada); Tijuana',
        7   => '(GMT-07:00) Arizona',
        8   => '(GMT-07:00) Chihuahua',
        9   => '(GMT-07:00) La Paz',
        10  => '(GMT-07:00) Mazatlan',
        11  => '(GMT-07:00) Mountain Time (US &amp; Canada)',
        12  => '(GMT-06:00) Central America',
        13  => '(GMT-06:00) Central Time (US &amp; Canada)',
        14  => '(GMT-06:00) Guadalajara',
        15  => '(GMT-06:00) Mexico City',
        16  => '(GMT-06:00) Monterrey',
        17  => '(GMT-06:00) Saskatchewan',
        18  => '(GMT-05:00) Bogota',
        19  => '(GMT-05:00) Eastern Time (US &amp; Canada)',
        20  => '(GMT-05:00) Indiana (East)',
        21  => '(GMT-05:00) Lima',
        22  => '(GMT-05:00) Quito',
        23  => '(GMT-04:00) Atlantic Time (Canada)',
        24  => '(GMT-04:00) Caracas',
        25  => '(GMT-04:00) La Paz',
        26  => '(GMT-04:00) Santiago',
        27  => '(GMT-03:30) Newfoundland',
        28  => '(GMT-03:00) Brasilia',
        29  => '(GMT-03:00) Buenos Aires',
        30  => '(GMT-03:00) Georgetown',
        31  => '(GMT-03:00) Greenland',
        32  => '(GMT-02:00) Mid-Atlantic',
        33  => '(GMT-01:00) Azores',
        34  => '(GMT-01:00) Cape Verde Is.',
        35  => '(GMT) Casablanca',
        36  => '(GMT) Edinburgh',
        37  => '(GMT) Greenwich Mean Time : Dublin',
        38  => '(GMT) Lisbon',
        39  => '(GMT) London',
        40  => '(GMT) Monrovia',
        41  => '(GMT+01:00) Amsterdam',
        42  => '(GMT+01:00) Belgrade',
        43  => '(GMT+01:00) Berlin',
        44  => '(GMT+01:00) Bern',
        45  => '(GMT+01:00) Bratislava',
        46  => '(GMT+01:00) Brussels',
        47  => '(GMT+01:00) Budapest',
        48  => '(GMT+01:00) Copenhagen',
        49  => '(GMT+01:00) Ljubljana',
        50  => '(GMT+01:00) Madrid',
        51  => '(GMT+01:00) Paris',
        52  => '(GMT+01:00) Prague',
        53  => '(GMT+01:00) Rome',
        54  => '(GMT+01:00) Sarajevo',
        55  => '(GMT+01:00) Skopje',
        56  => '(GMT+01:00) Stockholm',
        57  => '(GMT+01:00) Vienna',
        58  => '(GMT+01:00) Warsaw',
        59  => '(GMT+01:00) West Central Africa',
        60  => '(GMT+01:00) Zagreb',
        61  => '(GMT+02:00) Athens',
        62  => '(GMT+02:00) Bucharest',
        63  => '(GMT+02:00) Cairo',
        64  => '(GMT+02:00) Harare',
        65  => '(GMT+02:00) Helsinki',
        66  => '(GMT+02:00) Istanbul',
        67  => '(GMT+02:00) Jerusalem',
        68  => '(GMT+02:00) Kyiv',
        69  => '(GMT+02:00) Minsk',
        70  => '(GMT+02:00) Pretoria',
        71  => '(GMT+02:00) Riga',
        72  => '(GMT+02:00) Sofia',
        73  => '(GMT+02:00) Tallinn',
        74  => '(GMT+02:00) Vilnius',
        75  => '(GMT+03:00) Baghdad',
        76  => '(GMT+03:00) Kuwait',
        77  => '(GMT+03:00) Moscow',
        78  => '(GMT+03:00) Nairobi',
        79  => '(GMT+03:00) Riyadh',
        80  => '(GMT+03:00) St. Petersburg',
        81  => '(GMT+03:00) Volgograd',
        82  => '(GMT+03:30) Tehran',
        83  => '(GMT+04:00) Abu Dhabi',
        84  => '(GMT+04:00) Baku',
        85  => '(GMT+04:00) Muscat',
        86  => '(GMT+04:00) Tbilisi',
        87  => '(GMT+04:00) Yerevan',
        88  => '(GMT+04:30) Kabul',
        89  => '(GMT+05:00) Ekaterinburg',
        90  => '(GMT+05:00) Islamabad',
        91  => '(GMT+05:00) Karachi',
        92  => '(GMT+05:00) Tashkent',
        93  => '(GMT+05:30) Chennai',
        94  => '(GMT+05:30) Kolkata',
        95  => '(GMT+05:30) Mumbai',
        96  => '(GMT+05:30) New Delhi',
        97  => '(GMT+05:45) Kathmandu',
        98  => '(GMT+06:00) Almaty',
        99  => '(GMT+06:00) Astana',
        100 => '(GMT+06:00) Dhaka',
        101 => '(GMT+06:00) Novosibirsk',
        102 => '(GMT+06:00) Sri Jayawardenepura',
        103 => '(GMT+06:30) Rangoon',
        104 => '(GMT+07:00) Bangkok',
        105 => '(GMT+07:00) Hanoi',
        106 => '(GMT+07:00) Jakarta',
        107 => '(GMT+07:00) Krasnoyarsk',
        108 => '(GMT+08:00) Beijing',
        109 => '(GMT+08:00) Chongqing',
        110 => '(GMT+08:00) Hong Kong',
        111 => '(GMT+08:00) Irkutsk',
        112 => '(GMT+08:00) Kuala Lumpur',
        113 => '(GMT+08:00) Perth',
        114 => '(GMT+08:00) Singapore',
        115 => '(GMT+08:00) Taipei',
        116 => '(GMT+08:00) Ulaan Bataar',
        117 => '(GMT+08:00) Urumqi',
        118 => '(GMT+09:00) Osaka',
        119 => '(GMT+09:00) Sapporo',
        120 => '(GMT+09:00) Seoul',
        121 => '(GMT+09:00) Tokyo',
        122 => '(GMT+09:00) Yakutsk',
        123 => '(GMT+09:30) Adelaide',
        124 => '(GMT+09:30) Darwin',
        125 => '(GMT+10:00) Brisbane',
        126 => '(GMT+10:00) Canberra',
        127 => '(GMT+10:00) Guam',
        128 => '(GMT+10:00) Hobart',
        129 => '(GMT+10:00) Melbourne',
        130 => '(GMT+10:00) Port Moresby',
        131 => '(GMT+10:00) Sydney',
        132 => '(GMT+10:00) Vladivostok',
        133 => '(GMT+11:00) Magadan',
        134 => '(GMT+11:00) New Caledonia',
        135 => '(GMT+11:00) Solomon Is.',
        136 => '(GMT+12:00) Auckland',
        137 => '(GMT+12:00) Fiji',
        138 => '(GMT+12:00) Kamchatka',
        139 => '(GMT+12:00) Marshall Is.',
        140 => '(GMT+12:00) Wellington',
        141 => '(GMT+13:00) Nuku-alofa'
    );

    var $zones = array(
        1   => -12,
        2   => -11,
        3   => -11,
        4   => -10,
        5   => -9,
        6   => -8,
        7   => -7,
        8   => -7,
        9   => -7,
        10  => -7,
        11  => -7,
        12  => -6,
        13  => -6,
        14  => -6,
        15  => -6,
        16  => -6,
        17  => -6,
        18  => -5,
        19  => -5,
        20  => -5,
        21  => -5,
        22  => -5,
        23  => -4,
        24  => -4,
        25  => -4,
        26  => -4,
        27  => -3.3,
        28  => -3,
        29  => -3,
        30  => -3,
        31  => -3,
        32  => -2,
        33  => -1,
        34  => -1,
        35  => 0,
        36  => 0,
        37  => 0,
        38  => 0,
        39  => 0,
        40  => 0,
        41  => 1,
        42  => 1,
        43  => 1,
        44  => 1,
        45  => 1,
        46  => 1,
        47  => 1,
        48  => 1,
        49  => 1,
        50  => 1,
        51  => 1,
        52  => 1,
        53  => 1,
        54  => 1,
        55  => 1,
        56  => 1,
        57  => 1,
        58  => 1,
        59  => 1,
        60  => 1,
        61  => 2,
        62  => 2,
        63  => 2,
        64  => 2,
        65  => 2,
        66  => 2,
        67  => 2,
        68  => 2,
        69  => 2,
        70  => 2,
        71  => 2,
        72  => 2,
        73  => 2,
        74  => 2,
        75  => 3,
        76  => 3,
        77  => 3,
        78  => 3,
        79  => 3,
        80  => 3,
        81  => 3,
        82  => 3.3,
        83  => 4,
        84  => 4,
        85  => 4,
        86  => 4,
        87  => 4,
        88  => 4.3,
        89  => 5,
        90  => 5,
        91  => 5,
        92  => 5,
        93  => 5.3,
        94  => 5.3,
        95  => 5.3,
        96  => 5.3,
        97  => 5.45,
        98  => 6,
        99  => 6,
        100 => 6,
        101 => 6,
        102 => 6,
        103 => 6.3,
        104 => 7,
        105 => 7,
        106 => 7,
        107 => 7,
        108 => 8,
        109 => 8,
        110 => 8,
        111 => 8,
        112 => 8,
        113 => 8,
        114 => 8,
        115 => 8,
        116 => 8,
        117 => 8,
        118 => 9,
        119 => 9,
        120 => 9,
        121 => 9,
        122 => 9,
        123 => 9.3,
        124 => 9.3,
        125 => 10,
        126 => 10,
        127 => 10,
        128 => 10,
        129 => 10,
        130 => 10,
        131 => 10,
        132 => 10,
        133 => 11,
        134 => 11,
        135 => 11,
        136 => 12,
        137 => 12,
        138 => 12,
        139 => 12,
        140 => 12,
        141 => 13
    );

    function __construct()
    {
        parent::__construct();
    }

    function timestamp($gmt = 0)
    {
        if(empty($gmt))
        {
            $qusers = $this->db->query("SELECT gmt FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
            $gmt    = ($qusers->num_rows() > 0)? $qusers->row()->gmt:0;
        }

        return strtotime(gmdate("M d Y H:i:s", time())) + ($gmt * 60 * 60);
    }

    function shortlink_redirect($shortlink)
    {
        $qlink = $this->db->query("SELECT id_cronjob FROM short_link WHERE short_link='".$shortlink."'");

        if($qlink->num_rows() > 0)
        {
            $this->redirect(url_encode($qlink->row()->id_cronjob));
        }
        else redirect('s');
    }

    function redirect($id)
    {
        $qjobs = $this->db->query("SELECT posts.link, cronjobs.id FROM posts, cronjobs WHERE posts.id=cronjobs.id_post AND posts.status='1' AND cronjobs.status='1' AND cronjobs.id='".( (isset($id) && !empty($id))? (int)url_decode($id):0 )."'");

        if($qjobs->num_rows() > 0)
        {
            foreach($qjobs->result() as $post)
            {
                if(!empty($post->link))
                {
                    $this->db->query("UPDATE cronjobs SET clicks=clicks+1 WHERE id='".$post->id."'");
                    header('Location: '.$post->link);
                }
            }
        }
    }

    function rand_numbers($length = 15){
        $rCode = '';
        
        for($i=1;$i<=$length;$i++){
            $oneLetter = rand(1,2);
            if($oneLetter == 1){        //if $oneLetter is equal to 1, fetch a letter
                $letters = 'abcdeghijkmnoqrstuvwxyzABDFGKMNPQRSTUVWXYZ';
                $x = rand(1,strlen($letters)); 
        
                $aChar = substr($letters,$x,1);     //fetch selected letter element from $letters
                $rCode .= $aChar;
            } else {        //if $oneLetter not equal to 1, fetch a number
                $nChar = rand(0,9);     //get random number from 0-9
                $rCode .= $nChar;
            }
        }
        
        return $rCode;
    }

}

?>