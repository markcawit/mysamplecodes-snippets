<?php

class Faceboook extends CI_Model {

    var $app_id       = '';
    var $app_secret   = '';
    var $pages_limit  = '';
    var $groups_limit = '';
    var $retry_limit  = '';
    var $post_admin   = '';

    function __construct()
    {
        parent::__construct();
        $this->settings();
    }

    function settings()
    {
        $qsettings = $this->db->query("SELECT app_id, app_secret, pages_limit, groups_limit, retry_limit, post_admin FROM settings WHERE id='1' LIMIT 1");
        if($qsettings->num_rows() > 0)
        {
            $this->app_id       = $qsettings->row()->app_id;
            $this->app_secret   = $qsettings->row()->app_secret;
            $this->pages_limit  = $qsettings->row()->pages_limit;
            $this->groups_limit = $qsettings->row()->groups_limit;
            $this->retry_limit  = $qsettings->row()->retry_limit;
            $this->post_admin   = $qsettings->row()->post_admin;
        }
    }

    function token_time($access_token, $time)
    {
        if(function_exists('file_get_contents'))
        {
            $access_token_info = file_get_contents('https://graph.facebook.com/oauth/access_token_info?client_id='.$this->app_id.'&access_token='.$access_token);
            $access_token_info = json_decode($access_token_info);

            if(isset($access_token_info->expires_in))
            {
                return timespan($time - $access_token_info->expires_in, $time);
            }
                else
            {
                return 'Could not get token expire time.';
            }
        }
            else
        {
            return 'The function file_get_contents is disabled check your php settings.';
        }
    }

    function access($redirect_url = 'app/dashboard')
    {
        $list = array();
        $user_profile = $loginUrl = $logoutUrl = $batch_response = '';

        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        $user     = $facebook->getUser();

        if($user)
        {
            try
            {
                $user_profile = $facebook->api('/me');
            }
                catch(FacebookApiException $e)
            {
                error_log($e);
                $user = NULL;
            }
        }

        /*if($user)
        {
            $logoutUrl = $facebook->getLogoutUrl();
        }
            else
        {*/
            $loginUrl = $facebook->getLoginUrl(array('scope' => 'publish_stream, email, user_groups, user_likes, manage_pages, photo_upload', 'redirect_uri' => site_url($redirect_url)));
        /*}*/

        /*if($user)
        {
            $fb_user_id = $this->general->info('fb_user_id');
            if(!empty($fb_user_id) && $fb_user_id != $user) 
            {
                $facebook->destroySession();
                redirect($redirect_url . '/error_fb_user_id');
            }

            $batch_response = $this->batch_request($facebook, $user);
            $user_info      = $batch_response['user_info'];
            $friends_list   = $batch_response['friends_list'];
            $groups         = $batch_response['groups'];
            $pages          = $batch_response['pages'];
        }*/

        $list['user']           = $user;
        $list['user_info']      = $user_profile;
        $list['loginUrl']       = $loginUrl;
        $list['logoutUrl']      = $logoutUrl;
        /*$list['batch_response'] = $batch_response;*/

        return $list;
    }

    function access_lite()
    {
        $list = array();
        $loginUrl = $logoutUrl = $user_profile = '';

        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        $user     = $facebook->getUser();

        if($user)
        {
            try
            {
                $user_profile = $facebook->api('/me');
            }
                catch(FacebookApiException $e)
            {
                error_log($e);
                $user = NULL;
            }
        }

        $list['facebook']     = $facebook;
        $list['user']         = $user;
        $list['user_profile'] = $user_profile;

        return $list;
    }

    function access_init()
    {
        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        return $facebook;
    }

    function logout()
    {
        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        $facebook->destroySession();
        redirect('dashboard');
    }

    function batch_request($facebook, $user)
    {
        $queries = array(
            array('method' => 'GET', 'relative_url' => '/'.$user),
            //array('method' => 'GET', 'relative_url' => '/'.$user.'/friends?limit='.$limit),
            array('method' => 'GET', 'relative_url' => '/'.$user.'/groups?limit='.$this->groups_limit),
            array('method' => 'GET', 'relative_url' => '/'.$user.'/likes?limit='.$this->pages_limit),
        );

        try
        {
            $batch_response = $facebook->api('?batch='.json_encode($queries), 'POST');
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $list                 = array();
        $list['user_info']    = json_decode($batch_response[0]['body'], TRUE);
        $list['friends_list'] = array(); //json_decode($batch_response[1]['body'], TRUE);
        $list['groups']       = json_decode($batch_response[1]['body'], TRUE);
        $list['pages']        = json_decode($batch_response[2]['body'], TRUE);

        return $list;
    }

    function facebook_account_request($fb_id, $field = 'name')
    {
        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook   = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        $user_info  = $facebook->api('/'.$fb_id);
        /*
        echo "<pre>";
        print_r($user_info);
        echo "</pre>";
        */
        if($field == 'all')
        {
            return $user_info;
        }
        else
        {
            return $user_info[$field];
        }

        //return json_decode(file_get_contents('http://graph.facebook.com/'.$fb_id))->$field;
    }

    function facebook_check_registered()
    {
        $list = array();
        $registered = false;
        $user_profile = null;

        try
        {
            include_once "app/content/src/facebook.php";
        }
            catch(Exception $e)
        {
            error_log($e);
        }

        $facebook = new Facebook(array('appId' => $this->app_id, 'secret' => $this->app_secret));
        $user     = $facebook->getUser();

        if($user)
        {
            try
            {
                $user_profile = $facebook->api('/me');
                $quser = $this->db->query("SELECT id, username, password FROM users WHERE fb_user_id='".$user."' AND username='".$user_profile['username']."'");
                if($quser->num_rows() > 0)
                {
                    $username  = $quser->row()->username;
                    $password  = $quser->row()->password;

                    redirect('app/login/auth/app/'.$username.'/'.$password);
                    $registered = true;
                }
                else
                {
                    $this->messages->add('<strong>Oops!</strong> Sorry you\'re not a registered user.<br>Please register first before you can login.', 'danger');
                }
            }
                catch(FacebookApiException $e)
            {
                error_log($e);
                $user = NULL;
            }
        }

        return $registered;
    }

}

?>