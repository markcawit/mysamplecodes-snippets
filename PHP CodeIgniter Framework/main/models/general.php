<?php
class General extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function id_user()
    {
        return $this->encrypt->decode($this->session->userdata('username'));
    }

    function id_subscription()
    {
        return $this->encrypt->decode($this->session->userdata('subscription'));
    }

    function admin()
    {
        $qusers = $this->db->query("SELECT access FROM users WHERE id='".$this->id_user()."' LIMIT 1");
        return ($qusers->num_rows() > 0)? $qusers->row()->access:0;
    }

    function support()
    {
        if($this->admin()) return true;
        else return false;
    }

    function info($field = 'email',$table = 'users',$cond = 'id',$cond_value = 0)
    {
        $x = ($cond_value == 0) ? $this->id_user() : $cond_value;
        
        $qusers = $this->db->query("SELECT ".$field." FROM ".$table." WHERE ".$cond."='".$x."' LIMIT 1");
        return ($qusers->num_rows() > 0)? $qusers->row()->$field:0;
    }

    function subscription_info($field, $user_subs_id = false)
    {
        $id_subscription = ($user_subs_id) ? $user_subs_id : $this->id_subscription();
        $qsubs = $this->db->query("SELECT `".$field."` FROM subscription WHERE id='".$id_subscription."' LIMIT 1");
        return ($qsubs->num_rows() > 0)? $qsubs->row()->$field:0;
    }

    function get_subscription_id($subscription, $field = 'id')
    {
        $qsubs = $this->db->query("SELECT `".$field."` FROM subscription WHERE `option`='".$subscription."'");
        return ($qsubs->num_rows() > 0)? $qsubs->row()->$field:0;
    }

    function check_user()
    {
        $qusers = $this->db->query("SELECT name FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
        if($qusers->num_rows() == 0)
        {
            $this->session->sess_destroy();
            redirect();
        }
    }


    function xss_post($value)
    {
        $value = preg_replace('#\s{2,}#',' ',$value);
        $value = str_replace("\t"," ", $value);
        $value = strip_tags($value);
        $value = str_replace('"', "&#34;", $value);
        $value = str_replace("'", "&#39;", $value);
        $value = trim($value);

        return $value;
    }

    function xss_post2($inp)
    {
        if(is_array($inp)) return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp))
        {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

    function xss_post_3($value)
    {
        $value = str_replace('"', "&#34;", $value);
        $value = str_replace("'", "&#39;", $value);
        $value = trim($value);
    }

    function strlenchar($input, $limit)
    {
        return (strlen($input) > $limit && $limit > 0)? substr($input, 0, $limit)."...":$input;
    }

    function access($redirect_url = '')
    {
        $qusers = $this->db->query("SELECT name FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
        if($qusers->num_rows() == 0)
        {
            $this->session->sess_destroy();
            redirect();
        }
            else
        {
            $data['utilizator'] = $qusers->row()->name;
        }

        //$data['fbaccess']      = $this->faceboook->access($redirect_url);
        $data['check_admin']   = $this->general->admin();

        return $data;
    }

    function get_username()
    {
        $qusers = $this->db->query("SELECT name FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
        if($qusers->num_rows() == 1)
        {
            return $qusers->row()->name;
        }
    }

    function gravatar($id_user)
    {
        $quser = $this->db->query("SELECT email FROM users WHERE id='".$id_user."' LIMIT 1");
        if($quser->num_rows() == 1)
        {
            $email_hash = md5(strtolower(trim($quser->row()->email)));
            return 'http://www.gravatar.com/avatar/'.$email_hash;
        }
    }

    function usertime($timestamp, $gmt = 0)
    {
        if(empty($gmt))
        {
            $qusers = $this->db->query("SELECT gmt FROM users WHERE id='".$this->general->id_user()."' LIMIT 1");
            $gmt    = ($qusers->num_rows() > 0)? $qusers->row()->gmt:0;
        }

        return strtotime(gmdate("M d Y H:i:s", $timestamp)) + ($gmt * 60 * 60);
    }

    function check_available_email()
    {
        $available = true;
        $email = $this->security->sanitize_filename($this->input->post('email'), TRUE);
        
        $qusername = $this->db->query("SELECT id FROM users WHERE email='".$email."'");
        if($qusername->num_rows() > 0)
        {
            $available = false;
        }
        return $available;
    }

    function _date_format($timestamp, $i=1)
    {
        switch ($i) {
            case 1:
                return date('m/d/Y', $timestamp);
                break;

            case 2:
                return date('m/d/Y G:i:s', $timestamp);
                break;
            
            default:
                # code...
                break;
        }
    }

    function _time_format($timestamp, $i=12, $sec = true)
    {
        $seconds = $sec ? ':s' : '';

        switch ($i) {
            case 12:
                return date('h:i'.$seconds.' A', $timestamp);
                break;

            case 24:
                return date('G:i'.$seconds, $timestamp);
                break;
            
            default:
                # code...
                break;
        }
    }

    function current_time()
    {
        //return strtotime( date( "Y-m-d H:i:s" ) );
        return strtotime(gmdate("M d Y H:i:s", time())) + (0 * 60 * 60);
    }

    function add_leading_zero($val)
    {
        if($val < 10) $val = '0'.$val;

        return $val;
    }

    function time_difference($timestamp1, $timestamp2) 
    {
        $all = round( ( $timestamp1 - $timestamp2 ) / 60 );
        $d = floor ( $all / 1440 );
        $h = floor ( ( $all - $d * 1440 ) / 60 );
        $m = $all - ( $d * 1440 ) - ( $h * 60 );

        $h = ( $d * 24 ) + $h;

        $diff = array(
            'days'  => $d,
            'hours' => $this->add_leading_zero( $h ), 
            'minutes'   => $this->add_leading_zero( $m )
            );

        return json_decode( json_encode( $diff ) );
    }

}
?>