<?php

class Subscribe extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function add_subscriber($subscription = null)
    {
        $done       = 0;
        $error      = 0;
        $errors     = $errors_list = array();

        if($this->input->post('register') == 'submit')
        {            

            $firstname  = $this->security->sanitize_filename($this->input->post('field_firstname'), TRUE);
            $lastname   = $this->security->sanitize_filename($this->input->post('field_lastname'), TRUE);

            $name       = $firstname . ' ' . $lastname;
            $email      = $this->security->sanitize_filename($this->input->post('field_email'), TRUE);
            $admin      = 0;
            $username   = $this->security->sanitize_filename($this->input->post('field_username'), TRUE);
            $password   = $this->input->post('field_password', true);
            $cpassword  = $this->input->post('field_cpassword', true);
            $fb_user_id = 1;

            #setting up expiry date and time
            $registered_timestamp   = $this->core->timestamp();
            $start_timestamp        = $registered_timestamp;
            $expire_timestamp       = strtotime(date('Y-m-d H:i:s', $start_timestamp) . "+1 month");

            $gmt        = 8;
            $gmt_zone   = 114;

            $fields = array('field_firstname' => $firstname, 'field_lastname' => $lastname, 'name' => $name, 'field_email' => $email, 'field_username' => $username, 'field_password' => $password, 'field_cpassword' => $cpassword);
            foreach($fields as $key => $value)
            {
                if($value == "")
                {
                    $error        = 1;
                    $errors[$key] = 1;
                }
            }

            if(!empty($password) && !empty($cpassword) && $password != $cpassword)
            {
                $error = 1;
                $errors['field_password'] = $errors['field_cpassword'] = 1;
                $errors_list[] = 'The confirmation password does not match the password.';
            }

            if(empty($name) || empty($email) || empty($username) || empty($password) || empty($cpassword))
            {
                $errors_list[]  = 'All fields are required.';
                $error          = 1;
            }
            else if(!empty($username))
            {
                $qusers = $this->db->query("SELECT id FROM users WHERE username='".$username."'");
                if($qusers->num_rows() > 0)
                {
                    $errors['field_username']   = 1;
                    $errors_list[]              = 'The username is already in use.';
                    $error                      = 1;
                }
            }
            
            if(!empty($email))
            {
                $quser_email = $this->db->query("SELECT id FROM users WHERE email='".$email."'");
                if($quser_email->num_rows() > 0)
                {
                    $errors['field_email']  = 1;
                    $errors_list[]          = 'The email was already registered.';
                    $error                  = 1;
                }            
            }

            if(empty($error))
            {
                $this->db->query("INSERT INTO users(name, email, username, fb_user_id, password, gmt, gmt_zone, access, timestamp, subscription) VALUES('".$name."', '".$email."', '".$username."', '".$fb_user_id."', '".$this->encrypt->sha1($password)."', '".$gmt."', '".$gmt_zone."', '".$admin."', '".$registered_timestamp."', '".$subscription."')");
                $insert_id = $this->db->insert_id();

                $verification_code  = url_encode($insert_id) . '-' . $this->core->rand_numbers(5);

                $this->db->query("INSERT INTO subscribers(id, firstname, lastname, email, registered_timestamp, start_timestamp, expire_timestamp, verification_code) VALUES('".$insert_id."', '".$firstname."', '".$lastname."', '".$email."', '".$registered_timestamp."', '".$start_timestamp."', '".$expire_timestamp."', '".$verification_code."')");
                $done = 1;
                
                $link = site_url('register/verify_email/'.$verification_code.'?email='.$email);
                $message = "
                    You have successfully registered your Account on Postfor.us using the following email address: " . $email . ". We're excited to have you onboard!<br>
                    <br>
                    <a href='" . $link . "'>Click here</a> or copy and paste the url below to verify your account email:<br>
                    " . $link . "<br>";
                $this->subscribe->send_mail($email, $message);
            }

        }

        return array('result' => 'ok', 'fields' => $fields, 'errors' => $errors, 'errors_list' => $errors_list, 'success' => $done);
    }

    function fb_add_subscriber($subscription, $fb_user_info)
    {
        $done           = 0;
        $error          = 0;
        $email_sent_tru = 'email_address';

        $firstname  = $fb_user_info['first_name'];
        $lastname   = $fb_user_info['last_name'];

        $name       = $fb_user_info['name'];
        $username   = $fb_user_info['username'];
        $email      = (isset($fb_user_info['email'])) ? $fb_user_info['email'] : $username."@facebook.com";
        $admin      = 0;
        $password   = $this->core->rand_numbers(10);
        $cpassword  = $password;
        $fb_user_id = $fb_user_info['id'];

        if(!isset($fb_user_info['email'])) $email_sent_tru = 'fb_inbox';

        #setting up expiry date and time
        $registered_timestamp   = $this->core->timestamp();
        $start_timestamp        = $registered_timestamp;
        $expire_timestamp       = strtotime(date('Y-m-d H:i:s', $start_timestamp) . "+1 month");

        $gmt        = 8;
        $gmt_zone   = 114;

        if(!empty($username))
        {
            $qusers = $this->db->query("SELECT id FROM users WHERE username='".$username."'");
            if($qusers->num_rows() > 0)
            {
                $this->messages->add('The username is already in use.','danger');
                $error                      = 1;
            }
        }
        
        if(!empty($email))
        {
            $quser_email = $this->db->query("SELECT id FROM users WHERE email='".$email."'");
            if($quser_email->num_rows() > 0)
            {
                $this->messages->add('The email was already registered.','danger');
                $error                  = 1;
            }            
        }

        if(empty($error))
        {
            $this->db->query("INSERT INTO users(name, email, username, fb_user_id, password, gmt, gmt_zone, access, timestamp, register_through, subscription) VALUES('".$name."', '".$email."', '".$username."', '".$fb_user_id."', '".$this->encrypt->sha1($password)."', '".$gmt."', '".$gmt_zone."', '".$admin."', '".$registered_timestamp."', 'facebook', '".$subscription."')");
            $insert_id = $this->db->insert_id();

            $verification_code  = 1;

            $this->db->query("INSERT INTO subscribers(id, firstname, lastname, email, registered_timestamp, start_timestamp, expire_timestamp, verification_code) VALUES('".$insert_id."', '".$firstname."', '".$lastname."', '".$email."', '".$registered_timestamp."', '".$start_timestamp."', '".$expire_timestamp."', '".$verification_code."')");
            $done = 1;

            $message = "Your Login Credentials:\r\n";
            $message.= "Username: $username \r\n";
            $message.= "Password: $password\r\n \r\n";
            $message.= "You may login to you account here - ".site_url('app/login');
            $this->subscribe->send_mail($email, $message);
        }

        return array('result' => 'ok', 'success' => $done, 'email_sent_tru' => $email_sent_tru);
    }

    function email_verification($verification_code = '', $email = '')
    {
        $error = false;
        $error_text = "";

        if( !empty($verification_code) && !empty($email) )
        {
            $quser = $this->db->query("SELECT id FROM subscribers WHERE email='".$email."' AND verification_code='".$verification_code."' LIMIT 1");
            if($quser->num_rows() <> 0)
                $this->db->query("UPDATE subscribers SET verification_code = '1' WHERE id='".$quser->row()->id."'");
            else 
            {
                $error = 1;
                $error_text = 'The verification link is not valid.<br>Please login and update your email in your Account Settings and request for a new link to verify your email.';
            }
        }
        else
        {
            $error = 1;
            $error_text = 'The verification link is not valid. Please check your email for the verification link to activate your account.';
        }

        if($error) $this->messages->add($error_text,'danger');

        $invalid_verification = $error;
        return $invalid_verification;
    }

    function resend_verification_code()
    {
        $quser = $this->db->query("SELECT email, verification_code FROM subscribers WHERE id='".$this->general->id_user()."'");

        if($quser->row()->verification_code == 1)
        {
            $this->messages->add('<strong>Oops! Request Denied</strong><br>Your email is verified, no need for another verification.','warning');
        }
        else
        {
            $verification_code  = url_encode( $this->general->id_user() ) . '-' . $this->core->rand_numbers(5);
            $this->db->query("UPDATE subscribers SET verification_code = '".$verification_code."' WHERE id='".$this->general->id_user()."'");
            
            $link = site_url('register/verify_email/'.$verification_code.'?email='.$quser->row()->email);
            $message = "
                Your verification link request!<br>
                <br>
                <a href='" . $link . "'>Click here</a> or copy and paste the url below to verify your account email:<br>
                " . $link . "<br>";
            $this->subscribe->send_mail($quser->row()->email, $message, 'Request - New Email Verification Link');

            $this->messages->add('Verification link request has been sent to your email.','success');
        }

        redirect('client_area/account');
    }

    function update_email()
    {
        $success = true;
        $error_msg = '';

        $email = $this->security->sanitize_filename( $this->input->post('email', TRUE) );

        $qusername = $this->db->query("SELECT id FROM users WHERE email='".$email."' AND id='".$this->general->id_user()."' LIMIT 1");
        if($qusername->num_rows() == 1)
        {
            //email not updated or ramained
            $error_msg = '<span class="text-green">This email is already linked to your account.</span>';
        }
        else
        {
            $this->load->helper('email');
            if (valid_email($email))
            {
                if( $this->general->check_available_email() )
                {
                    //email is valid
                    $verification_code  = url_encode( $this->general->id_user() ) . '-' . $this->core->rand_numbers(5);
                    $this->db->query("UPDATE subscribers SET email='".$email."', verification_code = '".$verification_code."' WHERE id='".$this->general->id_user()."'");
                    $this->db->query("UPDATE users SET email='".$email."' WHERE id='".$this->general->id_user()."'");
                    
                    $link = site_url('register/verify_email/'.$verification_code.'?email='.$email);
                    $message = "
                        Your verification link request!<br>
                        <br>
                        <a href='" . $link . "'>Click here</a> or copy and paste the url below to verify your account email:<br>
                        " . $link . "<br>";
                    $this->subscribe->send_mail($email, $message, 'Email Update - Verification Link');

                    $error_msg = '<span class="text-green"><strong>Email successfully updated!</strong><br>A verification link was sent to this email, please verify your new email.</span>';
                }
                else
                {
                    $success = false;
                    $error_msg = '<span class="text-red">Email not available!</span>';
                }
            }
            else
            {
                //email is not valid
                $success = false;
                $error_msg = '<span class="text-red"><strong>Invalid email!</strong> Please enter a valid email address.</span>';
            }
        }

        return json_encode( array('success' => $success, 'msg' => $error_msg, 'email' => $email ) );
    }


    //PASSWORD RETRIEVAL VERIFICATION LINK
    function retrieval_verification($verification_code, $email)
    {
        $error = false;
        $error_text = "";

        if(!empty($verification_code) && !empty($email))
        {
            $quser = $this->db->query("SELECT id FROM subscribers WHERE email='".$email."' AND verification_code='".$verification_code."' LIMIT 1");
            if($quser->num_rows() <> 0)
                $data['id_user'] = $quser->row()->id;
            else 
            {
                $error = 1;
                $error_text = 'The verification link is not valid or has already expired.<br>Please request for a new link for password retrieval <a href="/register/forgot_password">here</a>.';
            }
        }
        else
        {
            $error = 1;
            $error_text = 'The verification link is not valid or has already expired.<br>Please request for a new link for password retrieval <a href="/register/forgot_password">here</a>.';
        }

        if($error) $this->messages->add($error_text,'danger');

        $data['invalid_retrieval_link'] = $error;
        return $data;
    }

    //FORGOT PASSWORD, SENDING RETRIEVAL LINK and UPDATE PASSWORD WITH RETRIEVAL
    function password_retrieval($found = false)
    {
        if($this->input->post('updatepassword') == 'submit')
        {
            $verification_code  = $this->input->post('verification_code', true);
            $email              = $this->input->post('email', true);

            $id_user = (int)url_decode($this->input->post('id'));
            $passw   = $this->input->post('field_password', true);
            $cpassw  = $this->input->post('field_cpassword', true);
            $error      = false;
            $error_text = '';

            if(($passw == '') || ($cpassw == ''))
            {
                $error = 1;
                $error_text = 'Password cannot be empty.';
            }

            if($passw <> $cpassw)
            {
                $error = 1;
                $error_text = 'The confirmation password does not match the new password.';
            }

            if(!$error)
            {
                $this->db->query("UPDATE users SET password = '".$this->encrypt->sha1($passw)."' WHERE id='".$id_user."'");
                $this->db->query("UPDATE subscribers SET verification_code = '1' WHERE id='".$id_user."'");
                $success = 1;
                $this->messages->add('Password successfully updated.<br>You can now login using your new password.','success');
                redirect('register/new_password_success');
            }
            else
            {
                $this->messages->add($error_text,'danger');
                redirect('register/new_password/'.$verification_code.'?email='.$email);
            }
        }

        if($this->input->post('forgotpassword') == 'submit')
        {

            $username   = trim($this->input->post('field_username'));
            $email      = trim($this->input->post('field_email'));
            if(!empty($username))
            {
                $quser = $this->db->query("SELECT id, email FROM users WHERE username='".$username."'");
                if($quser->num_rows() > 0)
                {
                    $id     = $quser->row()->id;
                    $email  = $quser->row()->email;
                    $verification_code  = url_encode($id) . '-' . $this->core->rand_numbers(5);

                    $this->db->query("UPDATE subscribers SET verification_code = '".$verification_code."' WHERE id='".$id."'");
                    $found = true;
                }
                else $text = 'The username <strong>('.$username.')</strong> you entered was not found.';
            }
            elseif(!empty($email))
            {
                $quser = $this->db->query("SELECT id, email FROM users WHERE email='".$email."'");
                if($quser->num_rows() > 0)
                {
                    $id     = $quser->row()->id;
                    $verification_code  = url_encode($id) . '-' . $this->core->rand_numbers(5);

                    $this->db->query("UPDATE subscribers SET verification_code = '".$verification_code."' WHERE id='".$id."'");
                    $found = true;
                }
                else $text = 'The email <strong>('.$email.')</strong> you entered was not found.';
            }
            else
            {
                $text = "<strong>Cannot be empty!</strong><br>Enter either your username or email address.";
            }

            if($found)
            {
                $link = site_url('register/new_password/'.$verification_code.'?email='.$email);
                $message = "Please click the link below or copy and paste to browser's address bar to retrieve your password:\r\n";
                $message.= "<a href='$link'>".$link."</a>";
                $this->subscribe->send_mail($email, $message, 'Password retrieval');

                $this->messages->add('An email was sent to <strong>'.$email.'</strong><br>Please check your email for the retrieval link of your password.','success');
            }
            else
            {
                $this->messages->add($text,'danger');
            }
        }
    }

    function send_mail($email_to, $message, $subject = 'Registered - email verification', $email_from = 'info@postfor.us', $name_from = 'PostForUs')
    {
        $this->load->library('email');
        
        $this->email->from($email_from, $name_from);
        $this->email->to($email_to);            
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
        } else { 
            $err_email = $this->email->print_debugger();
            mail('markcawit@gmail.com', 'Email NOT Sent - Notification', $err_email);
        }
    }

    //CREATE SUPPORT TICKET
    function support_ticket()
    {
        if($this->input->post('submit_ticket') == 'submit')
        {
            $id_user = $this->general->id_user();
            $subject = $this->input->post('subject', true);
            $message = str_replace ( array("\r\n",'"',"'"), array("<br>","&#34;","&#39;"), $this->input->post('message', true) );
            $critical= isset($_POST['critical'])? $this->input->post('critical'):0;

            $error      = false;
            $error_text = '<strong>Empty fields should be filled.</strong>';
            $timestamp  = strtotime(date('Y-m-d H:i:s'));

            if($subject == '')
            {
                $error = 1;
                $error_text .= '<br>Subject cannot be empty.';
            }

            if($message == '')
            {
                $error = 1;
                $error_text .= '<br>Message cannot be empty.';
            }


            if(!$error)
            {
                $this->db->query("INSERT INTO tickets(user_id,subject,timestamp,critical,client_reply) VALUES('$id_user','$subject','$timestamp','$critical','1')");
                $ticket_id = $this->db->insert_id();
                $this->db->query("INSERT INTO tickets_messages(message,timestamp,ticket_id) VALUES('$message','$timestamp','$ticket_id')");
                $success = 1;
                $this->messages->add('Ticket submitted successfully.','success');

                $info['ticket_id']   = $ticket_id;
                $info['subject']     = $subject;
                $info['client_name'] = $this->general->info('name');
                $info['priority']    = isset($_POST['critical'])?'High':'Normal';
                $this->ticket_send_email($this->general->info('email'), $message, $info);
                redirect('client_area/support');
            }
            else
            {
                $this->messages->add($error_text,'danger');
                redirect('support/new_ticket');
            }
        }
    }

    //TICKET NOTIFICATION TO USER
    function ticket_send_email($to_email, $message, $data = array())
    {
        $info = $data;
        $info['message'] = $message;

        $ticket_link= site_url('support/ticket/'.$data['ticket_id']);
        $message    = "
                    <strong style='color:red;'>!! PLEASE READ SO THAT YOU UNDERSTAND HOW OUR SUPPORT AND ADMIN DEPARTMENTS WORK !!</strong><br>
                    <br>
                    ".$data['client_name'].",<br>
                    <br>
                    Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email. The details of your ticket are shown below.<br>
                    <br>
                    Your ticket may be closed and unresponded to...<br>
                    If you are an existing customer requesting support for a service, you MUST open your ticket under your postfor.us account.  If you simply e-mail us, the system may or may not tie it with your postfor.us account.  Should it not, then the ticket will be closed without a response, so please login directly to your account to request for opening a ticket.<br>
                    <br>
                    Subject: ".$data['subject']."<br>
                    Priority: ".$data['priority']."<br>
                    Status: <span style='color:green;'>Open</span><br>
                    <br>
                    You can view the ticket at any time at <a href='$ticket_link' target='_blank'>".$ticket_link."</a> <br>
                    <br>
                    <strong style='color:red;'>Please note our business hours below:</strong><br>
                    <br>
                    Sales/Billing/Management:<br>
                    Monday - Friday 10 AM - 5 PM CST- excluding major U.S. holidays<br>
                    <br>
                    If you opened your ticket during the above hours, please note that we try to address all tickets within 24 hours, however, please allow up to 36 hours at the max.  Tickets opened on the weekend will be responded to the following business day.<br>
                    <br>
                    Technical Support:<br>
                    24/7<br>
                    <br>
                    Please note that any technical support tickets are bound to our Support Policy as stated in our Terms of Service.<br>
                    <br>
                    <br>
                    Become a fan of PostforUS on Facebook!<br>
                    <a href='https://www.facebook.com/postforus' target='_blank'>https://www.facebook.com/postforus</a><br>
                    <br>
                    <strong style='color:red;'>ESCALATION PROCEDURE:</strong><br>
                    <br>
                    If you feel that your issue is not being handled in a way that is not par to your expectations, please e-mail info@postfor.us with the subject 'Ticket Escalation' with your ticket number, so that your issue can be addressed promptly.<br>
                    <br>
                    Thank you,<br>
                    <br>
                    PostforUS Support Team
                      ";

        $subject = '[Ticket ID #'.$data['ticket_id'].'] '.$data['subject'];
        $from_email = $this->config->item('support_email');//'support-noreply@postfor.us';
        $from_name  = 'PostforUs Support Team';

        $this->send_mail($to_email,$message,$subject,$from_email,$from_name);

        $this->ticket_forward_email($info);
    }

    /*
    *   EMAIL NOTIFICATION TO SUPPORT TEAM UPON CREATION OF TICKET
    */
    function ticket_forward_email($data = array())
    {
        $message     = $data['message'];
        $ticket_link = site_url('admin_area/ticket/'.$data['ticket_id']);

        $message    .= "<br><br><br>
                    Ticket link: <a href='$ticket_link' target='_blank'>".$ticket_link."</a><br>
                    Subject: ".$data['subject']."<br>
                    Priority: ".$data['priority']."<br>
                    Status: <span style='color:green;'>Open</span><br>
                    <br>
                       ";

        $subject = 'Ticket Support Notification [Ticket ID #'.$data['ticket_id'].'] ';
        $to_email = 'PostforUS <' . $this->config->item('account_email').'>, Mark Cawit <markcawit@gmail.com>';
        $from_email = $this->config->item('support_email');//'support-noreply@postfor.us';
        $from_name  = 'Ticket Notification';

        $this->send_mail($to_email,$message,$subject,$from_email,$from_name);
    }

    function tickets_info($ticket_id,$field = 'user_id')
    {
        $qticket = $this->db->query("SELECT ".$field." FROM tickets WHERE id='".$ticket_id."' LIMIT 1");
        return ($qticket->num_rows() > 0)? $qticket->row()->$field:0;
    }

    function get_tickets($status)
    {
        $id_user = $this->general->id_user();
        $tickets = $this->db->query("SELECT id,subject, timestamp, critical, client_reply, support_reply FROM tickets WHERE user_id='".$id_user."' AND status='".$status."' ORDER BY tickets.timestamp DESC");

        return $tickets;
    }

    function get_ticket($ticket_id)
    {
        $ticket = $this->db->query("SELECT tickets.*, users.name, users.email FROM tickets INNER JOIN users ON tickets.user_id = users.id WHERE tickets.id='".$ticket_id."'");

        return $ticket;
    }

    function get_ticket_messages($ticket_id)
    {
        $messages = $this->db->query("SELECT * FROM tickets_messages WHERE ticket_id='".$ticket_id."' ORDER BY timestamp DESC");
        return $messages;
    }

    //REPLY SUPPORT TICKET
    function submit_ticket_reply($ticket_id)
    {
        if($this->input->post('submit_reply') == 'submit')
        {
            $id_user = $this->general->id_user();
            $message = str_replace ( array("\r\n",'"',"'"), array("<br>","&#34;","&#39;"), $this->input->post('message') );
            $author  = ($this->general->admin() || $this->general->support()) ? 'support' : 'user';
            $author_name = ($author == 'support') ? 'Support' : '';

            $error      = false;
            $error_text = '';
            $timestamp  = strtotime(date('Y-m-d H:i:s'));

            if($message == '')
            {
                $error = 1;
                $error_text .= 'Message cannot be empty.';
            }


            if(!$error)
            {
                $this->db->query("INSERT INTO tickets_messages(message,author,author_name,timestamp,ticket_id) VALUES('$message','$author','$author_name','$timestamp','$ticket_id')");
                $success = 1;
                $this->messages->add('Reply posted successfully.','success');

                // send notification to user if ticket was answered
                // and set the notification for the user to know using the control panel
                if($author == 'support')
                {
                    //send email to client
                    $info['ticket_id']   = $ticket_id;
                    $info['subject']     = $this->tickets_info( $ticket_id, 'subject' );
                    $info['client_name'] = $this->general->info( 'name', 'users', 'id', $this->tickets_info( $ticket_id, 'user_id' ) );
                    $info['priority']    = ( $this->tickets_info( $ticket_id, 'critical' ) )?'High':'Normal';
                    $this->ticket_reply_email( $this->general->info( 'email', 'users', 'id', $this->tickets_info( $ticket_id, 'user_id' ) ), $message, $info );
                    $this->db->query("UPDATE tickets SET support_reply='1' WHERE id='".$ticket_id."'");
                }
                else
                {
                    $this->db->query("UPDATE tickets SET client_reply='1' WHERE id='".$ticket_id."'");
                }
            }
            else
            {
                $this->messages->add($error_text,'danger');
            }
        }
    }

    //SUPPORT REPLY EMAIL SENDING TO USER
    function ticket_reply_email($to_email, $message, $data = array())
    {
        $ticket_link= site_url('support/ticket/'.$data['ticket_id']);
        $message   .= "<br><br>
                    Become a fan of PostforUS on Facebook!<br>
                    <a href='https://www.facebook.com/postforus' target='_blank'>https://www.facebook.com/postforus</a><br>
                    <br>
                    Need your ticket escalated? Before escalating, please give our support staff enough time to diagnose and resolve your issue.  If after doing so, and things are not being resolved to your satisfaction, e-mail your ticket ID to ticket-escalation@virpus.com. Note that for tickets that are newly opened, and escalated at the same time, the escalation request will go ignored. Once again, allow our staff enough time to get an answer out to you.<br>
                    <br>
                    ----------------------------------------------<br>
                    Ticket ID: #".$data['ticket_id']."<br>
                    Subject: ".$data['subject']."<br>
                    Status: Answered<br>
                    Ticket URL: <a href='$ticket_link' target='_blank'>".$ticket_link."</a><br>
                    ----------------------------------------------<br>
                      ";

        $subject = '[Ticket ID #'.$data['ticket_id'].'] '.$data['subject'];
        $from_email = $this->config->item('support_email');//'no-reply-support@postfor.us';
        $from_name  = 'PostforUs Support Team';

        $this->send_mail($to_email,$message,$subject,$from_email,$from_name);
    }

    function opened_tickets($critical = 0)
    {
        $tickets = $this->db->query("SELECT tickets.id, tickets.user_id, tickets.subject, tickets.timestamp, tickets.client_reply, tickets.support_reply, users.name, users.username 
                                        FROM tickets INNER JOIN users ON tickets.user_id = users.id 
                                        WHERE tickets.status='open' AND tickets.critical='".$critical."' ORDER BY tickets.timestamp ASC");

        return $tickets;
    }

    function close_ticket($ticket_id,$value = 'close')
    {
        $close = $this->db->query("UPDATE tickets SET status = '".$value."' WHERE id='".$ticket_id."'");
    }

    function ticket_notification_off($ticket_id)
    {
        // remove notification if ticket was opened
        if($this->general->admin() || $this->general->support())
            $this->db->query("UPDATE tickets SET client_reply='0' WHERE id='".$ticket_id."'");
        else
            $this->db->query("UPDATE tickets SET support_reply='0' WHERE id='".$ticket_id."'");
    }



}

?>