<?php
extract($acct_info);
?>
<div class="account">
	<h4>My Account</h4>
	<ul type="none">
		<li><i class="glyphicon glyphicon-envelope"></i> <a id="account_email" href="#UpdateEmail" data-toggle="modal" data-target="#UpdateEmail"><?=$email;?> 
			<i class="glyphicon glyphicon-edit" title="Update your email"></i></a></li>
		<li><i class="glyphicon glyphicon-time"></i> Registered since <?=$this->general->_date_format( $this->general->usertime( $registered_timestamp ) );?></li>
		<li><i class="glyphicon glyphicon-fire"></i> Subscription <?=($expired)?'expired last':'will expire on';?> <strong class="text-<?=($expired)?'red':'green';?>"><?=$this->general->_date_format( $this->general->usertime( $expire_timestamp ) );?></strong></li>
		<? if($expired){ ?>
		<li><i class="glyphicon glyphicon-refresh"></i> Your subscription has expired. <a href="<?=site_url('client_area/payment_methods');?>">Click here to renew your subscription</a></li>
		<? } ?>
		<? if( $this->general->info('verification_code','subscribers') <> 1 ){ ?>
		<li><i class="glyphicon glyphicon-repeat"></i> Request: <a href="<?=site_url('client_area/resend_email_verification');?>">New Email Verification Link</a></li>
		<? } ?>
	</ul>
</div>
<div class="account">
	<h4>Contact Information</h4>
	<form role="form" action="<?=site_url('client_area/account');?>" method="post">
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_firstname" name="field_firstname" value="<?=$field_firstname;?>" placeholder="Enter your first name">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_lastname" name="field_lastname" value="<?=$field_lastname;?>" placeholder="Enter your last name">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_address" name="field_address" value="<?=$field_address;?>" placeholder="Enter your mailing address">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_url" name="field_url" value="<?=$field_url;?>" placeholder="Website URL">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_company" name="field_company" value="<?=$field_company;?>" placeholder="Company name">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_title" name="field_title" value="<?=$field_title;?>" placeholder="Title">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_industry" name="field_industry" value="<?=$field_industry;?>" placeholder="Industry">
	  </div>
	  <div class="form-group">
	    <input type="text" class="form-control" id="field_phone" name="field_phone" value="<?=$field_phone;?>" placeholder="Phone Number">
	  </div>
	  <button type="submit" id="update_info" name="update_info" value="submit" class="btn btn-success">Save Changes</button>
	</form>
</div>

<!-- Modal -->
<div class="modal fade" id="UpdateEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Change Account Email</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
			<div class="form-group">
				<label for="new_email" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="new_email" name="new_email" value="<?=$email;?>" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">&nbsp;</label>
				<div class="col-sm-9">
					<span id="errMsg"></span>
				</div>
			</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="update_accountemail">Update</button>
        <button type="button" class="btn btn-default" id="account_modal_close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>