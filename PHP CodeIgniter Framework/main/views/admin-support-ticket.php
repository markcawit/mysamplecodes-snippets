<?php
$this->subscribe->ticket_notification_off($ticket->row()->id);
?>
<div class="support-ticket">
	<div class="ticket_head">
		<a class="ticket_subject"><?=$ticket->row()->subject;?></a><br>
		<span>Ticket # <?=$ticket->row()->id;?> - Created on <?=$this->general->_date_format($this->general->usertime($ticket->row()->timestamp));?>
			at <?=$this->general->_time_format($this->general->usertime($ticket->row()->timestamp),24,false);?>
			by <?=$ticket->row()->name;?></span>
	</div>
	<div class="ticket_body">
		<?php if($ticket->row()->status == 'open'){ ?>
		<h4><i class="glyphicon glyphicon-share-alt"></i><span> Post Reply</span> &nbsp; 
			<a id="close_ticket" href="<?=site_url('admin_area/close_ticket/'.$ticket->row()->id);?>" title="Close ticket"><i class="glyphicon glyphicon-ban-circle"></i> Close Ticket</a></h4>
		<div class="new-support col-sm-12">
			<form action="<?=site_url('admin_area/ticket/'.$ticket->row()->id);?>" method="post" role="form">
				<textarea class="form-control" id="message" name="message" placeholder="Enter your message here" rows="5"></textarea>
				<div class="clear"></div>
				<button type="submit" id="submit_reply" name="submit_reply" value="submit" class="btn btn-success">Submit Reply</button>
			</form>
		</div>
		<?php }else { ?><hr><?php } ?>

		<div class="ticket_messages col-sm-12">
			<?php
			$gravatar = $this->general->gravatar($ticket->row()->user_id);
			foreach ($ticket_messages->result() as $message) {
				$id = $message->id;
				?>
				<div class="ticket_message_details col-sm-12">
					<div class="col-sm-2">
						<center>
						<?=($message->author == 'user')?'<img src="'.$gravatar.'">':'';?><br>
						<?=($message->author == 'user') ? $ticket->row()->name :$message->author_name;?>
						</center>
					</div>
					<div class="col-sm-10">
						<div class="col-sm-12"><?=$message->message;?></div>
						<div class="datetime col-sm-12"><?='Posted on '. $this->general->_date_format($this->general->usertime($message->timestamp)) . ' at ' . $this->general->_time_format($this->general->usertime($message->timestamp),24,false);?></div>
					</div>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</div>