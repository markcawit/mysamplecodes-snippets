<div class="support-ticket">
	<h4><i class="glyphicon glyphicon-folder-open"></i><span> Open Tickets</span></h4>
	<div>
		<h3>Critical</h3>
		<?php
		if($critical_ticket->num_rows() > 0)
		{
		?>
		<ul class="tickets" type="none">
			<? foreach ($critical_ticket->result() as $ticket) { ?>
			<li <?=($ticket->client_reply)?'class="new-reply"':'';?>>
				<a href="<?=site_url('admin_area/ticket/'.$ticket->id);?>"><?=$ticket->subject;?></a><br>
				<span>Ticket # <?=$ticket->id;?> - Created on <?=$this->general->_date_format($this->general->usertime($ticket->timestamp));?>
					at <?=$this->general->_time_format($this->general->usertime($ticket->timestamp),24,false);?>
					by <?=$ticket->name;?></span>

				<a id="close_ticket" href="<?=site_url('admin_area/close_ticket/'.$ticket->id);?>" title="Close ticket" class="glyphicon glyphicon-ban-circle"></a>
			</li>
			<? } ?>
		</ul>
		<?php
		}
		else echo 'Currently, there are no critical tickets';
		?>

		<h3>Moderate</h3>
		<?php
		if($moderate_ticket->num_rows() > 0)
		{
		?>
		<ul class="tickets" type="none">
			<? foreach ($moderate_ticket->result() as $ticket) { ?>
			<li <?=($ticket->client_reply)?'class="new-reply"':'';?>>
				<a href="<?=site_url('admin_area/ticket/'.$ticket->id);?>"><?=$ticket->subject;?></a><br>
				<span>Ticket # <?=$ticket->id;?> - Created on <?=$this->general->_date_format($this->general->usertime($ticket->timestamp));?>
					at <?=$this->general->_time_format($this->general->usertime($ticket->timestamp),24,false);?>
					by <?=$ticket->name;?></span>

				<a id="close_ticket" href="<?=site_url('admin_area/close_ticket/'.$ticket->id);?>" title="Close ticket" class="glyphicon glyphicon-ban-circle"></a>
			</li>
			<? } ?>
		</ul>
		<?php
		}
		else echo 'Currently, there are no open tickets';
		?>
	</div>
</div>

<div class="col-sm-12 content-spacer"></div>	
