<?php
if(!$this->general->admin())
{
	redirect('client_area');
}
?>
    <div class="registration plain_body">
      <div class="container">
        <div class="row">

	      	<?php $messages = $this->messages->get(); ?>
		    <?php if(is_array($messages)){ ?>
		        <div>
		            <? foreach ($messages as $type => $msgs){ ?>
		                <? foreach ($msgs as $message){ ?>
		                    <div class="alert alert-<? echo $type; ?> alert-dismissable">                 
		                    <button class="close" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
		                    <? echo $message; ?>
		                    </div>                
		                <? } ?>
		            <? } ?>
		        </div>
		    <? } ?>

<?php
    		$sub_segment_exist = ($this->uri->segment(2) != 'balance' && $this->uri->segment(2) != 'account') ? true : false;
    		//($this->uri->segment(1) == 'billing' && $sub_segment_exist)? 'active':'';
?>
        	<div class="col-sm-2 list-group">
			  <a href="<?=site_url('app/dashboard');?>" class="list-group-item">App Dashboard</a>
			  <?php if($this->general->admin()){ ?>
			  <a href="<?=site_url('admin_area');?>" class="list-group-item <?=($page_active == 'supp')? 'active':'';?>">Tickets</a>
			  <?php } ?>
	       	</div>
        	<div class="billing-section col-sm-10">
        		<h3><?php echo $page_icon . $page_description; ?></h3>
<?php 
				$this->load->view($page_details);
?>
        	</div>
        </div>
      </div>
    </div>
