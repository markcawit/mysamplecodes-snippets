<a href="<?=site_url('client_area/payment_methods');?>" class="btn btn-success top" style="margin-top: 10px;">Manage Payments</a>
<div class="payment-history">
	<h4><i class="glyphicon glyphicon-time"></i><span> Payment History</span></h4>
	<div>
<?php
		if($payments->num_rows() > 0)
		{
?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th style="width: 200px;">Date</th>
					<th>Description</th>
					<th style="width: 150px;">Method</th>
					<th style="width: 150px; text-align: center;">Amount</th>
				</tr>
			</thead>
			<tbody>
<?php
			foreach ($payments->result() as $history) 
			{
?>
				<tr>
					<td><?=$this->general->_date_format( $this->general->usertime( $history->payment_date ) );?></td>
					<td><?=$history->description;?></td>
					<td><?=( $history->type == 'pp' ) ?'Paypal' :'Credit Card';?></td>
					<td style="text-align: right;">$<?=number_format( $history->amount, 2, '.', ',' );?> USD</td>
				</tr>
<?php
			}
?>
			</tbody>
		</table>
<?php
		}
		else
		{
?>
		Currently, you have no payments made.
<?php
		}
?>
	</div>
</div>
