<div class="paypal">
	<h4><i class="icon-paypal"></i><span>PayPal Payment</span></h4>
	<div class="payment-method col-sm-12">
		<form class="form-horizontal" role="form">
			<div class="form-group">
				<label class="col-sm-4 control-label">Make a one time Paypal Payment</label>
				<div class="col-sm-5">
					<select class="form-control" id="subscription" name="subscription" onchange="paypal_button(id);">
						<option value="1">1 Month $5.00 USD</option>
						<option value="6">6 Months $25.00 USD</option>
						<option value="12">1 Year $50.00 USD</option>
					</select>
				</div>
<?php
				$user_id		= url_encode( $this->general->id_user() );
				$sandbox 		= $this->config->item('sandbox_mode');
				$merchant_id	= ($sandbox) ?'seller@postfor.us' :'2N7LT3GWMDLJ6';
				$callback_url 	= ($sandbox) ?'https://postfor.us/thank-you.php' :'https://postfor.us/register/success';
?>
				<div class="col-sm-3">
					<!-- <input type="submit" class="paynow_btn" id="paynow" name="paynow"> -->
					<div id="mon1" class="paypal_buttons">
						<script src="https://www.paypalobjects.com/js/external/paypal-button.min.js?merchant=<?=$merchant_id;?>" 
							data-custom="<?=$user_id;?>"
						    data-button="buynow" 
						    data-name="1 Month Subscription"
						    data-item_number="1"
						    data-quantity="1" 
						    data-amount="5" 
						    data-currency="USD" 
						    data-shipping="0" 
						    data-tax="0" 
						    data-callback="<?=$callback_url;?>"
						    data-return="<?=site_url('front/thankyou');?>" 
						    data-cancel_return="<?=site_url('client_area/payment_methods');?>" 
						    <?php if($sandbox){ ?>data-env="sandbox"<?php } ?>
						></script>
					</div>
					<div id="mon6" class="paypal_buttons">
						<script src="https://www.paypalobjects.com/js/external/paypal-button.min.js?merchant=<?=$merchant_id;?>" 
							data-custom="<?=$user_id;?>"
						    data-button="buynow" 
						    data-name="6 Months Subscription" 
						    data-item_number="6"
						    data-quantity="1" 
						    data-amount="25" 
						    data-currency="USD" 
						    data-shipping="0"
						    data-tax="0" 
						    data-callback="<?=$callback_url;?>" 
						    data-return="<?=site_url('front/thankyou');?>" 
						    data-cancel_return="<?=site_url('client_area/payment_methods');?>" 
						    <?php if($sandbox){ ?>data-env="sandbox"<?php } ?>
						></script>
					</div>
					<div id="mon12" class="paypal_buttons">
						<script src="https://www.paypalobjects.com/js/external/paypal-button.min.js?merchant=<?=$merchant_id;?>" 
							data-custom="<?=$user_id;?>"
						    data-button="buynow" 
						    data-name="1 Year Subscription" 
						    data-item_number="12"
						    data-quantity="1"
						    data-amount="50"
						    data-currency="USD"
						    data-shipping="0" 
						    data-tax="0" 
						    data-callback="<?=$callback_url;?>" 
						    data-return="<?=site_url('front/thankyou');?>" 
						    data-cancel_return="<?=site_url('client_area/payment_methods');?>" 
						    <?php if($sandbox){ ?>data-env="sandbox"<?php } ?>
						></script>
					</div>
				</div>
			</div>
		</form>
		<p>* It may take less than 5 minutes for the payment to process</p>
	</div>
</div>
