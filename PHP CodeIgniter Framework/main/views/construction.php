<html>
	<head>
		<title>Underconstruction - PostForUs</title>
    	<meta name="alexaVerifyID" content="IPwzm7nyY0vPCMnfx750dx5wXPU" />
		<link rel="shortcut icon" href="assets/images/icons/favicon.ico" type="image/x-icon" />

		<style type="text/css">
		body {
			background-color: #12b6df;
		}
		.logo {
			position: absolute;
			top: 35%;
			left: 42%;
			text-align: center;
		}
		.logo p {
			font-family: "Century Gothic", "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #fff;
			font-size: 12px;
		}
		</style>
	</head>
	<body>
		<div class="logo"><img src="assets/images/postforus-logo.png"><br><p>Operational soon..</p></div>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-41566325-3', 'postfor.us');
		  ga('send', 'pageview');

		</script>
	</body>
</html>