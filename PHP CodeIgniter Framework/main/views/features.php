    <div class="features light_body wall_bg">
      <div class="container">
        <div class="row">
          <div class="feat-box col-sm-12">
            <div class="intro">
              <p>PostforUs is a Platform where you can easily manage your campaign post to all your facebook pages & groups in one place. 
                It allows you to schedule posts or advertisements you want to post automatically on all your selected groups and pages.<br>
                <br>
                Post photo or link to all groups/pages as many as you want with one campaign.<br>
                <br>
                <strong>COMPOSE</strong> &mdash; <strong>SCHEDULE</strong> &mdash; <strong>RELAX</strong>!</p>
              <h2 id="subscription" class="info"><span>Free 30-Day Trial</span><br><em>for you to decide if you want our service or not.</em></h2>
            </div>
          </div>
          <div class="feat-box col-sm-12 features_list">
            <div class="feat-header text-cyan">
              <h2>Featured Services</h2>
              <p>$5/month</p>
            </div>
            <div class="feat-content">
              <ul class="features-list">
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Unlimited Group selection</span>
                    <span class="feat-desc">You can select all your joined facebook groups</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Unlimited Page selection</span>
                    <span class="feat-desc">You can select all the facebook pages you liked</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Campaign Post Automation</span>
                    <span class="feat-desc">The campaigns you created will be posted to your selected groups/pages timeline according to your schedule</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Link Posting</span>
                    <span class="feat-desc">i.e. A link to your facebook album, post, event, or even your own blog site</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Photo Posting</span>
                    <span class="feat-desc">i.e. You're selling gadgets, you upload the photo and create a schedule for campaign</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Custom Selection</span>
                    <span class="feat-desc">Allowed to create a pre-selection of Groups/Pages for your campaign</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Post Interval</span>
                    <span class="feat-desc">Inserts the time between each post of your campaign</span></li>
                <li><span class="feat"><i class="glyphicon glyphicon-ok"></i> Click counts</span>
                    <span class="feat-desc">View clicks of each Link post and total clicks of the Link Campaign</span></li>
                <li class="list-spacer">&nbsp;</li>
                <li><a href="/register/plan/premium" class="btn btn-lg btn-warning orange">Register your 30-DAY FREE Trial Now</a><br>
                    <span class="feat-regbtn-info">After 30-Days of free trial you will subject to pay for the succeeding months of service.</span>
                    </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="cyan_bg wall_bg_shadow clear"></div>