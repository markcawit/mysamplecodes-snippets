    <div class="registration plain_body">
      <div class="container">
        <div class="row">
			<?php $messages = $this->messages->get(); ?>
			<?php if(is_array($messages)){ ?>
				<div class="text-center">
					<? foreach ($messages as $type => $msgs){ ?>
						<? foreach ($msgs as $message){ ?>
							<div class="alert alert-<? echo $type; ?> alert-dismissable">                 
							<? echo $message; ?>
							</div>                
						<? } ?>
					<? } ?>
				</div>
			<? } ?>

			<div class="col-sm-2">
			</div>
			<div class="form_registration col-sm-6">
				<?php
				$label_width = 4;
				$input_width = 8;
				?>
				<form id="forgot_form" name="forgot" action="<?=site_url('register/forgot_password');?>" class="form-horizontal" method="post" autocomplete="off" role="form">
					<div class="form-group">
						<div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
							<h3>Forgot your password?</h3>
							<p>Enter your username or your email address and we'll help you reset your password.</p>
						</div>
					</div>
					<div class="form-group">
						<label for="field_username" class="col-sm-<?=$label_width;?> control-label">Username</label>
						<div class="col-sm-<?=$input_width;?>">
							<input type="text" class="form-control" id="field_username" name="field_username" value="" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-<?=$label_width;?> control-label"></label>
						<div class="col-sm-<?=$input_width;?> text-center">OR</div>
					</div>
					<div class="form-group">
						<label for="field_email" class="col-sm-<?=$label_width;?> control-label">Email</label>
						<div class="col-sm-<?=$input_width;?>">
							<input type="text" class="form-control" id="field_email" name="field_email" value="" placeholder="Email Address">
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-group">
						<div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
							<button type="submit" name="forgotpassword" value="submit" class="btn btn-md btn-primary cyan">Send me Retrieval Link</button>
						</div>
					</div>
				</form>
			</div>
        </div>
      </div>
    </div>
	<div class="col-sm-12 content-spacer"></div>