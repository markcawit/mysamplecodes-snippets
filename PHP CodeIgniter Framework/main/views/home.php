    <div class="banner-slider main_body">
      <div class="container">
        <div class="row">
          <div id="info-section" class="col-sm-6">
            <div class="free-trial-subscription"><span class="highlight">30 DAYS FREE TRIAL</span><br>REGISTER NOW</div>
            <div>
              <p>Your post will be published according to your schedule, whether or not you are logged into your account.</p>
              <p>While you're away, we post it for you. Life is so easy...!</p>

              <div class="spacer"></div>
              <a href="/front/features/#subscription" id="start-freetrial" class="btn btn-lg btn-warning oranger">START <span>your</span> FREE TRIAL</a>
            </div>
          </div>
          <div id="slider-section" class="col-sm-6">
            <div class="slider-wrapper theme-default">
              <div id="slider" class="nivoSlider">
                  <img src="<?php echo base_url();?>assets/images/slider-sample-image.jpg" data-thumb="<?php echo base_url();?>assets/images/slider-sample-image.jpg" alt="" />
                  <!-- <a href="#"> This is an example of a caption- ->
                  <img src="assets/images/slider-2.jpg" data-thumb="assets/images/up.jpg" alt="" title="" />
                <!-- </a> -->
                  <img src="<?php echo base_url();?>assets/images/slider-sample-image-2.jpg" data-thumb="<?php echo base_url();?>assets/images/slider-sample-image-2.jpg" alt="" /><!--  data-transition="slideInLeft" -->
              </div>
              <!-- <div id="htmlcaption" class="nivo-html-caption">#htmlcaption
                  <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="functionality light_body wall_bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="col-1 cols-3-inner">
              <h2>Compose</h2>
              <div class="col-sm-8">
                <p>Prepare your link, photo and select the pages and groups where you want your link to be posted.</p>
                
                <a href="/front/features" class="btn btn-primary cyan">Learn more</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="col-2 cols-3-inner">
              <h2>Schedule</h2>
              <div class="col-sm-8">
                <p>Choose the starting date, time and interval of each post to publish to your selected timelines.</p>
                
                <a href="/front/features" class="btn btn-warning orange">Learn more</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="col-3 cols-3-inner">
              <h2>Relax and Have Fun!</h2>
              <div class="col-sm-8">
                <p>Forget it. Never waste another night or weekend adding posts to all your groups and pages.</p>
                
                <a href="/front/features" class="btn btn-danger red">Learn more</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="ssl-secured plain_body wall_bg_shadow">
      <div class="container">
        <div class="row">
          <div class="col-sm-8">
            <h2>Secure, Reliable <span class="text-lower">and</span> Trusted</h2>
            <p>PostforUs uses SSL 128-bit encryption, 100% of the time on every environment.</p>
          </div>
          <div class="col-sm-4">
            <p>
              <center>
              <a href="https://www.positivessl.com" target="_blank" style="font-family: arial; font-size: 10px; color: #212121; text-decoration: none;">
                <img class="ssl-logo" src="https://www.positivessl.com/images-new/PositiveSSL_tl_white2.gif" alt="SSL Certificate" title="SSL Certificate" border="0" />
                <img class="ssl-logo" src="<?php echo base_url();?>assets/images/ssl-comodo-logo.jpg" alt="SSL Certificate" title="SSL Certificate" border="0" />
              </a>
              </center>
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="subscription dark_body">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <p>Register Now and use it for FREE. <br>Paid subscription enables you to use the designated features for your selected subscription.</p>
            <a href="/front/features/#subscription" id="start-freetrial" class="btn btn-lg btn-primary cyan">START <span>your</span> FREE TRIAL</a>
          </div>
        </div>
      </div>
    </div>
