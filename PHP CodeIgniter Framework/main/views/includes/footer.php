    <div class="footer cyan_bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 links">
            <p><a href="/">Home</a> ·  <a href="<?=site_url('terms_of_use');?>">Terms of Use</a> · <a href="<?=site_url('privacy_policy');?>">Privacy Policy</a></p>
          </div>
          <div class="col-sm-6 copy">
            <p>&copy; <?=$this->config->item('platform');?> 2014. All rights reserved.</p>
          </div>
        </div>
      </div>
    </div>
