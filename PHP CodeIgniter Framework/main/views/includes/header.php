    <div class="navbar navbar-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/front"><img class="logo" src="<?php echo base_url();?>assets/images/postforus-logo.png"></a>
        </div>
        <div class="navbar-collapse collapse relative">
          <ul class="nav navbar-nav navbar-right">
            <li<?=($page_active=='home')?' class="active"':'';?>><a href="/front/home">Home</a></li>
            <li<?=($page_active=='feat')?' class="active"':'';?>><a href="/front/features">Features</a></li>
            <?php if($this->general->id_user() && !$this->general->admin()) { ?>
            <li<?=($page_active=='bill')?' class="active"':'';?>><a href="/client_area/account">Client Area</a></li>
            <?php }elseif($this->general->admin()){ ?>
            <li<?=($page_active=='bill')?' class="active"':'';?>><a href="/admin_area">Admin Area</a></li>
            <?php } ?>
            <li id="user-login-li"><a id="login_nav" href="javascript:void(0);"><i class="glyphicon glyphicon-user"></i></a></li>
          </ul>
          <div id="login-block" class="col-sm-3">
            <?php $protocol = "http" .((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "") . "://"; ?>
            <form action="<?=$protocol.$_SERVER['SERVER_NAME'];?>/app/login/auth/front" name="login" method="post">
              <?php
              if($this->general->id_user())
              {
              ?>
                <div class="form-input">
                  <center>
                    You're already logged in. &rarr; <a href="<?=site_url('app/login/logout/front');?>" class="login_form_logout"><i class="glyphicon glyphicon-log-out"></i> Logout ?</a>
                    <div class="clear"></div>
                    <a href="<?php echo site_url('app/dashboard'); ?>" class="btn btn-md btn-primary cyan">Proceed to Dashboard</a>
                  </center>
                </div>
              <?php
              }
              else
              {
              ?>
                <div class="form-input">
                  <h2>Login to your account</h2>
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="username" required>
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="password" required>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="1" id="remember" name="remember"> Remember me
                    </label>
                  </div>
                </div>

                <button type="submit" class="btn btn-md btn-primary cyan">Log In</button> <span class="forgot_link">&rarr; <a href="/register/forgot_password" class="text-cyan">Forgot Password?</a></span>
                <div class="text-center"> OR </div>
                <div class="clear"></div>
                <div class="text-center">
                  <a href="<?=$fblogin['loginUrl'];?>"><img src="<?=base_url().'assets/images/facebook_connect.png'?>"></a>
                </div>
                <div class="clear"></div>
              <?php
              }
              ?>
            </form>
          </div>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <div class="top-spacer"></div>