<?php $messages = $this->messages->get(); ?>
<?php if(is_array($messages)){ ?>
<div id="message-wrapper">
    <? foreach ($messages as $type => $msgs){ ?>
        <? foreach ($msgs as $message){ ?>
            <div class="alert alert-<? echo $type; ?>">                 
            <? echo $message; ?>
            </div>                
        <? } ?>
    <? } ?>
</div>
<? } ?>