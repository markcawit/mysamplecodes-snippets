<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Postfor.Us is a platform where you can easily manage your campaign posts to all your facebook pages & groups in one place.<?php /*echo $layout_description; ?> for <?=$this->config->item('platform');*/?>">
    <meta name="author" content="http://markcawit.com">
    <meta name="keywords" content="facebook, auto posting, post scheduler, post monitoring, photo posting, fb photo, fb, link share, link post, link scheduler">

    <meta content='<?php echo base_url();?>' property='og:url'>
    <meta content='Postfor.Us - Facebook Campaign Manager for Advertisers' property='og:title'>
    <meta content='Postfor.Us is a platform where you can easily manage your campaign posts to all your facebook pages & groups in one place.' property='og:description'>
    <meta content='<?php echo base_url();?>assets/images/slider-sample-image.jpg' property='og:image'>

    <!-- Favicon and Apple Icons -->
    <?php $this->load->view('includes/apple-icons'); ?>

    <title>Postfor.Us - Facebook Campaign Manager for Advertisers<?php /*echo $layout_title; ?> · <?=$this->config->item('platform');*/?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom.postforus.css" rel="stylesheet">

      <!-- Slider -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/slider/style/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/slider/style/nivo-slider.css" type="text/css" media="screen" />

    <script type="text/javascript">var site_url = '<?=site_url();?>';</script>
  </head>

  <body>

    <?php 
    #header 
    $this->load->view($layout_header);
    ?>

    <?php 
    #content 
    echo $layout_content;
    ?>

    <?php 
    #footer 
    $this->load->view('includes/footer');
    ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/collapse.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/dropdown.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/button.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/tooltip.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/alert.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/transition.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/popover.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/tab.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/modal.js"></script>
    <!--<script src="<?php echo base_url();?>assets/bootstrap/js/affix.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/carousel.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/scrollspy.js"></script> -->

    <!-- Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/slider/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({pauseTime:5000,effect:'sliceDown',});
    });
    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-41566325-3', 'postfor.us');
      ga('send', 'pageview');

    </script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo base_url();?>assets/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </body>
</html>
