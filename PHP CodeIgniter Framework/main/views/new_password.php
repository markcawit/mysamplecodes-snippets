    <div class="registration plain_body">
      <div class="container">
        <div class="row">
			<?php $messages = $this->messages->get(); ?>
			<?php if(is_array($messages)){ ?>
				<div class="text-center">
					<? foreach ($messages as $type => $msgs){ ?>
						<? foreach ($msgs as $message){ ?>
							<div class="alert alert-<? echo $type; ?> alert-dismissable">                 
							<? echo $message; ?>
							</div>                
						<? } ?>
					<? } ?>
				</div>
			<? } ?>

			<?php
			if(!$verify['invalid_retrieval_link']){
			?>
			<div class="col-sm-2">
			</div>
			<div class="form_registration col-sm-6">
				<?php
				$label_width = 4;
				$input_width = 8;
				?>
				<form id="forgot_form" name="forgot" action="<?=site_url('register/forgot_password');?>" class="form-horizontal" method="post" autocomplete="off" role="form">
					<div class="form-group">
						<div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
							<h3>New Password</h3>
							<p>Enter your new password to change the one you forgotten.</p>
						</div>
					</div>
					<input type="hidden" id="verification_code" name="verification_code" value="<?=$verification_code;?>">
					<input type="hidden" id="email" name="email" value="<?=$email;?>">
					<input type="hidden" id="id" name="id" value="<?=url_encode($verify['id_user']);?>">
					<div class="form-group">
						<label for="field_password" class="col-sm-<?=$label_width;?> control-label">New Password</label>
						<div class="col-sm-<?=$input_width;?>">
							<input type="password" class="form-control" id="field_password" name="field_password" value="" placeholder="New password">
						</div>
					</div>

					<div class="form-group">
						<label for="field_cpassword" class="col-sm-<?=$label_width;?> control-label">Confirm Password</label>
						<div class="col-sm-<?=$input_width;?>">
							<input type="password" class="form-control" id="field_cpassword" name="field_cpassword" value="" placeholder="Re-type new password">
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-group">
						<div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
							<button type="submit" name="updatepassword" value="submit" class="btn btn-md btn-primary cyan">Update Password</button>
						</div>
					</div>
				</form>
			</div>
			<?php
			}
			?>
        </div>
      </div>
    </div>
	<div class="col-sm-12 content-spacer"></div>	
	<div class="col-sm-12 content-spacer"></div>