    <div class="registration plain_body">
      <div class="container">
        <div class="row">
			<?php $messages = $this->messages->get(); ?>
			<?php if(is_array($messages)){ ?>
				<div class="text-center">
					<? foreach ($messages as $type => $msgs){ ?>
						<? foreach ($msgs as $message){ ?>
							<div class="alert alert-<? echo $type; ?> alert-dismissable">                 
							<? echo $message; ?>
							</div>                
						<? } ?>
					<? } ?>
				</div>
			<? } ?>
        </div>
      </div>
    </div>
	<div class="col-sm-12 content-spacer"></div>
	<div class="col-sm-12 content-spacer"></div>
	<div class="col-sm-12 content-spacer"></div>