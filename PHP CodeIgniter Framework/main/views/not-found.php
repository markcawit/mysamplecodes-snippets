<div class="thank-you">
	<h1>Oops!</h1>
	<h3>The page you requested was not found.</h3>
	<p>If you're not a registered user, please register first. Click <a href="<?=site_url('front/features/#subscription');?>">here</a><br>Don't worry - everyone is welcome to test our service. It's free for your first 30 Days.</p>

    <a href="/front/features/#subscription" id="start-freetrial" class="btn btn-lg btn-warning oranger">START <span>your</span> FREE TRIAL</a>
</div>
