<div class="privacy-terms">
	<h2>Privacy Policy</h2>

	<p>PostforUS (“PostforUS,” “We,” “Us” and “Our”) takes your privacy seriously. Please read this Privacy Policy, which describes the types of information We collect through www.postfor.us (the “Site”), and how We use that information.</p>

	<h3>A. Overview</h3>
	<p>This Privacy Policy is incorporated into and subject to the terms of the Site’s <a href="<?=site_url('terms_of_use');?>">Terms of Use Agreement</a>. This Privacy Policy covers PostforUS’ treatment of personally identifiable information (“Personal Information”) and other non-personally identifiable information that We gather when you access the Site and when you use our services on the Site to order the services from PostforUS (the “Services”). By visiting the Site or using the Services, you expressly acknowledge that you accept the practices and policies outlined in this Privacy Policy and consent to Our use and disclosure of your Personal Information in the manner described in this Privacy Policy. If you do not agree to the terms of this Privacy Policy, please do not use the Site or the Services. If you have questions regarding this Privacy Policy, you may contact Us at info@postfor.us.</p>

	<h3>B. Information We Collect</h3>
	<p>To order products or use some Services on the Site, you will need to provide contact and identity information, billing information, and other Personal Information as indicated. Once you register on the Site, you are no longer anonymous to Us. The information that We gather from you through the Site enables Us to provide, maintain and improve the Site and the Services, personalize your experience, process your requests, and process the requests of others, and includes the following information:<p>

	<ul>
		<li>Personal Information that you provide to Us including your name, address, email address, phone numbers and other basic contact information, and your preferences and credit card information.</li>
		<li>Non-personally identifiable information that We gather on the Site, including your IP address, computer sign-on data, statistics on page views, traffic to and from the Site and individual pages on the Site and data that We may collect through cookies, Web beacons or other means. You can take steps to disable cookies on your browser; however, this may affect your ability to use the Site.</li>
		<li>General information regarding the orders you place through the Site.</li>
	</ul>

	<h3>C. Our Use and Disclosure of Your Information</h3>
	<p>We may use your Personal Information to:</p>

	<ul>
		<li>provide the Services and products;</li>
		<li>resolve disputes, calculate and collect fees, and troubleshoot problems;</li>
		<li>verify users’ identity and the information users provide;</li>
		<li>encourage a safe online experience and enforce Our policies;</li>
		<li>customize users’ experience, analyze usage of the Site, improve and measure interest in Our Services, and inform users about Services, products and updates;</li>
		<li>provide users with information that may affect their use of the Site, the Services and products;</li>
		<li>communicate marketing and promotional offers to Our users;</li>
		<li>provide customer service; and</li>
		<li>perform certain other business activities.</li>
	</ul>

	<p>We do not sell or rent your Personal Information to third parties for their marketing purposes without your explicit consent. We may share and you hereby consent to Our sharing of personal information with the following:</p>

	<ul>
		<li>Corporate affiliates to help coordinate the Services or products We provide to you, enforce Our Terms of Service and other agreements and to promote trust and safety.</li>
		<li>Service providers and others who help with Our business operations and assist in the delivery of the Services and other products to Our users. Examples include, but are not limited to, the following: maintaining servers, sending email, removing repetitive information from user lists, analyzing data, providing marketing assistance, processing payments, reviewing content and providing customer service. Unless We tell you differently, such service providers and other companies and people do not have any right to use Personal Information We share with them beyond what is necessary to assist Us.</li>
		<li>Third parties if We believe that disclosure is advisable or necessary to comply with the law, to enforce or apply Our conditions of use and other agreements, or to protect the rights, property or safety of PostforUS, Our employees, Our users or others. This may include exchanging information with other companies and organizations for fraud protection and credit risk reduction.</li>
		<li>Entities who acquire Us or assets from Us. In some cases, We may choose to buy or sell assets. In these types of transactions, user information, including your Personal Information, is typically one of the business assets that are transferred. Moreover, if PostforUS, all or Our assets or the Site Were acquired, or in the unlikely event that PostforUS goes out of business or enters bankruptcy, user information, including your Personal Information, would be one of the assets that is transferred to or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of PostforUS, Our assets or the Site may continue to use your Personal Information as set forth in this Privacy Policy.</li>
		<li>Persons or entities with whom you consent to have your Personal Information shared. For instance, when you request information, Services or products from Us, from Our affiliates or a third party through Us, or when you post information to the Site or through the Services, you provide your consent for Us to share that information with others. Please note that whenever you post information on or through the Site or Services you are giving Us your consent to share that information with third parties.</li>
	</ul>

	<h3>D. Third Party Websites & User Files</h3>
	<p>Emails from PostforUS or its business partners, and the Site itself, may contain links to other Internet Websites, including sites that may or may not be owned or operated by PostforUS. You may also be able to access data, content, code, files or other material in any form that users post to the Site (“User Files”) on or through the Site. Unless otherwise explicitly stated, We are not responsible for the privacy practices or the content of such Websites and User Files, including the use of any information collected when email recipients or the Site’s visitors click through links to those sites or click on or download User Files. If you visit such Websites or use User Files, We encourage you to become familiar with the applicable privacy practices and any terms of use.</p>

	<h3>E. Cookies</h3>
	<ol>
		<li>A cookie is a small amount of data, which often includes an anonymous unique identifier, that is sent to your browser from a web site's computers and stored on your computer's hard drive.</li>
		<li>Cookies are required to use the PostforUS Services.</li>
		<li>We use cookies to record current session information, but do not use permanent cookies. You are required to log-in to PostforUS after a certain period of time has elapsed to protect you against others accidentally accessing your account contents.</li>
	</ol>

	<h3>F. Accessing, Reviewing and Changing Your Personal Information</h3>
	<p>You may contact Us at Our support section to review any Personal Information We store about you that is not available on the Site. There may be a charge associated with such requests.</p>
	<p>Additionally, We may retain Personal Information to resolve disputes, enforce Our policies, and as otherwise permitted or required by law.</p>
	
	<h3>G. Security</h3>
	<p>You should know that “perfect security” does not exist on the Internet, regardless of any effort on Our part to protect Our Personal Information, so We make no guarantees. Third parties may unlawfully intercept or access transmissions or private communications and you should not expect that your Personal Information will remain private. Additionally, it is your responsibility to keep your account and Personal Information safe and secure by selecting and protecting your password appropriately and limiting access to your computer and browser by signing off after you have finished accessing your account.</p>

	<h3>H. Notification of Changes</h3>
	<p>We may amend this Privacy Policy at any time by posting the amended terms on the Site. In the event of amendment, all terms shall become effective immediately upon posting to the Site.</p>
	
	<h3>I. Questions or Concerns</h3>
	<p>If you have any questions or concerns regarding privacy at the Site, please send a detailed message to info@postfor.us. We will make every effort to resolve your concerns.</p>
</div>