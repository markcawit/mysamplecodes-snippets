    <div class="registration plain_body">
      <div class="container">
        <div class="row">

          <?php $messages = $this->messages->get(); ?>
          <?php if(is_array($messages)){ ?>
              <div class="text-center">
                  <? foreach ($messages as $type => $msgs){ ?>
                      <? foreach ($msgs as $message){ ?>
                          <div class="alert alert-<? echo $type; ?> alert-dismissable">                 
                          <? echo $message; ?>
                          </div>                
                      <? } ?>
                  <? } ?>
              </div>
          <? } ?>

          <?php
          /*echo "<pre>";
          print_r($fbaccess['user_info']);
          echo "</pre>";*/
          ?>

<?php
    if(!$this->general->id_user() && !$registered)
    {
?>
          <div class="fb_registration col-sm-4">
            <h3>Want to register using your facebook account?</h3>

            <p class="head_info">Click the "Register through Facebook" button to link your account. 
              Just make sure to allow all the requested access by PostforUs application in order to finish your registration.</p>

            <p><a href="<?=$fbaccess['loginUrl'];?>"><img class="fb_reg_btn" src="<?=base_url().'assets/images/facebook_register.png'?>"></a></p>
          </div>
          <div class="col-sm-1"></div>
          <div class="form_registration col-sm-6">
            <?php
            $label_width = 4;
            $input_width = 8;
            ?>
            <form id="registration_form" name="register" action="<?=site_url('register/plan/'.$subscription.'/add');?>" class="form-horizontal" method="post" autocomplete="off" role="form">
              <div class="form-group">
                <div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
                  <h3>Registration Form</h3>
<?php
    $with_errors = false;
    $num_errors = 0;

    if(isset($registration))
    {
        foreach($registration['errors_list'] as $error)
        {
            if(!$with_errors) 
            {
?>
              <div class="alert alert-danger"><strong>Oops!</strong> Please check the listed errors..<br>
                <ul>
<?php
              $with_errors = true;
            }
?>
                  <li><?=$error;?></li>
<?php
        }

        if($with_errors)
        {
        ?>
                </ul>
              </div>
        <?php
        }
    }
?>
                </div>
              </div>
              <div class="form-group">
                <label for="field_firstname" class="col-sm-<?=$label_width;?> control-label">First Name</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="text" class="form-control" id="field_firstname" name="field_firstname" value="<?=@$registration['fields']['field_firstname'];?>" required placeholder="First Name">
                </div>
              </div>
              <div class="form-group">
                <label for="field_lastname" class="col-sm-<?=$label_width;?> control-label">Last Name</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="text" class="form-control" id="field_lastname" name="field_lastname" value="<?=@$registration['fields']['field_lastname'];?>" required placeholder="Last Name">
                </div>
              </div>
              <div class="form-group">
                <label for="field_email" class="col-sm-<?=$label_width;?> control-label">Email</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="email" class="form-control" id="field_email" name="field_email" value="<?=@($registration['errors']['field_email'])?'':@$registration['fields']['field_email'];?>" required placeholder="Email Address">
                  <?=@($registration['errors']['field_email'])?'<label for="field_email" class="invalid-pass">Email was already registered by other user.</label>':'';?>
                </div>
              </div>
              <div class="spacer"></div>
              <div class="form-group">
                <label for="field_username" class="col-sm-<?=$label_width;?> control-label">Username</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="text" class="form-control" id="field_username" name="field_username" value="<?=@($registration['errors']['field_username'])?'':@$registration['fields']['field_username'];?>" required placeholder="Username">
                  <?=@($registration['errors']['field_username'])?'<label for="field_username" class="invalid-pass">Username is not available.</label>':'';?>
                </div>
              </div>
              <div class="form-group">
                <label for="field_password" class="col-sm-<?=$label_width;?> control-label">Password</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="password" class="form-control" id="field_password" name="field_password" required placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <label for="field_cpassword" class="col-sm-<?=$label_width;?> control-label">Confirm Password</label>
                <div class="col-sm-<?=$input_width;?>">
                  <input type="password" class="form-control" id="field_cpassword" name="field_cpassword" placeholder="Re-type your password">
                </div>
              </div>
              <div class="clear"></div>
              <div class="form-group">
                <div class="col-sm-offset-<?=$label_width;?> col-sm-<?=$input_width;?>">
                  <button type="submit" name="register" value="submit" class="btn btn-md btn-primary cyan">Register for Free Trial</button>
                </div>
              </div>
            </form>
          </div>
<?php
    }
?>
        </div>
      </div>
    </div>