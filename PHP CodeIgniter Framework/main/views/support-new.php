<div class="support-ticket">
	<h4><i class="glyphicon glyphicon-bell"></i><span> New Ticket</span></h4>
	<div class="new-support col-sm-12">
		<form action="<?=site_url('support/new_ticket');?>" method="post" role="form">
			<input class="form-control" id="subject" name="subject" placeholder="Subject">
			<textarea class="form-control" id="message" name="message" placeholder="Please detail your issue or question..." rows="5"></textarea>
			<label class="control-label">
				<input type="checkbox" id="critical" name="critical" value="1"> If your ticket is truly critical, click here and we will have our on-call support response right away.
			</label>
			<div class="clear"></div>
			<button type="submit" id="submit_ticket" name="submit_ticket" value="submit" class="btn btn-success">Create Ticket</button>
		</form>
	</div>
</div>
