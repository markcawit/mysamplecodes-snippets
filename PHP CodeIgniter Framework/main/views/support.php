<?php
$user_info = $this->general->access();
if($user_info['check_admin'])
{
	redirect('admin_area');
}
?>
<a href="<?=site_url('support/new_ticket');?>" class="btn btn-success top" style="margin-top: 10px;">New Ticket</a>
<div class="support-ticket">
	<h4><i class="glyphicon glyphicon-folder-open"></i><span> Open Tickets</span></h4>
	<div>
		<?php
		if($otickets->num_rows() > 0)
		{
		?>
		<ul class="tickets" type="none">
			<? foreach ($otickets->result() as $ticket) { ?>
			<li <?=($ticket->support_reply)?'class="new-reply"':'';?>>
				<a href="<?=site_url('support/ticket/'.$ticket->id);?>"><?=$ticket->subject;?></a><br>
				<span>Ticket # <?=$ticket->id;?> - Created on <?=$this->general->_date_format($this->general->usertime($ticket->timestamp));?>
					at <?=$this->general->_time_format($this->general->usertime($ticket->timestamp),24,false);?></span>

				<a id="close_ticket" href="<?=site_url('support/close_ticket/'.$ticket->id);?>" title="Close ticket" class="glyphicon glyphicon-ban-circle"></a>
			</li>
			<? } ?>
		</ul>
		<?php
		}
		else echo 'Currently, you have no open tickets';
		?>
	</div>
</div>

<div class="support-ticket">
	<h4><i class="glyphicon glyphicon-folder-close"></i><span> Closed Tickets</span></h4>
	<div>
		<?php
		if($ctickets->num_rows() > 0)
		{
		?>
		<ul class="tickets" type="none">
			<? foreach ($ctickets->result() as $ticket) { ?>
			<li>
				<a href="<?=site_url('support/ticket/'.$ticket->id);?>"><?=$ticket->subject;?></a><br>
				<span>Ticket # <?=$ticket->id;?> - Created on <?=$this->general->_date_format($this->general->usertime($ticket->timestamp));?>
					at <?=$this->general->_time_format($this->general->usertime($ticket->timestamp),24,false);?></span>
			</li>
			<? } ?>
		</ul>
		<?php
		}
		else echo 'Currently, you have no closed tickets';
		?>
	</div>
</div>

<div class="col-sm-12 content-spacer"></div>	
