<div class="thank-you">
	<h1>Congratulations!</h1>
	<h3>You have successfully extended your subscription for using PostforUS</h3>
	<p>You'll receive an email with your subscription containing the payment details.</p>

	<a href="<?=site_url('app/dashboard');?>" class="btn btn-lg btn-success">PROCEED TO APPLICATION DASHBOARD</a>
</div>