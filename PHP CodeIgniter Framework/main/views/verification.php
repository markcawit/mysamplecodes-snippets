
<div class="thank-you">

<?php $messages = $this->messages->get(); ?>
<?php if(is_array($messages)){ ?>
    <div>
        <? foreach ($messages as $type => $msgs){ ?>
            <? foreach ($msgs as $message){ ?>
                <div class="alert alert-<? echo $type; ?> alert-dismissable">                 
                <button class="close" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
                <? echo $message; ?>
                </div>                
            <? } ?>
        <? } ?>
    </div>
<? } ?>

<?php
	if($invalid_verification)
	{
?>
	<h1>Oops!</h1>
	<h3>Verification link is not valid.</h3>
	<p>If a registered user, please login and request for a email verification link again.</p>
<?php
	}
	else
	{
?>
	<h1>Great!</h1>
	<h3>Thank your for verifying your email. Any important notification will be sent to your email.</h3>
	<p>Enjoy your subscription for our services.</p>

    <a href="<?=site_url('app/login');?>" id="start-freetrial" class="btn btn-lg btn-warning oranger">LOGIN TO DASHBOARD</a>
<?php
	}
?>
</div>
