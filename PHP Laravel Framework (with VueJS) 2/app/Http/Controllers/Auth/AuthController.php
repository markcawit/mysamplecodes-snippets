<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
// use Validator;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/#!/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login()
    {
        if ( !Auth::check() ) {
            return view('auth.login');
        }
        else
            return redirect()->back();
    }

    public function postLogin(LoginRequest $request)
    {
        $input = $request->all();

        $user = User::where(['username' => $input['username'], 'password' => $input['password']])->first();
        $token = is_null($user) ? 0 : $user->user_sn;

        $remember = isset($input['remember']) ? 1 : 0;
        // \Session::flash('flash_message', 'Remember: '.$remember);

        if (!is_null($user)) {
            Auth::login($user, $remember);

            if ( Auth::check() ) {
                // return redirect($this->redirectTo);
                return [
                        // 'url'=>'/',
                        'user' => Auth::user(),
                        'access' => 1,
                        'error' => 0
                    ];
            }
        }

        // return redirect()->back()
        //     ->withInput()
        //     ->withErrors(['err_message'=>'Please check your credentials']);
        return [
                'access' => 0,
                'error' => 1,
                'message' => 'Please check your credentials'
            ];
    }

    public function checkAccess()
    {
        $access = false;
        $user = null;
        if (Auth::user()) {
            $access = 1;
            $user = Auth::user()->load('people', 'company');
        }
        return ['access' => $access, 'user' => $user];
    }

    /**
     * Log out the user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        // Or put this on AuthenticatesUser, and
        // do $this->auth->logout();
        Auth::logout();
        // return redirect('/');
    }

}
