<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\User;
use App\Events\EmailPasswordReset;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function requestEmail(Request $request)
    {
        $username = $request->all()['email'];
        $user = User::where('username', $username)->first();

        if ($user) {
            event( new EmailPasswordReset($user->toArray()) );

            return [
                'error' => false,
                'message' => 'Please check your email and click the reset link.',
                'module' => 'request-email',
                ];
        }
        else {
            return [
                'error' => true, 
                'message' => 'Email/Username is not registered',
                'module' => 'request-email',
                ];
        }
    }

    public function resetPass(Request $request)
    {
        $input = $request->all();
        $user = User::where([ 
                        'username'=>$input['email'], 
                        'remember_token'=>$input['token'] 
                        ]
                    )->first();

        if ($user) {
            $token = Str::random(60); // Password has been reset, Token will reset 

            $user->password = $input['password'];
            $user->remember_token = $token;
            $user->save();

            return [
                'error' => false, 
                'message' => 'Successful password reset. <a href="/#!/login">Click here to Login</a>',
                'module' => 'reset-password',
                ];
        }
        else {
            return [
                'error' => true, 
                'message' => 'Invalid email or expired reset token',
                'module' => 'reset-password',
                ];
        }
    }

    public function validateToken($token, $email)
    {
        $valid = User::where(['username'=>$email, 'remember_token'=>$token])->first();

        $validated = true;
        $message = '';
        if (!$valid) { 
            $validated = false;
            $message = 'The link you used to reset your password was already expired. Please got to Forgot Password page again to request for a password reset link.';
        }

        return [
            'valid'=>$validated,
            'message'=>$message,
            'module' => 'request-email',
            ];
    }

}
