<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Group;
use App\Models\Report;
use App\Models\ReportUser;
use App\Models\ArgReportUser;

use Auth;

class ReportsController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReports($category)
    {
    	$reports = Report::where(['category'=>$category])->get();

    	$response = [];
        $response['facilities'] = Auth::user()->company->facilities;
        $response['reports'] = $reports;
        $response['company'] = Auth::user()->company->company_sn;

        foreach($reports as $report) {
        	$response['args'][$report->report_sn] = $report->args;
        }

        $groups = Auth::user()->company->groups;
        foreach($groups as $group) {
            // $response['groups'][$group->c_sn.'-'.$group->f_sn] = $group;
            if (!isset($response['groups'][$group->c_sn.'-'.$group->f_sn])) {
                $response['groups'][$group->c_sn.'-'.$group->f_sn] = array();
            }

            array_push($response['groups'][$group->c_sn.'-'.$group->f_sn], $group);
        }
        
        return $response;
    }

    /**
     * Store selected Report with its Arguments
     *
     * @return \Illuminate\Http\Response
     */
    public function storeReports(Request $request)
    {
    	$input = $request->all();

        $dashboard = (isset($input['dashboard']) && $input['dashboard'] == 'true') ? 1 : 0;
		$reportUser = ReportUser::create([
		            'user_sn' => Auth::user()->user_sn,
		            'report_sn' => $input['report'],
		            'dashboard' => $dashboard
		        ]);
    	$report_user_sn = $reportUser->report_user_sn;

    	$argReportUser = $this->insertArgReportUser($input['fields'], $report_user_sn);
    	return $argReportUser;
    }

    public function insertArgReportUser ($fields, $report_user_sn)
    {
    	$args = $argReportUser = [];

    	for ($x=0; $x < count($fields); $x++) { 
    		$args['report_user_sn'] = $report_user_sn;
    		$args['user_sn'] = Auth::user()->user_sn;
    		$args['arg_sn'] = (int)$fields[$x]['arg_sn'];

    		if ($fields[$x]['name'] == 'company')
    			$args['value'] = (string)Auth::user()->company_sn;
    		else
    			$args['value'] = $fields[$x]['value'];

	    	$argReportUser[] = ArgReportUser::create($args);
    	}

    	return $argReportUser;
    }

    public function fetchUserReports ()
    {
        $reportArgs = ReportUser::where(['dashboard'=>1, 'user_sn'=>Auth::user()->user_sn])->get();
        return $this->generateAttributes($reportArgs);
    }

    public function fetchUserReportsByCategory ($category)
    {
        $reports = Report::where(['category'=>$category])->get();
        $reps = [];
        foreach ($reports as $report) {
            $rep = $report->ReportUser()->get();

            foreach ($rep as $r) {
                $reps[] = $this->fetchUserReportsByID($r->report_user_sn);
            }
        }
        return $reps;
    }

    public function fetchUserReportsByID ($id)
    {
        $reportArgs = ReportUser::where(['report_user_sn'=>$id])->get();
        return $this->generateAttributes($reportArgs)[0];
    }

    public function generateAttributes ($reportArgs)
    {
        $x=0;
        $attributes = [];
        foreach ($reportArgs as $reportArg) {
            $attributes[$x]['report_sn'] = $reportArg->report_sn;
            $attributes[$x]['report_user_sn'] = $reportArg->report_user_sn;
            $attributes[$x]['params'] = $reportArg->getIframeAttribute();
            $attributes[$x]['title'] = $reportArg->report->name;

            $attributes[$x]['sub_title'] = $reportArg->getReportSubtitle();
            $attributes[$x]['value'] = $reportArg->value;
            $x++;
        }

        return $attributes;
    }

    public function deleteReports ($id)
    {
        ArgReportUser::where(['report_user_sn'=>$id])->delete();
        ReportUser::where(['report_user_sn'=>$id])->delete();

        return ['success'=>1, 'message'=>'Report and its arguments were removed.'];
    }
}
