<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {

    Route::get('login', 'Auth\AuthController@login');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@logout');
    Route::get('access', 'Auth\AuthController@checkAccess');

    Route::post('forgot-password/email', 'Auth\PasswordController@requestEmail');
    Route::post('forgot-password/reset', 'Auth\PasswordController@resetPass');
    Route::get('validate-token/{token}/{email}', 'Auth\PasswordController@validateToken');

    Route::get('fetch-reports', 'ReportsController@fetchUserReports');
    Route::get('fetch-reports/{category}', 'ReportsController@fetchUserReportsByCategory');
    Route::get('get-report/{category}', 'ReportsController@getReports');
    Route::get('delete-report/{id}', 'ReportsController@deleteReports');
    Route::post('save-report', 'ReportsController@storeReports');


    Route::group(['prefix' => 'password'], function() {
        // Password Reset
        Route::post('email', 'Auth\PasswordController@sendResetLinkEmail');
        Route::post('reset', 'Auth\PasswordController@reset');
        Route::get('reset/{token?}', 'Auth\PasswordController@showResetForm');
    });

    Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
});