<?php

namespace App\Listeners;

use App\Events\EmailPasswordReset;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class EmailPasswordResetListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  EmailPasswordReset  $event
     * @return void
     */
    public function handle(EmailPasswordReset $event)
    {
        $data = $event->inputs;

        $token = $data['remember_token'];
        $email = $data['username'];

        // Sending of email 
        Mail::send('auth.emails.resetpass', compact('data', 'token', 'email'), function ($m) use ($data) {
            $m->to($data['username'], '') // username as Email Address
                ->subject('Request for Password Reset')
                ->replyTo('no-reply@keygreen.com', 'Keygreen');
        });
    }
}
