<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Arg extends Eloquent {

	/**
     * [$table description]
     * @var string
     */
	protected $table = 'arg_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'arg_sn';

    /**
     * [users description]
     * @return [type] [description]
     */
    public function report()
    {
        return $this->belongsTo('App\Models\Report');
    }

}