<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ArgReportUser extends Eloquent
{

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'arg_report_user_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'arg_report_sn';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['report_user_sn', 'user_sn', 'arg_sn', 'value'];

    /**
     * [args description]
     * @return [type] [description]
     */
    public function args() {
        return $this->belongsTo('App\Models\Arg');
    }

    public function getValueAttribute($value)
    {
        return 'test-'.$value;
    }

}
