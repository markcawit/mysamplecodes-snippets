<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Company extends Eloquent {

	/**
     * [$table description]
     * @var string
     */
	protected $table = 'company_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'company_sn';

    /**
     * [facilities description]
     * @return [type] [description]
     */
    public function facilities()
    {
        return $this->hasMany('App\Models\Facility', 'company_sn')->orderBy('facility_name', 'asc');
    }

    /**
     * [groups description]
     * @return [type] [description]
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Group', 'c_sn')->orderBy('group_name', 'asc');
    }

}