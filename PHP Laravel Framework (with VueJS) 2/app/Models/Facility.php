<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Facility extends Eloquent {

	/**
     * [$table description]
     * @var string
     */
	protected $table = 'facility_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'facility_sn';

}