<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class People extends Eloquent {

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'people_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'people_sn';

}