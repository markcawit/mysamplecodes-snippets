<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Auth;
use Carbon\Carbon;

class Report extends Eloquent {

    /**
     * [$table description]
     * @var string
     */
	protected $table = 'report_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'report_sn';

    /**
     * [args description]
     * @return [type] [description]
     */
    public function args()
    {
        return $this->belongsToMany('App\Models\Arg', 'arg_report_table', 'report_sn', 'arg_sn', 'arg_report_sn')
            ->withPivot('default')
                ->withTimestamps();
    }

    public function ReportUser()
    {
        return $this->hasMany('App\Models\ReportUser', 'report_sn')
                    ->whereUserSn(Auth::user()->user_sn);
    }
}