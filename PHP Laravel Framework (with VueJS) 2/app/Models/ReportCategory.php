<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ReportCategory extends Eloquent
{

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'report_categories_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'report_categories_sn';
    //

}
