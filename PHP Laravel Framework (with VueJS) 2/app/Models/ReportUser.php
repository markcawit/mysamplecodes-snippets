<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Auth;
use Carbon\Carbon;

class ReportUser extends Eloquent
{

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'report_user_table';

    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey = 'report_user_sn';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_sn', 'report_sn', 'dashboard', 'order'];

    /**
     * [report description]
     * @return [type] [description]
     */
    public function report() {
        return $this->belongsTo('App\Models\Report', 'report_sn');
    }

    /**
     * [args description]
     * @return [type] [description]
     */
    public function args() {
        return $this->belongsToMany('App\Models\Arg', 'arg_report_user_table', 'report_user_sn', 'arg_sn')
            ->withPivot('value')
                ->withTimestamps();
    }

    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_sn');
    }

    /**
     * [getArgValue description]
     * @return [type] [description]
     */
    public function getArgValue($argument)
    {
        foreach ($this->args as $arg) {
            if ($arg->name == $argument) {
                if ($arg->type == 'date' && ! empty($arg->pivot->value)) {
                    return Carbon::parse($arg->pivot->value)->format('m/d/Y');//.'-test'
                }
                return $arg->pivot->value;//.'-test'
            }
        }
        return null;
    }

    /**
     * [getIframeAttribute description]
     * @return [type] [description]
     */
    public function getIframeAttribute()
    {
        $args = [];
        foreach ($this->args as $arg) {
            // if($arg->type != 'system' && $arg->name != 'autodates' && ! empty($arg->pivot->value)) {
                if ($arg->pivot->value == '' || $arg->pivot->value == '0')
                    $value = '';
                elseif ($arg->type == 'month')
                    $value = date('M', strtotime(sprintf('2015-%s-01', $arg->pivot->value)));
                elseif ($arg->name == 'facility' && $arg->pivot->value != '0')
                    $value = Facility::find($arg->pivot->value)->facility_sn;
                else
                    $value = $arg->pivot->value;
            // }
            $args[$arg->name] = $value; // $arg->pivot->value; // Only line before (within this Foreach loop)
        }
        $args['company'] = empty($args['company']) ? 0 : Auth::user()->company->getKey();
        if (array_key_exists('autodates', $args)) {
            if($args['autodates'] == 1) {
                $args['startdate'] = Carbon::now()->subMonth()->startOfMonth()->subYear()->format('Y-m-d');
                $args['enddate'] = Carbon::now()->subMonths(2)->endOfMonth()->format('Y-m-d');
            }
            unset($args['autodates']);
        }
        return sprintf('https://keygreensolutions.com/cgi-bin/greencgi?%s', http_build_query($args));
    }

    public function getReportSubtitle ()
    {
        $subtitle = '';
        foreach($this->args as $arg) :
            if($arg->type != 'system' && $arg->name != 'autodates' && ! empty($arg->pivot->value)) :

                $subtitle .= ' <span class="text-gray">' . $arg->label . '</span>';

                if ($arg->pivot->value == '' || $arg->pivot->value == '0')
                    $subtitle .= ' <strong>None</strong>'; 
                elseif ($arg->type == 'month')
                    $subtitle .= ' <strong>' . date('M', strtotime(sprintf('2015-%s-01', $arg->pivot->value))) . '</strong>'; 
                elseif ($arg->name == 'facility' && $arg->pivot->value != '0')
                    $subtitle .= ' <strong>' . Facility::find($arg->pivot->value)->facility_name . '</strong>'; 
                else
                    $subtitle .= ' <strong>' . $arg->pivot->value . '</strong>'; 

            endif;
        endforeach;

        return $subtitle;
    }

    /**
     * [getIframeEmbedAttribute description]
     * @return [type] [description]
     */
    public function getIframeEmbedAttribute()
    {
        $args = [];
        foreach ($this->args as $arg) {
            $args[$arg->name] = $arg->name == 'format' ? 'pdf' : $arg->pivot->value;
            $args['test'] = 'bledh!';
        }
        $args['company'] = $args['company'] == '0' ? 0 : $this->user->company->getKey();
        if (array_key_exists('autodates', $args)) {
            if($args['autodates'] == 1) {
                $args['startdate'] = Carbon::now()->subMonth()->startOfMonth()->subYear()->format('Y-m-d');
                $args['enddate'] = Carbon::now()->subMonths(2)->endOfMonth()->format('Y-m-d');
            }
            unset($args['autodates']);
        }
        return sprintf('https://%s/cgi-bin/greencgi?%s', $_SERVER['HTTP_HOST'], http_build_query($args));
    }
}
