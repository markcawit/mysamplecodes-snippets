var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
process.env.NODE_ENV='production';

elixir(function(mix) {
	mix.sass(['app.scss']
    		, 'resources/assets/css/custom.css')
    	.styles(['style.css', 'animate.css', 'custom.css']
    		, 'public/assets/css/custom.styles.css')
    	.styles(['../../limitless/css/bootstrap.css',
    			'../../limitless/css/colors.css',
    			'../../limitless/css/components.css',
    			'../../limitless/css/core.css']
    		, 'public/assets/css/limitless.styles.css')
		.scripts(['app.js']
			, 'public/assets/js/custom.app.js')
		.scripts(['bookmark/bookmark_bubble.js',
				'bookmark/example.js',
				'bookmark/jquery.mobile.min.js']
			, 'public/js/bookmark.js')
		.scripts('add-homescreen-popup/*'
			, 'public/js/add-homescreen-popup.js')
		.scripts(['routes/pages.js']
			, 'public/assets/js/routes.js')
	    .copy('resources/limitless/css/extras', 'public/assets/css/extras')
	    .copy('resources/limitless/css/icons', 'public/assets/css/icons')
	    .copy('resources/limitless/img', 'public/assets/img')
	    .copy('resources/limitless/images', 'public/assets/images')
	    .copy('resources/limitless/js', 'public/assets/js')
	    .copy('resources/limitless/locales', 'public/assets/locales')
	    .copy('resources/limitless/swf', 'public/assets/swf');

	mix.browserify('main.js');
});
