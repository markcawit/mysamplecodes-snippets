$(function() {
    // Default initialization

    $('ul.navigation-main > li > a').on('click', function(){
    	$('.sidebar-mobile-main-toggle').click();
    });


    $('#bookmark_bubble').on('click', function(e){
    	e.preventDefault();
    	resetBookmarkBubble(); // To reset the Bookmark Bubble on mobile
    });

    function receiveMessage(e) {
        $(e.data.id).height(e.data.height);
    }
    window.addEventListener("message", receiveMessage, false);
});

function resetBookmarkBubble()
{
	var key = google.bookmarkbubble.Bubble.prototype.LOCAL_STORAGE_PREFIX
                + google.bookmarkbubble.Bubble.prototype.DISMISSED_;
	window.localStorage[key] = String(0);
}
