    import Vue from 'vue'
    import VueRouter from 'vue-router'
    import VueResource from 'vue-resource'
    import VueValidator from 'vue-validator'
    import dashboard_view from './components/dashboard.vue'
    import energy_view from './components/energy.vue'
    import waste_view from './components/waste.vue'
    import food_view from './components/food.vue'
    import purchasing_view from './components/purchasing.vue'
    import cannot_be_found from './components/cannot-be-found.vue'
    import login_view from './components/login.vue'
    import request_resetpass_view from './components/request-resetpass-view.vue'
    import resetpass_view from './components/resetpass-view.vue'

    // Vue.config.devtools = true;
    // Vue.config.debug = false;

    Vue.use(VueRouter)
    Vue.use(VueResource)
    Vue.use(VueValidator)


    // Global Declaration of CSRF Token
    var token = document.querySelector('input[name="_token"]');
    if (token) {
        Vue.http.headers.common['X-CSRF-TOKEN'] =  token.value;
        Vue.http.options.emulateJSON = true;
    }

    Vue.directive('ajax', {
        bind: function() {
            this.el.addEventListener('submit', this.onSubmit.bind(this));
        },
        update: function( value ) {
            if (value) {
                alert(value);
            }
        },
        onSubmit: function(e) {
            e.preventDefault();

            this.vm
                .$http[this.getRequestType()](this.el.action, { username: this.vm.username, password: this.vm.password })
                .then(this.onComplete.bind(this))
                .catch(this.onError.bind(this));
        },
        onComplete: function(response) {
            var data = response.data;
            console.log('VMo: '+this.vm.access);
            if (data.access) {
                console.log(data);
                console.log(this.vm);

                console.log('VMi: '+this.vm.access);

                this.vm.loader = true;
                window.setTimeout( function() {
                    window.location.reload();
                }.bind(this), 1000);
            }
            else {
                this.vm.login_err = true;
                this.vm.login_msg = response.data.message;
            }
        },
        onError: function(response) {
            console.log(response);
            this.displayLoginError(response.data);
        },
        getRequestType: function() {
            var method = this.el.querySelector('input[name="_method"]');
            return (method ? method.value : this.el.method).toLowerCase();
        },
        displayLoginError: function(data) {
            // Display logged in error messages
            this.vm.login_err = true;
            var msg = '';
            if (data.username)
                msg += data.username[0];

            if (data.password)
                msg += (msg === '') ? data.password[0] : ' and '+data.password[0];

            this.vm.login_msg = msg + ' can\'t be blank';
        },
        clearLoginData: function() {
        }
    })

    Vue.directive('ajax-uni', {
        bind: function() {
            this.el.addEventListener('submit', this.onSubmit.bind(this));
        },
        update: function( value ) {
            if (value) {
                alert(value);
            }
        },
        onSubmit: function(e) {
            e.preventDefault();

            var fields = this.vm.fields;
            this.vm
                .$http[this.getRequestType()]( this.el.action, fields  )
                .then(this.onComplete.bind(this))
                .catch(this.onError.bind(this));
        },
        onComplete: function(response) {
            console.log(response.data);
            var res = response.data;

            this.vm.submit_msg = res.message;
            if (!res.error) {
                this.vm.submit_suc = true;
                this.vm.submit_err = false;
            }
            else {
                this.vm.submit_suc = false;
                this.vm.submit_err = true;
            }
        },
        onError: function(response) {
            console.log(response);
        },
        getRequestType: function() {
            var method = this.el.querySelector('input[name="_method"]');
            return (method ? method.value : this.el.method).toLowerCase();
        }
    })


    var App = Vue.extend({
        data: function() {
            return {
                appName: 'Key Green Solutions',
                title: ' ',
                user: null,
                access: window.access,
                username: '',
                password: '',
                login_err: 0,
                login_msg: '',
                loader: true,
                add_btn: false,
                page_title: '',
                login: false,
            };
        },
        created: function() {
            this.checkAccess();
        },
        ready: function() {
            window.setTimeout( function() {
                this.loader = false;
            }.bind(this), 500);
        },
        methods: {
            checkAccess: function() {
                this.$http.get('access', function (response) {
                    this.access = response.access;
                    this.user = response.user;
                }.bind(this));
            },
            logout: function() {
                this.$http.get('logout', function (response) {
                    this.loader = true;
                    window.setTimeout( function() {
                        window.location.reload();
                    }.bind(this), 500);
                }.bind(this));
            },

            addReport: function() {
                this.$children[0].addReportForm();
            }
        },
    })

    var router = new VueRouter()

    router.map({
        '/login': {
            component: login_view,
            name: 'login',
            title: 'Login | ',
            auth: false
        },
        '/password': {
            component: request_resetpass_view,
            name: 'forgotpass',
            title: 'Request Password Reset | ',
            auth: false,
        },
        '/password-reset/:token/:email': {
            component: resetpass_view,
            name: 'resetpass',
            title: 'Reset Password | ',
            auth: false,
        },
        '/energy': {
            component: energy_view,
            name: 'energy',
            title: 'Energy | ',
            auth: true,
            add_btn: true,
            pageTitle: '<i class="glyphicon glyphicon-globe"></i> Energy/Water'
        },
        '/waste': {
            component: waste_view,
            name: 'waste',
            title: 'Waste | ',
            auth: true,
            add_btn: true,
            pageTitle: '<i class="glyphicon glyphicon-trash"></i> Waste'
        },
        '/food': {
            component: food_view,
            name: 'food',
            title: 'Food | ',
            auth: true,
            add_btn: true,
            pageTitle: '<i class="glyphicon glyphicon-cutlery"></i> Food'
        },
        '/purchasing': {
            component: purchasing_view,
            name: 'purchasing',
            title: 'Purchasing | ',
            auth: true,
            add_btn: true,
            pageTitle: '<i class="glyphicon glyphicon-shopping-cart"></i> Purchasing'
        },
        '*': {
            component: cannot_be_found,
            name: 'not-found',
            title: '404 | ',
            auth: false
        },
        '/': {
            component: dashboard_view,
            name: 'dashboard',
            title: 'Dashboard | ',
            auth: true,
            pageTitle: '<i class="icon-home2 position-left"></i> Sustainability Dashboard'
        },
    })

    router.beforeEach(function (transition) {
        // Authentical Session Access
        router.app.checkAccess();

        var authenticated = router.app.access;
        if (transition.to.path == '/login' && authenticated) {
            transition.redirect('/');
        }else if (transition.to.path == '/' && !authenticated) {
            router.app.login = true;
            transition.redirect('/login');
        }

        if (transition.to.auth && !authenticated) {
            transition.redirect('/');
        }

        //Showing of Add Report button
        router.app.add_btn = transition.to.add_btn;
        router.app.page_title = transition.to.pageTitle;

        router.app.title = (authenticated) ? transition.to.title+router.app.appName : 'Login | '+router.app.appName;
        transition.next();
    })

    router.start(App, '#app')