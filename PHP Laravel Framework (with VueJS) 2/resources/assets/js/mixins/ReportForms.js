export default {
    data() {
        return {
            dashboard: false,
            report: 0,
            company: 0,

            args: [],
            report_options: [],
            facility_options: [],
            report_args: [],
            group_args: [],
            group_options: [],
            add_report: false,
            fields: [],

            notify: false,
            notifyMsg: '',
        };
    },

	created() {
		// ReportForms mixins created.
	},

    ready() {
        this.$root.$http.get('get-report/'+this.category, function (response) {
                this.report_options = response.reports;
                this.facility_options = response.facilities;
                this.report_args = response.args;
                this.group_args = response.groups;

                this.company = response.company;
            }.bind(this));
    },

	methods: {
		initializeReport() {
            this.notificationOff();
            this.toggleReportForm();
        },

        toggleReportForm() {
            // Show or Hide Report Form
            this.add_report = !this.add_report;
        },

        toggleOffReportForm() {
            this.add_report = 0;
        },

        selectedReport() {
            var id = this.report;
            this.args = this.report_args[id];

            this.fields = [];
            this.dashboard = false;
            var args = this.args;

            // populated data fields from args
            for (var i=0; i < args.length; i++) {
                this.fields.push({
                        arg_sn: args[i].arg_sn,
                        label: args[i].label,
                        name: args[i].name,
                        type: args[i].type,
                        value: args[i].pivot.default,
                    });
            }
        },

        selectedFacility(e) {
            var facility = e.target.value,
                company = this.company;
            var index = company + '-' + facility;

            this.group_options = this.group_args[index];
        },

        saveReport() {
        	// Report Forms is saving.
            var data = {
                category: this.category,
                dashboard: this.dashboard,
                report: this.report,
                args: this.args,
                fields: this.fields,
            }

            this.$root
                .$http['post']('save-report', data)
                .then(this.onComplete.bind(this))
                .catch(this.onError.bind(this));            
        },

        onComplete: function(response) {
            this.dashboard = false;
            this.report = 0;
            this.args = [];
            this.fields = [];

            this.notificationAlert('Report added successfully..', true);

            // from iFrameAttributes mixin
            this.fetchReports();
        },
        
        onError: function(response) { 
            console.log(response);
            alert('Something went wrong..')
        },

        notificationAlert(msg, b) {
            this.notify = b;
            this.notifyMsg = msg;

            window.setTimeout( function() {
                this.notificationOff();
            }.bind(this), 5000);
        },
        notificationOff() {
            this.notify = false;
        }

	},

};
