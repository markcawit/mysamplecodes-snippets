export default {
    methods: {
        convertStringToHTML(str) {
            var opentag = '<';
            var closetag = '>';

            str = str.replace("&lt", opentag);
            str = str.replace("&gt", closetag)

            return str;
        }
    }
}