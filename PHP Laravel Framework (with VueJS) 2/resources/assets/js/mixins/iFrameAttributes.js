export default {
	data() {
		return {
            attributes: [],
            empty_attributes: false,
            ifHeight: 200,
            ifWidth: '100%',
		};
	},

	created() {
		// iFrameAttibutes mixins created.
	},

    watch: {
        'attributes': function(val, oldval) {
            if (val.length > 0) {
                $.each(val, function(key, attr) {
                    var id = '#iframe_' + attr.id;
                    $(id).unbind('load').on('load', function() {
                        this.contentWindow.postMessage({id:id}, 'https://keygreensolutions.com');
                    });
                });
            }
        }
    },

	ready() {
		this.fetchReports();
	},

	methods: {
		fetchReports() {
			this.attributes = [];
			var cat = (this.category) ? '/'+this.category : '';
			this.$root.$http.get('fetch-reports'+cat, function (data) {
					if (data.length) {
						for (var i=0; i < data.length; i++) {
							this.attributes.push({
									id: data[i].report_user_sn,
									title: data[i].title,
									sub_title: data[i].sub_title, //convertStringToHTML(data[i].sub_title),
									params: data[i].params,
									value: data[i].value,
								});
						}
						this.empty_attributes = false;
					}
					else {
						this.empty_attributes = true;
					}

		            this.add_report = false;
	            }.bind(this));
		},

		removeReport( id ) {
			var ok = confirm("Are you sure you want to delete Report?");

			if (ok) {
				this.$root.$http.get('delete-report/'+id, function (data) {
						this.attributes = [];
						this.fetchReports(); // Fetch the updated reports attribute
		            }.bind(this))
				.catch(this.onErrorDelete.bind(this));
			}
		},

		onErrorDelete(response) {
			alert('Something went wrong..');
		},
	},

};
