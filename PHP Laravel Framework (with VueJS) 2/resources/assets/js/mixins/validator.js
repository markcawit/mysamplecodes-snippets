export default {
    validators: { // `numeric` and `url` custom validator is local registration
        numeric: function (val/*,rule*/) {
            return /^[-+]?[0-9]+$/.test(val)
        },
        url: function (val) {
            return /^(http\:\/\/|https\:\/\/)(.{4,})$/.test(val)
        },
        match: function (v, result) {
            // return this.fields.password === this.fields.password_confirmation;
            return result
        }
    },

    methods: {
        invalidFormSubmit() {
            alert('Please fill required fields with *.');
        },
        validClassIndicator() {
            return { 
                valid: 'valid-entry', 
                invalid: 'invalid-entry' 
            };
        },
    }
};