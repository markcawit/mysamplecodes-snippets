	<form action="{{ url('login') }}" method="POST" class="form-validate" v-ajax v-show="!access && login">
		{!! csrf_field() !!}
		<div class="panel panel-body login-form">
			<div class="text-center">
				<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
				<h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="text" class="form-control" id="username" name="username" v-model="username" value="{{ old('username') }}" placeholder="Username">
				<div class="form-control-feedback">
					<i class="icon-user text-muted"></i>
				</div>

                @include('errors.field', ['fieldName' => 'username'])
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="password" class="form-control" id="password" name="password" v-model="password" placeholder="Password">
				<div class="form-control-feedback">
					<i class="icon-lock2 text-muted"></i>
				</div>
                
                @include('errors.field', ['fieldName' => 'password'])
			</div>

			<div class="form-group login-options">
				<div class="row">
					<div class="col-sm-6">
						<label class="checkbox-inline" style="display: none;">
							<input type="checkbox" class="styled" id="remember" name="remember" checked="checked" value="1">
							Remember
						</label>
					</div>

					<div class="col-sm-6 text-right">
						<a v-link="{ name: 'forgotpass' }">Forgot password?</a>
					</div>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-success btn-block">Sign in <i class="icon-circle-right2 position-right" ></i></button>


	            <span class="help-block error-message" v-show="login_err">
	                @{{ login_msg }}
	            </span>
			</div>

		</div>
	</form>