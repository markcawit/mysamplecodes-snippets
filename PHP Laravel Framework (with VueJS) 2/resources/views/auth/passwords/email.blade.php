@extends('layout.keygreen.master')
    
@section('title', 'Request Password Reset - ')

@section('content')

    <form class="form-validate" role="form" method="POST" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}

        <div class="panel panel-body login-form">
            <div class="text-center">
                <h4>Request Password Reset</h4>
            </div>

            <div class="form-group has-feedback has-feedback-left 
                            {{ $errors->has('email') ? ' field-entry-error' : '' }}"
                >
                {!! Form::email('email', old('email'), ['class' => 'required-field form-control', 
                                                          'placeholder'=>'your@example.com'] ); !!}
                <div class="form-control-feedback">
                    <i class="icon-envelope text-muted"></i>
                </div>

                @include('errors.field', ['fieldName' => 'email'])
            </div>
        
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">Send Password Reset Link</button>
            </div>

            @if (session('status'))
            <div class="form-group">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
            @endif
        </div>

    </form>
@stop
