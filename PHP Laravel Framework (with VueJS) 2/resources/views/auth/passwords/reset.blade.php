@extends('layout.keygreen.master')

@section('title', 'Reset Password - ')

@section('content')
    <form class="form-validate" role="form" method="POST" action="{{ url('/password/reset') }}">
        {!! csrf_field() !!}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="panel panel-body login-form">
            <div class="text-center">
                <h4>Reset Password</h4>
            </div>

            <div class="form-group has-feedback has-feedback-left 
                    {{ $errors->has('email') ? ' field-entry-error' : '' }}"
                >
                {!! Form::email('email', old('email', $email), ['class' => 'required-field form-control', 
                                                          'placeholder'=>'your@example.com'] ); !!}
                <div class="form-control-feedback">
                    <i class="icon-envelope text-muted"></i>
                </div>

                @include('errors.field', ['fieldName' => 'email'])
            </div>

            <div class="form-group has-feedback has-feedback-left 
                    {{ $errors->has('password') ? ' field-entry-error' : '' }}"
                >
                <input type="password" placeholder="Password" name="password" value="" class="required-field form-control">
                {{--!! Form::text('password', '', 
                                        [ 'class' => 'required-field form-control', 
                                          'placeholder' => 'Password' ] ); !!--}}
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>

                @include('errors.field', ['fieldName' => 'password'])
            </div>

            <div class="form-group has-feedback has-feedback-left 
                    {{ $errors->has('password_confirmation') ? ' field-entry-error' : '' }}"
                >
                <input type="password" placeholder="Password Confirmation" name="password_confirmation" value="" class="required-field form-control">
                {{--!! Form::text('password_confirmation', '', 
                                        [ 'class' => 'required-field form-control', 
                                          'placeholder'=>'Password Confirmation' ] ); !!--}}
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>

                @include('errors.field', ['fieldName' => 'password_confirmation'])
            </div>

            <div class="form-group">
                    {!! Form::submit('Reset Password', ['class'=>'btn btn-success btn-block']) !!}
            </div>

        </div>
    </form>
@stop
