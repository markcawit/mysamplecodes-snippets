@extends('layout.keygreen.master')

@section('page-title', '<i class="icon-home2 position-left"></i> Sustainability Dashboard')

@section('content')

{{-- 	<router-view>
		@if (Auth::user())
			@include('dashboard.template')
		@endif

	</router-view>
 --}}
	{{-- <report-forms>
		Should display if no content for Report Forms
	</report-forms>

	<report-attrib>
		Should display default content for Report Attributes
	</report-attrib> --}}

	
{{-- 		@include('auth.login')
 --}}	{{--@include('layout.partials.vuetemplates')--}}
@stop

@section('script')

	<!-- Theme JS files -->
	{{-- <script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script> --}}
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js') }}"></script> --}}
	
	<!-- Form UI-->
	{{-- <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom.app.js') }}"></script> --}}

	<!-- /theme JS files -->
@stop 

