@extends('layout.keygreen.master')

@section('title', 'Page Not Found')

@section('content')
    <div class="row column">
        <div class="callout">
            <h4>Page Not Found</h4>
            <p>This Error 404 has been placed due to page not being found in our system.</p>
        </div>
    </div>
@stop