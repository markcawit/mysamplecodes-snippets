        @if ($errors->has($fieldName))
            <span class="help-block error-message">
                {{ $errors->first($fieldName) }}
            </span>
        @endif
