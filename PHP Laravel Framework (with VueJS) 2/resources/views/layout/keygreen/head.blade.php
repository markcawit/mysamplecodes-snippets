<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="Key Green Solutions">

	<title>Key Green Solutions</title>

	<!-- Global stylesheets -->
	{{-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> --}}
	<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/limitless.styles.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Custom stylesheets -->
	<link href="{{ asset('assets/css/custom.styles.css') }}" rel="stylesheet" type="text/css">
	<!-- /custom stylesheets -->

	@yield('header_script')
	<script>
	@if (Auth::user())
		{{ "var access = 1;" }} 
	@else 
		{{ "var access = 0;" }}
	@endif
	</script>
</head>
