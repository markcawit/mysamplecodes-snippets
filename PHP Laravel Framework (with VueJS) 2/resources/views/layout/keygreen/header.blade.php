	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" v-link="{ name: 'dashboard' }"><img src="{{ asset('assets/img/kg-logolg.png') }}" alt=""></a>


			<ul class="nav navbar-nav visible-xs-block">
				<li>
					<a data-toggle="collapse" data-target="#navbar-mobile">
						<i class="icon-cog5" v-if="access"></i>
						<i class="icon-paragraph-justify3" v-if="!access"></i>
					</a>
				</li>
				<li v-if="access">
					<a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a>
				</li>
			</ul>

		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav" v-if="access">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user" v-if="access">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<!-- <img src="assets/images/placeholder.jpg" alt=""> -->
						<span v-if="user">@{{ user.people.people_first }} @{{ user.people.people_last }}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<!-- <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li> -->
						<li><a @click="logout"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
				<li v-if="!access">
					<a v-link="{ path: '/' }">Login</a>
					<!-- #!login -->
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
