<!DOCTYPE html>
<html id="app" lang="en-US">
    @include('layout.keygreen.head')

    <body>
		<div id="loading_bg" v-if="loader" transition="fade" transition-mode="out-in">{{--  transition="preloader" --}}
			<img id="loader_logo" src="{{ asset('assets/img/kg-logolg.png') }}">
		</div>

        @include('layout.keygreen.header')

		<!-- Page container -->
		<div v-else class="page-container" transition="fade" transition-mode="out-in">

			<!-- Page content -->
			<div class="page-content">
				
			{{-- @if (Auth::user()) --}}
				@include('layout.keygreen.sidebar')
	        {{-- @endif --}}

				<!-- Main content -->
				<div class="content-wrapper">
				{{-- @if (Auth::user())	--}}
					@include('layout.keygreen.page-header')
				{{-- @endif --}}

					<!-- Content area -->
					<div class="content" transition="fade">
						
	        			@yield('content')
						<router-view>
							@if (Auth::user())
								@include('dashboard.template')
							@endif

						</router-view>

						@include('auth.login')
						@include('layout.keygreen.footer')

					</div>
					<!-- /content area -->

				</div>
				<!-- /main content -->
			<div data-role="page" id="bm-index">
			</div>
			<!-- /page content -->

			<!-- App JS file -->
			<script src="{{ asset('js/main.js')}}"></script>

			<!-- Core JS files -->
			{{-- <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script> --}}
			<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
			{{-- <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script> --}}
			<!-- /core JS files -->
			
			<script src="{{ asset('assets/js/custom.app.js') }}"></script>
		    {{-- <script src="{{ asset('js/bookmark.js') }}"></script> --}}
		    {{-- <script src="{{ asset('js/add-homescreen-popup.js') }}"></script> --}}

			<link rel="stylesheet" type="text/css" href="{{ asset('add-to-homescreen/style/addtohomescreen.css') }}">
			{{-- <script src="{{ asset('add-to-homescreen/src/addtohomescreen.js') }}"></script>
			<script>
			addToHomescreen();
			</script> --}}


			<!-- Theme JS files -->
			<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>

			<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
			@yield('script')
    </body>
</html>