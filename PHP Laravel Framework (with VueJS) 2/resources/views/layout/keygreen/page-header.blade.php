					<!-- Page header -->
					<div class="page-header page-header-default" v-if="access">
						<div class="page-header-content">
							<div class="page-title">
								<h4>
									@{{{ page_title }}}
								</h4>
							</div>

							
							<div class="breadcrumb-line" v-if="add_btn">
								<ul class="breadcrumb">
									<li>
										<a @click="addReport"><i class="icon-plus-circle2 position-left"></i> Add A Report</a>
									</li>
								</ul>
							</div>
						</div>

					</div>
					<!-- /page header -->

					<!-- Content area -->
					<div class="content" transition="fade">
