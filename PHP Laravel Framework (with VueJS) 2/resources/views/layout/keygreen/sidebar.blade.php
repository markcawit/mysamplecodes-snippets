			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" v-if="access">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<!-- <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a> -->
								<div class="media-body">
									<span class="media-heading text-semibold" v-if="user">@{{ user.company.company_name }}</span>
									<div class="text-size-mini text-muted">
										To add reports to your dashboard, choose a category below to begin.
									</div>
								</div>

								<!-- <div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header">
									<span>Report Categories</span>
									<i class="icon-menu" title="" data-original-title="Main pages"></i>
								</li>{{--  v-link="{name: 'dashboard', activeClass: 'active', exact: true}" --}}
								<li v-link-active>
									<a v-link="{ path: '/', exact: true }">
									   <i class="icon-home4"></i><span>Dashboard</span>
                                    </a>
								</li>
								<li v-link-active>
									<a v-link="{ path: '/energy' }">
									<i class="glyphicon glyphicon-globe"></i> <span>Energy/Water</span></a>
								</li>
								<li v-link-active>
									<a v-link="{ path: '/waste' }">
									<i class="glyphicon glyphicon-trash"></i> <span>Waste</span></a>
								</li>
								<li v-link-active>
									<a v-link="{ path: '/food' }">
									<i class="glyphicon glyphicon-cutlery"></i> <span>Food</span></a>
								</li>
								<li v-link-active>
									<a v-link="{ path: '/purchasing' }">
									<i class="glyphicon glyphicon-shopping-cart"></i> <span>Purchasing</span></a>
								</li>
    						</ul>

						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
