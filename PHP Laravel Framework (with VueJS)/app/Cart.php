<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coupon'
    ];

    protected $appends = ['formatted_payment_date', 'invoice_number'];

    public function getFormattedPaymentDateAttribute()
    {
        return Carbon::parse($this->payment_date)->format('F j, Y');
    }

    public function getInvoiceNumberAttribute()
    {
        return $this->id .'-'. $this->user_id .'-'. strtotime(Carbon::parse($this->created_at));
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cartItems($type = 'snack')
    {
        return $this->hasMany('App\CartItem')->whereType($type);
    }

    public function cartItemsAmenities()
    {
        return $this->hasMany('App\CartItem')->whereType('amenity');
    }

    public function cartItemsAddPlans()
    {
        return $this->hasMany('App\CartItem')->whereType('plan');
    }

}
