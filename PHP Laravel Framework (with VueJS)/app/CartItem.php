<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'product_id', 'cart_id', 'quantity', 'price', 'total', 'type'
    ];

	public function cart()
    {
        return $this->belongsTo('App\Cart');
    }
 
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
 
    public function Amenity()
    {
        // Bypass Foreign key product_id to "Amenities ID" instead of "Products ID"
        return $this->belongsTo('App\Amenity', 'product_id', 'id');
    }
 
    public function Membership()
    {
        // Bypass Foreign key product_id to "Membership ID" instead of "Products ID"
        return $this->belongsTo('App\Membership', 'product_id', 'id');
    }
}
