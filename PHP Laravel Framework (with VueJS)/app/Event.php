<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Carbon\Carbon;
use Auth;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_title', 'description', 'start', 'end', 'all_day', 'color', 'location_id', 'user_id'
    ];

    public $NumOfDays = 3;
    protected $appends = ['title', 'event_date', 'start_time', 'end_time'];

    public function User()
    {
    	return $this->belongsTo(User::class);
    }  

    public function Location()
    {
    	return $this->belongsTo(Location::class);
    }

    public function getTitleAttribute()
    {  
        return $this->event_title . ' - ' . Str::words($this->description, 20);
    }

    public function getEventDateAttribute()
    {
        return Carbon::parse($this->start)->format('Y-m-d');
    }

    public function getStartTimeAttribute()
    {
        return Carbon::parse($this->start)->format('H:i');
    }

    public function formatedTime( $value )
    {
        return Carbon::parse($value)->format('h:i A');
    }

    public function formatedDate( $value )
    {
        return Carbon::parse($value)->format('F j, Y');
    }

    public function getEndTimeAttribute()
    {
        return Carbon::parse($this->end)->format('H:i');
    }

    public function formatEventDate()
    {
        if (Carbon::parse($this->start)->format('H:i:s') == '00:00:00') {
            return Carbon::parse($this->start)->format('F j, Y') . ' - Whole Day';
        }

        return Carbon::parse($this->start)->format('F j, Y h:i A');
    }

    public function getDescriptionExcerpt() 
    {
        return Str::words($this->description, 10);
    }

    public function scopeNextTwoDays($query) 
    {
        $from = Carbon::now();
        $to = Carbon::now()->addDays($this->NumOfDays);

        return $query->where('start', '>=', $from)
                        ->where('start', '<=', $to)
                        ->orderBy('start', 'ASC')->get();
    }

}
