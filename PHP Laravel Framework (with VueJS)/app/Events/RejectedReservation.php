<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RejectedReservation extends Event
{
    use SerializesModels;

    public $inputs;
    public $user;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($input, $user)
    {
        $this->inputs = $input;
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
