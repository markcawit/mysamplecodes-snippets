<?php
// use App\RoleUser;
// use App\Role;
// use Auth;

class Helpers {

	public static function getUserRole()
	{	
		$Id = auth()->user()->id;//Illuminate\Support\Facades\Auth::user()->id;

		$userRole = App\RoleUser::where(['user_id' => $Id])->first();
		return App\Role::where('id', $userRole['role_id'])->first()->name;
	}

	public static function breadcrumbs($pages = [], $active = 'dashboard') 
	{
		$output = "<ul class=\"breadcrumb\">";

		if ($active == 'dashboard')
			$output .= "<li class=\"active\">Dashboard</li>";
		else
			$output .= "<li><a href=\"". route('dashboard') . "\">Dashboard</a></li>";

		foreach ($pages as $route => $value) {
			$text = $value;
			
			if (is_array($value)) {
				$text = $value[0];
				$params = $value[1];
			}

			if ($active == $route)
				$output .= "<li class=\"active\">$text</li>";
			else {
				if (!is_array($value)) 
					$output .= "<li><a href=\"". route($route) . "\">$text</a></li>";
				else {
					$output .= "<li><a href=\"". route($route, $params) . "\">$text</a></li>";
				}
			}
		}
		$output .= "</ul>";

		return $output;
	}
}

function convert_to_array ($str)
{
	$str = lrtrim($str);
	$array = explode(',', $str);

	$new_str_array = [];
	foreach ($array as $key => $string) {
		$a = explode(':', $string);
		$new_str_array[ lrtrim($a[0], '"', '"') ] = lrtrim($a[1], '"', '"');
	}

	return json_decode(json_encode($new_str_array));
}

function lrtrim($str, $lneddle="{", $rneddle="}")
{
	$str = ltrim($str, $lneddle);
	$str = rtrim($str, $rneddle);

	return $str;
}

function currency ($num, $cur = '$') {
	return $cur . format_currency($num) . currency_code();
}

function currency_code()
{
    return ' USD';
}

function format_currency ($num, $dec = 2, $thou = '.') {
	return number_format($num, $dec, $thou, ",");;
}

function clean_quotes ($str)
{
	$str = str_replace('"', "", $str);
	$str = str_replace("'", "", $str);

	return $str;
}