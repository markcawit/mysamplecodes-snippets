<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RoboPlanner\Helper\ControllerHelper;
use RoboPlanner\Helper\CartHelper;
use Carbon\Carbon;

use App\Http\Requests;
use RoboPlanner\Repositories\UserRepository;
use Auth;
use Gate;
use App\Location;
use App\UserMembership;
use App\LocationUser;
use App\User;
use App\PersonalInfo;

use App\Events\WelcomeUserEvent;

class ClientController extends Controller
{
    use ControllerHelper;
    use CartHelper;

    protected $user;
    protected $auth;
    //
    public function __construct(UserRepository $user)
    {
        $this->middleware('auth');
        $this->auth = Auth::user();
        $this->user = $user;
    }

    public function index(Request $request, $location = null){
        if(Gate::denies('view_clients')){
            return view('roboplanner.errors.403');
        }

        // For the Page Breadcrumbs params
        $this_page              = 'clients';
        $data['breadcrumbs']    = ['clients'=>'Member Management'];
        if ($location) 
            $data['breadcrumbs'] = array_merge($data['breadcrumbs'], ['client_by_location'=>'By Location']);

        $data['active_page']    = ($location) ? 'client_by_location' : $this_page;
            // end user by location breadcrumbs

        $data['search']	        = trim($request->input('search'));
        $data['sort']           = ($request->input('sort') == 'asc')? 'desc' : 'asc';
        $data['page_number']    = $request->input('page');
        $data['location']       = ($location) ? $location : '';
        $data['locations']      = Location::all();

        if ($location)
            $data['users'] = $this->user->clientsByLocation($request, $location);
        else 
            $data['users'] = $this->user->getClients($request);
            
        // return $data;
        return view('clients.index',$data);
    }

    public function plan($id)
    {
        if(Gate::denies('view_clients')){
            return view('roboplanner.errors.403');
        }

    }

    public function profile(Request $request, $id)
    {
        if(Gate::denies('view_client_profile') || Gate::denies('update_account')){
            return view('roboplanner.errors.403');
        }

        if ($request->input()) {
            $profile_input = array( 'age' => $request->input('age'),
                                    'gender' => $request->input('gender') );

            if ($request->file('image')) {
                // Upload image if file input exist
                $profileName = 'profile_picture_' . strtotime(Carbon::now());
                $image_filename = $this->processImage($request->file('image'), $profileName, 'members/');

                $profile_input['image'] = $image_filename;
            }


            PersonalInfo::where('user_id',$id)->update($profile_input);

            $this->user->update([
                                    'first_name' => $request->input('first_name'),
                                    'last_name' => $request->input('last_name'),
                                    'email' => $request->input('email'),
                                ], $id);

            return [$request->input(),$request->file()];
        }
        else {
            $user = $this->user->find($id);

            $data['user'] = $user;

            if (!PersonalInfo::where('user_id',$id)->exists())
                PersonalInfo::create(['user_id'=>$id]);

            $member = UserMembership::where('user_id', $id)->first();
            $data['membership'] = $member->Membership;
            $data['amenities'] = $this->getCart($user->id)->cartItemsAmenities;
            $data['addplans'] = $this->getCart($user->id)->cartItemsAddPlans;

            $data['show_details'] = true;

            return view('clients.profile',$data);
        }
    }

    public function edit($id)
    {
        if(Gate::denies('edit_client')){
            return view('roboplanner.errors.403');
        }

        $client = $this->user->find($id);
        $client->LocationUser; // Get the Member's Location
        $client->UserMembership; // Get the Membership
        return $client;
    }

    public function store(Request $request)
    {
        if(Gate::denies('add_user')){
            return view('roboplanner.errors.403');
        }

        if ( $this->checkIfExist($request->all()['email']) ) {
            return array_merge(
                        $this->makeResponse(1, 'Something goes wrong.', 'add', 'client_management'),
                        $this->notifyResponse('EMAIL NOT AVAILABLE', 'The email you entered was already registered.')
                        );
        }
        else if ( $this->checkIfExist($request->all()['username'], 'username') ) {
            return array_merge(
                        $this->makeResponse(1, 'Something goes wrong.', 'add', 'client_management'),
                        $this->notifyResponse('USERNAME NOT AVAILABLE', 'The username you entered was already registered.')
                        );
        }

        if ( $client = $this->user->save($request) ) {
            $a = $this->makeResponse(0, 'Client added.', 'add', 'client_management', true);
            $clnt = $client->toArray();

            $user = User::find($clnt['id']);
            $clnt['location'] = $user->LocationUser->location->name;
            $membership = $user->UserMembership->membership;
            $clnt['membership'] = $membership->name . ' (' . currency($membership->fee) . ')';

            // Sending Email
            event( new WelcomeUserEvent($clnt) );

            $full_name = $clnt['first_name'] . ' ' . $clnt['last_name'];
            session()->set('success_message', 'New Client has been added. <strong>'.$full_name.' ('.$clnt['email'].')</strong>');
            return array_merge($client->toArray(), $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'client_management');
    }

    public function update($id, Request $request)
    {
        if(Gate::denies('edit_client')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }
        
        $input = $request->except('location', 'membership');

        // Update of Location
        LocationUser::where('user_id', $id)
                ->update(['location_id'=>$request->input('location')]);
        // Update of Membership Plan
        UserMembership::where('user_id', $id)
                ->update(['membership_id'=>$request->input('membership')]);

        if ($this->user->update($input, $id)) {
            session()->set('success_message', 'Client account/profile has been updated.');
            $req = $request->all();
            $a = $this->makeResponse(0, 'Client updated.', 'update', 'client_management', true);
            return array_merge($req, $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'client_management');
    }

    public function destroy($Id){
        if(Gate::denies('delete_client')){
            return view('roboplanner.errors.403');
        }

        $client = $this->user->find($Id);
        $cuser = $client;
        $client->delete();
        
        session()->set('success_message', ucfirst($cuser->username).'\'s account has been deleted.');

        return redirect()->route('clients');
    }
}
