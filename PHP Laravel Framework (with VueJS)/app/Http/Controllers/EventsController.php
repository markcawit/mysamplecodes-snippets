<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RoboPlanner\Repositories\EventCalendarRepository;
use RoboPlanner\Helper\ControllerHelper;

use App\Http\Requests;
use App\Event;
use App\Location;

// use MaddHatter\LaravelFullcalendar\Event;

use Auth;
use Guard;

class EventsController extends Controller
{
    use ControllerHelper;

    protected $event;
	protected $auth;
    
    public function __construct(EventCalendarRepository $event){
		$this->middleware('auth');
		$this->event = $event;
		$this->auth = Auth::user();
		$this->event->setListener($this);
	}
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($Id = 0) {
        $locations = Location::all();
        $locationId = ($Id) ? $Id : $this->auth->LocationUser->location_id;
        return view('events.index', compact('locations', 'locationId'));
    }

    public function getEventsByLocation($Id)
    {
        return Event::where('location_id',$Id)->get();
    }

    public function location($Id) {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(){
        // $array = array();
        // $array['event_name'] = $appointment->event_name;
        // $array['all_day_event'] = $appointment->all_day_event;
        // $array['event_start'] = $appointment->event_start;
        // $array['event_end'] = $appointment->event_end;
        // //$array['backgroundColor'] = $color;
        // $array['location'] = $appointment->location;

        // return view('events.calendar',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $event = new Event();
        $event->event_title = $input['title'];
        $event->description = $input['description'];
        $event->color       = $input['color'];
        $event->location_id = $input['location_id'];
        $event->user_id     = Auth::user()->id;

        if ($input['start_time'] !== "0:00:00") {
            $event->start = $input['date'].' '.$input['start_time'];

            if ($input['end_time'] !== "0:00:00")
                $event->end = $input['date'].' '.$input['end_time'];
        } 
        else {
            // All Day Event
            $event->start       = $input['date'];
            $event->all_day     = true;
        }

        if ($event->save()) {
            \Session::set('success_message', 'New event added successfully.');
            $a = $this->makeResponse(0, 'Event added.', 'add', 'events', true);
            return array_merge($event->toArray(), $a);;
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($Id)
    {
        $event = Event::find($Id);

        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($Id)
    {
        $event = Event::find($Id);

        return $event;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $Id)
    {
        $input = $request->all();
        $event = Event::find($Id);

        $event->event_title = $input['title'];
        $event->description = $input['description'];
        $event->color = $input['color'];
        $event->location_id = $input['location_id'];

        if ($input['start_time'] !== "0:00:00") {
            $event->start = $input['date'].' '.$input['start_time'];

            if ($input['end_time'] !== "0:00:00")
                $event->end = $input['date'].' '.$input['end_time'];
        } 
        else {
            // All Day Event
            $event->start       = $input['date'];
            $event->all_day     = true;
        }

        if ($event->save()) {
            \Session::set('success_message', 'Event successfully updated.');
            $a = $this->makeResponse(0, 'Event updated.', 'update', 'events', true);
            return array_merge($request->all(), $a);;
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id)
    {
        $event = Event::find($Id);
        $locationId = $event->location_id;
        $event->delete();

        return redirect()->route('events', $locationId.'#!/success/Event has been Deleted!');
    }
}
