<?php

namespace App\Http\Controllers\Kiosk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Cart;
use App\CartItem;
use App\UserMembership;

use App\Events\NotifyMemberPayment;

use App\Amenity;
use App\Membership;

use Carbon\Carbon;

use RoboPlanner\Helper\ControllerHelper;
use RoboPlanner\Helper\CartHelper;
use RoboPlanner\Repositories\UserRepository;
use Gate;

class CartController extends Controller
{
    use ControllerHelper;
    use CartHelper;

    protected $user;
    protected $auth;

    public function __construct(UserRepository $user)
    {
        $this->middleware('auth');
        $this->auth = Auth::user();
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = $this->getCart();

        return view('shop.cart.index');
    }

    public function getCartCount() 
    {
        return $this->cartItems();
    }

    /**
     * Store a selected product to cart
     *
     * @param  $productId
     */
    public function addItem (Request $request, $productId)
    {
        $input = $request->all();

        // fetch current unpaid cart
        $cart = $this->getCart();

        $addItem = [
                'product_id' => $productId,
                'cart_id' => $cart->id,
                'quantity' => $input['quantity'],
                'price' => $input['price'],
                'total' => $input['total']
            ];
        $cartItem = CartItem::create($addItem);

        if ($cartItem) {
            $a = $this->makeResponse(0, 'Add item to cart.', 'add', 'snack_shop');
            return array_merge($cartItem->toArray(), $a, ['cartItemCount' => $this->cartItems()]);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'snack_shop');
    }

    public function updateCart(Request $request)
    {
        $input = $request->all();

        foreach ($input['quantity'] as $id => $value) {
            $data['quantity'] = $value;
            $data['total'] = $input['price'][$id] * $value;
            $item = CartItem::where('id', $id)->update($data);
        }

        return $this->makeResponse(0, 'Update item to cart.', 'update', 'snack_shop');
    }

    public function showCart()
    {
        $cart = $this->getCart();

        // Snack items
        $items = $cart->cartItems;
        $total=0;
        foreach($items as $item){
            $total+=$item->total;
        }

        // get discount
        $coupon = $cart->coupon;
        $discount = $cart->discount;
        $total_recurring = 0;

        // Amenity items
        $amenities = $cart->cartItemsAmenities;
        foreach ($amenities as $amenity) {
            $total_recurring+=$amenity->total;
        }

        // Additional Plan Charges
        $addplans = $cart->cartItemsAddPlans;
        foreach ($addplans as $plan) {
            $total_recurring+=$plan->total;
        }

        // Get membership account
        $membership = UserMembership::where( 'user_id', $this->auth->id )->get();
        foreach ($membership as $plan) {
            $total_recurring = $total_recurring + $plan->Membership->fee;
        }
        $total = $total + $total_recurring;

        return view('shop.cart.index', compact(
                                'cart',
                                'items',
                                'amenities',
                                'addplans',
                                'total',
                                'coupon', 
                                'discount', 
                                'membership', 
                                'total_recurring'));
    }

    public function showMemberCart($id)
    {
        $cart = $this->getCart($id);

        $member = User::find($id);
        $cart_name = $member->full_name . "'s";
        // Snack items
        $items = $cart->cartItems;
        $total=0;
        foreach($items as $item){
            $total+=$item->total;
        }

        // get discount
        $coupon = $cart->coupon;
        $discount = $cart->discount;
        $total_recurring = 0;

        // Amenity items
        $amenities = $cart->cartItemsAmenities;
        foreach ($amenities as $amenity) {
            $total_recurring+=$amenity->total;
        }

        // Additional Plan Charges
        $addplans = $cart->cartItemsAddPlans;
        foreach ($addplans as $plan) {
            $total_recurring+=$plan->total;
        }

        // Get membership account
        $membership = UserMembership::where( 'user_id', $id )->get();
        foreach ($membership as $plan) {
            $total_recurring = $total_recurring + $plan->Membership->fee;
        }
        $total = $total + $total_recurring;

        return view('shop.cart.index', compact(
                                'cart',
                                'cart_name',
                                'items',
                                'amenities',
                                'addplans',
                                'total',
                                'coupon',  
                                'discount', 
                                'membership', 
                                'total_recurring'));
    }

    public function removeItem($id)
    {
        CartItem::destroy($id);
        return redirect()->back();
    }

    public function additionalCharges(Request $request)
    {
        if(Gate::denies('add_charges')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }

        $item = '';
        $input = $request->all();
        $cart = $this->getCart($input['user_id']);

        if ($input['membership']) {
            $membership_id = $input['membership'];
            $membership = Membership::find($membership_id);

            $addItem = [
                    'product_id' => $membership_id,
                    'cart_id' => $cart->id,
                    'quantity' => 1,
                    'price' => $membership->fee,
                    'total' => $membership->fee,
                    'type' => 'plan'
                ];
            $cartItem = CartItem::create($addItem);
        }

        if ($input['amenity']) {
            $amenity_id = $input['amenity'];
            $amenities = Amenity::find($amenity_id);

            $addItem = [
                    'product_id' => $amenity_id,
                    'cart_id' => $cart->id,
                    'quantity' => 1,
                    'price' => $amenities->fee,
                    'total' => $amenities->fee,
                    'type' => 'amenity'
                ];
            $cartItem = CartItem::create($addItem);
        }

        if ($cartItem) {
            \Session::set('success_message', 'Additional charges has been added to '.$cart->user->first_name. '\'s Cart.');
            $a = $this->makeResponse(0, 'Add item to cart.', 'add', 'additional_charges', true);
            return array_merge($a, ['cartItemCount' => $this->cartItems()]);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'additional_charges');
    }

    public function fetchCharges()
    {
        return [
            'amenities' => Amenity::all(),
            'memberships' => Membership::all(),
            ];
    }

    public function checkout($id = null)
    {
        $cart = (!$id) ? $this->getCart() : $this->getCartByID($id);

        $user = $this->user->find($cart->user_id);

        // Snack items
        $items = $cart->cartItems;
        $total=0;
        foreach($items as $item){
            $total = ($total + $item->total);
        }
        $total_snacks = $total;
        
        // get discount
        $total_recurring = 0;

        // Amenity items
        $amenities = $cart->cartItemsAmenities;
        foreach ($amenities as $amenity) {
            $total_recurring = ($total_recurring + $amenity->total);
        }

        // Additional Plan Charges
        $addplans = $cart->cartItemsAddPlans;
        foreach ($addplans as $plan) {
            $total_recurring = ($total_recurring + $plan->total);
        }

        // Get membership account
        $membership = UserMembership::where( 'user_id', $cart->user_id )->get();
        foreach ($membership as $plan) {
            $total_recurring = ($total_recurring + $plan->Membership->fee);
        }
        $total = ($total + $total_recurring);
        // return $total;
        $cart->amount_due = $total;
        $cart->save();

        return view('shop.cart.checkout', compact('cart',
                                            'user',
                                            'items',
                                            'amenities',
                                            'addplans',
                                            'discount', 
                                            'membership', 
                                            'total_snacks',
                                            'total', 
                                            'total_recurring'
                                            )
                );
    }

    public function payment(Request $request, $Id)
    {
        $cart = $this->getCartByID($Id);
        $input = $request->all();

        $input['date_time'] = Carbon::now();

        $cart->txn_id = $input['or_number'];
        $cart->payment_date = Carbon::parse($input['or_date'])->format('Y-m-d') . ' ' . Carbon::now()->format('H:i:s');
        $cart->status = 'paid';
        $cart->gateway = 'otc';
        $cart->ref_payment = json_encode($input);
        

        if ($cart->save()) {
            // Generate New Cart for Member
            $newCart = $this->getCart($cart->user_id);

            $notifyArray = ['cart' => $cart, 'input' => $input, 'gateway' => 'otc'];
            // Email Member to notify of payments
            event( new NotifyMemberPayment($notifyArray) );

            $a = $this->makeResponse(0, 'Over-the-Counter Payment.', 'payment', 'cart_manual_checkout', true);
            return array_merge($cart->toArray(), $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'payment', 'cart_manual_checkout');

    }

}
