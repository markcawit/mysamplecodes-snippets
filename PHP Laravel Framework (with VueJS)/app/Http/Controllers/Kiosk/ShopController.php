<?php

namespace App\Http\Controllers\Kiosk;

use Illuminate\Http\Request;
use RoboPlanner\Helper\ControllerHelper;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductCategory;

use Carbon\Carbon;
use Gate;
use Auth;

class ShopController extends Controller
{
    use ControllerHelper;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( !Auth::user() ) {
            return redirect('login');
        }

        $products = Product::all();
        return view('shop.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::denies('add_product')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }

        $current_time = Carbon::now(); //->toDayDateTimeString();
        $file = $request;

        $productName = str_slug($request->input('product_name'), '-') . '_' . strtotime($current_time);
        $image_filename = ($file->file('image')) ? $this->processImage($file->file('image'), $productName) : null;

        $input = $request->all();
        $input['image'] = $image_filename;
        $product = Product::create($input);

        if ($product) {
            \Session::set('success_message', ucfirst($input['product_name']).' has been successfully added to Products list.');
            $req = $product->toArray();
            $a = $this->makeResponse(0, 'Product added.', 'addnew', 'shop_dashboard', true);
            return array_merge($req, $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'addnew', 'shop_dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($Id)
    {
        $product = Product::find($Id);
        return $product->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::denies('update_product')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }
        $current_time = Carbon::now();
        $file = $request;

        if ($file->file('image')) { // Upload image if exist
            $productName = str_slug($request->input('product_name'), '-') . '_' . strtotime($current_time);
            $image_filename = $this->processImage($file->file('image'), $productName);
        }
        
        $input = $request->all();
        $product = Product::find($id);
        $product->product_name = $input['product_name'];
        $product->sku = $input['sku'];
        $product->price = $input['price'];
        $product->category_id = $input['category_id'];

        if ($file->file('image')) // Update only if new image uploaded
            $product->image = $image_filename;

        if ($product->save()) {
            \Session::set('success_message', 'Product item updated successfully.');
            $req = $input;
            $a = $this->makeResponse(0, 'Product updated.', 'update', 'shop_dashboard', true);
            return array_merge($req, $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'shop_dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $products = Product::all();
        $search = '';
        return view('shop.dashboard.index', compact('products', 'search'));
    }

    /**
     * Deletes the select Product Id from Database
     *
     * @param int $Id 
     * @return \Illuminate\Http\Response
     */
    public function deleteProduct($Id)
    {
        Product::destroy($Id);
        \Session::set('success_message', 'Product item has been removed from Product list.');
        return redirect()->route('shop-dashboard');
    }

    public function fetchCategories()
    {
        return ProductCategory::all();
    }

    public function addCategory(Request $request)
    {
        $input = $request->all();

        $validation = ProductCategory::where('category_name', $input['category_name'])->get();
        if ($validation->count() > 0) {
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'add', 'product_category'),
                $this->notifyResponse('EXISTING CATEGORY', 'The category you want to add is already existing.')
                );
        }

        $category = ProductCategory::create($input);


        if ($category) {
            $a = $this->makeResponse(0, 'Product Category Added', 'add', 'product_category');
            return array_merge($category->toArray(), $a);
        }
        else {
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'add', 'product_category'),
                $this->notifyResponse('FATAL ERROR', 'Something goes wrong with add of category.\nPlease contact system administrator.')
                );
        }
    }

    public function editCategory($Id)
    {
        $category = ProductCategory::find($Id);
        return $category->toArray();
    }

    public function updateCategory(Request $request, $Id)
    {
        $input = $request->all();

        $validation = ProductCategory::where('category_name', '=', $input['category_name'])
                                     ->where('id', '!=', $Id)->get();

        if ($validation->count() > 0) {
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'add', 'product_category'),
                $this->notifyResponse('EXISTING CATEGORY', 'The category you want to add is already existing.')
                );
        }
        
        $category = ProductCategory::find($Id);
        $category->category_name = $input['category_name'];

        if ( $category->save() ) {
            $a = $this->makeResponse(0, 'Product Category Updated--', 'update', 'product_category');
            return array_merge($category->toArray(), $a);
        }
        else {
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'update', 'product_category'),
                $this->notifyResponse('FATAL ERROR', 'Something goes wrong with updateing of category.\nPlease contact system administrator.')
                );
        }
    }

    public function removeCategory($Id)
    {
        ProductCategory::destroy($Id);
        return $this->makeResponse(1, 'Product Category Removed', 'delete', 'product_category');
    }


}
