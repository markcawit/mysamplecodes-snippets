<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use RoboPlanner\Helper\CartHelper;
use RoboPlanner\Helper\ControllerHelper;
use RoboPlanner\Repositories\UserRepository;

use App\Location;
use App\Membership;
use Auth;
use Gate;

class LocationController extends Controller
{
    use ControllerHelper;

    protected $user;
    protected $auth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('auth');
        $this->auth = Auth::user();
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->hasRole('administrator')){
            return view('roboplanner.errors.403');
        }
    	
        $locations = Location::all();
        return view('location.index', compact('locations'));
    }

    public function getUsers($Id)
    {
        if(!Auth::user()->hasRole('administrator')){
            return view('roboplanner.errors.403');
        }


    }

    public function edit($Id){
        if(Gate::denies('edit_location')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }

        $location = Location::find($Id);
        return $location;
    }

    public function store(Request $request)
    {
        if(Gate::denies('add_location')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }

        if ( $location = Location::create($request->all())->toArray() ) {
            \Session::set('success_message', 'Location successfully added.');

            $a = $this->makeResponse(0, 'Location added.', 'add', 'location', true);
            return array_merge($location, $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'location');
    }

    public function update($Id, Request $request)
    {
        if(Gate::denies('edit_location')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }
        
        $input = $request->all();

        $location = Location::find($Id);
        $location['location_name'] = $input['location_name'];
        $location['address'] = $input['address'];

        if ($location->save()) {
            \Session::set('success_message', 'Location successfully updated.');

            $req = $request->all();
            $a = $this->makeResponse(0, 'Location updated.', 'update', 'location', true);
            return array_merge($req, $a);;
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'location');
    }

    private function makeResponse ($error, $message, $action, $reload = false) {
        return [
                'error'=>$error, 
                'module'=>'location', 
                'action'=>$action, 
                'message'=>$message,
                'reload'=>$reload,
            ];
    }

    public function fetchLocations()
    {
        return [
            'locations' => Location::all(),
            'memberships' => Membership::all(),
            ];
    }


    /**
     * Soft deleting the location to trash
     *
     * @param int $Id
     * @return \Illuminate\Http\Response
     */
    public function trash($Id)
    {
        if(Gate::denies('edit_location')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }
        
        $location = Location::find($Id);
        $location->delete();

        \Session::set('success_message', 'Location has been deleted successfully.');

        return redirect()->route('location');
    }
}
