<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use RoboPlanner\Repositories\LogRepository;

class LogController extends Controller
{
    //
    protected $log;
    protected $auth;
    private $message;

    public function __construct(LogRepository $log)
    {
        $this->log = $log;
        $this->log->setListener($this);
    }

    public function index(Request $request){
        $data['logs']   = $this->log->getLogs($request);
//        return view('roboplanner.errors.403');

        $data['search']	= trim($request->input('search'));
        $data['sort']   = ($request->input('sort') == 'asc')? 'desc' : 'asc';
        $data['page_number']= $request->input('page');
        return view('logs.index', $data);
    }

    public function getUserLog($id){

    }

    public function delete($id){

    }
}
