<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RoboPlanner\Helper\ControllerHelper;

use App\Http\Requests;

use App\Membership;
use Gate;

class MembershipController extends Controller
{
    use ControllerHelper;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('edit_membership')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }  

        $membership = Membership::all();

        return view('membership.index', compact('membership'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::denies('add_membership')){
            return view('roboplanner.errors.403');
        }

        if ( $plan = Membership::create($request->all()) ) {
            \Session::set('success_message', "New Membership Plan has been added successfully.");
            $a = $this->makeResponse(0, 'Membership added.', 'add', 'membership_management', true);

            return array_merge($plan->toArray(), $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'membership_management');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('edit_membership')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }  

        $plan = Membership::find($id);
        return $plan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::denies('edit_membership')){
            return ['error'=>1, 'message'=>'Access Denied'];
        }  
        
        $input = $request->all();

        $plan = Membership::find($id);

        if ($plan->update($input)) {
            \Session::set('success_message', "Membership Plan has been successfully updated.");
            $req = $request->all();
            $a = $this->makeResponse(0, 'Membership updated.', 'update', 'membership_management', true);
            return array_merge($req, $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'membership_management');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('delete_membership')){
            return view('roboplanner.errors.403');
        }

        $plan = Membership::find($id);
        $plan->delete();

        \Session::set('success_message', "Membership Plan has been successfully deleted.");
        return redirect()->route('membership');
    }
}
