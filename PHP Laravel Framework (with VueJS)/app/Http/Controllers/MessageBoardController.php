<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RoboPlanner\Helper\ControllerHelper;
use RoboPlanner\Repositories\UserRepository;

use App\Http\Requests;

use App\Post;
use App\PostCategory;
use Auth;
use Gate;
use Validator;


class MessageBoardController extends Controller
{
    use ControllerHelper;

    protected $user;
    protected $auth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        // $this->middleware('auth');
        $this->auth = Auth::user();
        $this->user = $user;
    }

    public function index()
    {
    	$search = 0;
        $posts = Post::orderBy('pinned', 'ASC')
                    ->orderBy('user_id', 'ASC')
                    ->latest()->get();

        return view('messages.index', compact('posts', 'search'));
    }

    public function create()
    {
        $categories = PostCategory::all();

        return view('messages.create', compact('categories'));
    }

    public function edit($Id){
        $post = Post::find($Id);

        $allowedEdit = (Auth::user()->hasRole('administrator')) ? true : false;
        $allowedEdit = ($this->auth->id == $post->user_id) ? true : $allowedEdit;

        if (!$allowedEdit) {
            \Session::flash('denied_message', 'Ooops! You don\'t have permission to edit this message.');
            return view('messages.show', compact('post'));
        }

        $categories = PostCategory::all();
        
        return view('messages.edit', compact('categories', 'post'));
    }

    public function store(Request $request)
    {
        // if(Gate::denies('add_location')){
        //     return ['error'=>1, 'message'=>'Access Denied'];
        // }
        $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'content' => 'required',
            ]);

        if ( $validator->fails() ) {
            \Session::set('denied_message', 'Please fill all required (*) fields.');
            return redirect('messages/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $request->all();
        $input['user_id'] = $this->auth->id;

        if ( $post = Post::create($input) ) {
            \Session::set('success_message', 'New message added successfully.');
            return redirect()->route('message_show', $post->id);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'message_board');
    }

    public function update($Id, Request $request)
    {
        $input = $request->all();

        $post = Post::find($Id);
        $post['title'] = $input['title'];
        $post['content'] = $input['content'];
        $post['category_id'] = $input['category_id'];
        $post->save();

        if ( $post ) {
            \Session::set('success_message', 'Message updated successfully.');
            return redirect()->route('message_show', $Id);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'message_board');
    }

    public function show($Id) {
        // if(Gate::denies('edit_location')){
        //     return ['error'=>1, 'message'=>'Access Denied'];
        // }

        $post = Post::find($Id);
        
        return view('messages.show', compact('post'));
    }

    public function pin($Id) {

        $post = Post::find($Id);
        $post['pinned'] = 'yes';
        $post->save();
        
        if ($post->save())
            \Session::set('success_message', 'Message pinned successfully.');
        else 
            \Session::set('denied_message', 'Something went wrong in unpinning this message. Please contact system administrator.');

        return redirect()->route('message_show', $Id);
    }

    public function unpin($Id) {

        $post = Post::find($Id);
        $post['pinned'] = 'no';
        
        if ($post->save())
            \Session::set('success_message', 'Message unpinned successfully.');
        else 
            \Session::set('denied_message', 'Something went wrong in unpinning this message. Please contact system administrator.');

        return redirect()->route('message_show', $Id);
    }

    public function destroy($Id) {
        $post = Post::find($Id);

        if ($post->delete()) 
            \Session::set('success_message', 'Message deleted successfully.');
        else {
            \Session::flash('denied_message', 'Oops! Something went wrong in deleting this message. Please contact system administrator.');
            return redirect()->route('message_show', $Id);
        }

        return redirect()->route('message_board');
    }

}
