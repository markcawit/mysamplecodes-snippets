<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Cart;
use App\CartItem;
use App\Amenity;
use App\Membership;
use App\UserMembership;

use RoboPlanner\Helper\CartHelper;
use Auth;
use Gate;
use Carbon\Carbon;

use App\Events\NotifyMemberPayment;

use Paypal;
use Redirect;

class PayPalController extends Controller
{
    use CartHelper;

    private $_apiContext;
    private $currency;

    public function __construct()
    {
        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

        $this->currency = currency_code();
    }

    private function generateCartItems($cartId)
    {
        $total_snacks = $total_amenities = $total_addplans = $total_membership = 0;

        // get Member's Cart by CartID
        $cart = Cart::find($cartId); //$this->getCartByID($cartId);

        $invoiceNumber =  $cart->invoice_number;

        // Snack items
        $items = $cart->cartItems;
        foreach($items as $item){
            $total_snacks = ($total_snacks + $item->total);
        }

        // Get membership account
        $membership = UserMembership::where( 'user_id', $cart->user_id )->get();
        foreach ($membership as $plan) {
            $total_membership = ($total_membership + $plan->Membership->fee);
        }

        // Amenity items
        $amenities = $cart->cartItemsAmenities;
        foreach ($amenities as $amenity) {
            $total_amenities = ($total_amenities + $amenity->total);
        }

        // Additional Plan Charges
        $addplans = $cart->cartItemsAddPlans;
        foreach ($addplans as $plan) {
            $total_addplans = ($total_addplans + $plan->total);
        }

        return $this->GenerateItems($total_snacks, $total_membership, $total_amenities, $total_addplans, $invoiceNumber);
    }

    private function GenerateItems($snacks, $plan, $amenities, $additional, $invoiceNumber)
    {  
        $items = array();

        $item1 = PayPal::Item();
        $item1->setName('Snacks Consumed')
            ->setCurrency($this->currency)
            ->setQuantity(1)
            ->setPrice($snacks);
        if ($snacks) array_push($items, $item1);

        $item2 = PayPal::Item();
        $item2->setName('Membership Plan')
            ->setCurrency($this->currency)
            ->setQuantity(1)
            ->setPrice($plan);
        if ($plan) array_push($items, $item2);

        $item3 = PayPal::Item();
        $item3->setName('Amenities Charged')
            ->setCurrency($this->currency)
            ->setQuantity(1)
            ->setPrice($amenities);
        if ($amenities) array_push($items, $item3);

        $item4 = PayPal::Item();
        $item4->setName('Additional/Exrta Access')
            ->setCurrency($this->currency)
            ->setQuantity(1)
            ->setPrice($additional);
        if ($additional) array_push($items, $item4);

        return [
            'invoice_number' => $invoiceNumber,
            'cart_items' => $items,
            'total_amount_due' => ($snacks + $plan + $amenities + $additional)
            ];
    }

    public function getCheckout($cartId = null)
    {
        if( !$cartId ) {
            return view('roboplanner.errors.403');
        }

        $cart = $this->generateCartItems($cartId);
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');

        $itemList = PayPal::ItemList();
        $itemList->setItems($cart['cart_items']);

        $amount = PayPal::Amount();
        $amount->setCurrency($this->currency);
        $amount->setTotal($cart['total_amount_due']);
        // you can alternatively describe everything in the order separately;
        // Reference the PayPal PHP REST SDK for details.

        $invoiceNumber = $cart['invoice_number'];

        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($itemList);
        $transaction->setInvoiceNumber($invoiceNumber); // This can serve as CartID
        $transaction->setDescription("Membership Plan, Snacks, Amenities/Additional Charges");

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl(action('PayPalController@getDone', ['id' => $cartId]));
        $redirectUrls->setCancelUrl(action('PayPalController@getCancel'));

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        try {
            // The return object contains the status;
            $response = $payment->create($this->_apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {

            // Prints the detailed error message 
            $err_data = convert_to_array($ex->getData());
            $err_code = $ex->getCode();
            return view('errors.display', compact('err_data', 'err_code'));
        }
        // $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;

        return redirect( $redirectUrl );
    }

    public function getDone(Request $request, $cartId)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);

        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);
        try {
            // The return object contains the status;
            $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            // Prints the detailed error message 

            $err_data = convert_to_array($ex->getData());
            $err_code = $ex->getCode();
            return view('errors.display', compact('err_data', 'err_code'));
        }
        $txn         = json_decode($executePayment);
        $txnId       = $txn->transactions[0]->related_resources[0]->sale->id;
        $paymentDate = $txn->create_time;
        
        // $err_code = 'linti';
        // $err_data = $txn;
        // return view('errors.display', compact('err_data', 'err_code'));

        // Retrieve Cart being paid
        $cart = Cart::find($cartId);
        
        $userId = $cart->user_id; # capture the Cart userId
        
        // Flag Cart as paid
        $cart->payment_id = $id;
        $cart->token = $token;
        $cart->payer_id = $payer_id;
        $cart->txn_id = $txnId;
        $cart->status = 'paid';
        $cart->payment_date = Carbon::parse($paymentDate)->format('Y-m-d H:i:s');
        $cart->ref_payment = $executePayment;
        $cart->save();

        $newCart = $this->getCart($userId);

        // For the display of payment successful
        $txn            = json_decode($executePayment);
        $txn            = $txn->transactions[0];
        $itemList       = $txn->item_list;
        $sale           = $txn->related_resources[0]->sale;
        $paymentDate    = Carbon::parse($sale->create_time)->format('F j, Y h:i:sA');

        $notifyArray = ['cart' => $cart, 
                        'executePayment' => $executePayment, 
                        'txn' => $txn, 
                        'itemList' => $itemList, 
                        'sale' => $sale, 
                        'paymentDate' => $paymentDate,
                        'gateway' => 'paypal' ];
        // Email Member to notify of payments
        event( new NotifyMemberPayment($notifyArray) );
        return view('shop.cart.done', compact('cart', 'executePayment', 'txn', 'itemList', 'sale', 'paymentDate'));
    }

    public function getCancel()
    {
        return redirect()->route('cart-checkout');
    }
}
