<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RoboPlanner\Helper\ControllerHelper;
use RoboPlanner\Helper\RoomReservationHelper;

use App\Events\ApprovedReservation;
use App\Events\CancelledReservation;
use App\Events\RejectedReservation;

use App\Http\Requests;
use Carbon\Carbon;

use App\Location;
use App\RoomReservation;
use App\Room;

use Auth;
use Gate;

class RoomReservationController extends Controller
{
    use ControllerHelper;
    use RoomReservationHelper;

	protected $auth;
    
    public function __construct(){
		$this->middleware('auth');
		$this->auth = Auth::user();
	}
    
	public function index($locationId, $roomId= 0)
    {
        $locations = Location::all();
        $location = Location::find($locationId);

        $fetchBy = 'fetch_schedules'; // All Room Schedules

        $roomParams = array();
        $roomParams['location'] = $locationId;
        if ($roomId) $roomParams['room'] = $roomId;

		return view('room.index', compact('locations', 'locationId', 'roomId', 'fetchBy', 'location', 'roomParams'));
	}
    
    public function store(Request $request)
    {
        $input = $request->except('date','start_time','end_time');
        $input['user_id'] = $this->auth->id;

        $lapsedTime = $this->checkPastDate(Carbon::parse($input['start_date']), Carbon::now());
        if ($lapsedTime) {
            $a = $this->makeResponse(1, 'Reservation Date & Time should be backward.', 'add', 'room_reservations');
            return array_merge($a, $this->notifyResponse('TIME ALREADY PAST', 'Reservation date & time must be in a future.'));
        }

        // Check conflicted reservation schedule
        $conflict = RoomReservation::conflictSchedule($input);
        if ($conflict->count()) {
            $a = $this->makeResponse(1, 'Conflict time and date for the selected room.', 'add', 'room_reservations');
            return array_merge($a, $this->conflictScheduleResponse($input), ['conflict'=>$conflict->count()]);
        }

        // Check reservation limit of 3 days per week
        $weekLimit = RoomReservation::threeDaysPerWeekOnly($input);
        if (count($weekLimit) >= 3) {
            $a = $this->makeResponse(1, 'Weekly reservation limit reached', 'add', 'room_reservations');
            return array_merge($a, $this->weeklyLimitResponse($input['start_date']), ['week_limit'=>count($weekLimit)]);
        }

        // Check Daily Limit of 3 hours reservation
        $dailyLimit = $this->checkTimeCount( RoomReservation::threeHoursPerDay($input), $input );
        $limit = ( ceil($dailyLimit) > 3 ) ? true : false;
        if ($limit) {
            $a = $this->makeResponse(1, 'Daily limit reached.', 'add', 'room_reservations');
            return array_merge($a, $this->dailyLimitResponse(), ['daily_limit'=>$dailyLimit]);
        }

        // Check every other day
        $otherDay = RoomReservation::everyOtherDay($input);
        if ($otherDay->count()) {
            $a = $this->makeResponse(1, 'Reservation should be every other day.', 'add', 'room_reservations');
            return array_merge($a, $this->everyOtherDayResponse(), ['otherDay'=>$otherDay]);
        }

        // Store New Reservation
        if ($reservation = RoomReservation::create($input)) {
            $a = $this->makeResponse(0, 'Reservation added.', 'add', 'room_reservations', 1);
            return array_merge($reservation->toArray(), $a, ['weekLimit'=>count($weekLimit), 'dailyLimit'=>$dailyLimit, 'conflict'=>$conflict->count(), 'otherDay'=>$otherDay]);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'add', 'room_reservations');
    }

    public function edit($Id)
    {
        $reservation = RoomReservation::find($Id);
        return $reservation;
    }

    public function update(Request $request, $Id)
    {
        $input = $request->except('date','start_time','end_time');

        $reservation = RoomReservation::find($Id);
        $input['user_id'] = $reservation->user_id;

        // Check conflicted reservation schedule
        $conflict = RoomReservation::conflictSchedule($input);
        if ($conflict->count()) {
            $a = $this->makeResponse(1, 'Conflict time and date for the selected room.', 'add', 'room_reservations');
            return array_merge($a, $this->conflictScheduleResponse($input), ['conflict'=>$conflict->count()]);
        }

        // Check every other day
        $otherDay = RoomReservation::everyOtherDay($input);
        if ($otherDay->count()) {
            $a = $this->makeResponse(1, 'Reservation should be every other day.', 'add', 'room_reservations');
            return array_merge($a, $this->everyOtherDayResponse(), ['otherDay'=>$otherDay]);
        }

        $request = $reservation;
        if ($request->approved) {
            // Set as modified
            $request->status = 'modified';
            $request->approved = false;
        }

        $request->note = $input['note'];
        $request->room_id = $input['room_id'];
        $request->location_id = $input['location_id'];
        $request->start_date = $input['start_date'];
        $request->end_date = $input['end_date'];

        if ($request->save()) {
            $a = $this->makeResponse(0, 'Reservation updated.', 'update', 'room_reservations', true);
            return array_merge($reservation->toArray(), $a, ['conflict'=>$conflict->count(), 'otherDay'=>$otherDay]);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'update', 'room_reservations');

    }

    public function all(Request $request)
    {
        // $Id = (!$Id) ? $this->auth->LocationUser->location_id : $Id;
        $past = ($request->input('past')) ? 'past=1&' : '';

        $orderBy   = ($request->input('order_by')) ? $request->input('order_by') : 'start_date';
        $sort      = ($request->input('sort'))? $request->input('sort') : 'asc';

        $today = Carbon::now();
        $related = ['location', 'room', 'user'];
        if ( in_array($orderBy, $related) ) {
            $reservations = RoomReservation::where('start_date', '>=', $today)->get();
            
            $SORTING = ($sort == 'desc') ? 'sortByDesc' : 'sortBy';
            $FIELD = ($orderBy == 'user') ? 'username' : 'name';
            $reservations = $reservations->$SORTING(function($reservations) use ($orderBy, $FIELD) {
                return $reservations->$orderBy->$FIELD;
            });
        }
        else {
            $reservations = RoomReservation::where('start_date', '>=', $today)
                                        ->orderBy($orderBy, $sort)->get();
        }

        // Reverse sorting
        $sort = ($sort == 'asc')? 'desc' : 'asc';
        $roomParams = [
                'location' => $this->auth->LocationUser->location_id
            ];
        return view('room.reservations.index', compact('reservations', 'roomParams', 'sort', 'past'));
    }

	public function user(Request $request, $Id = 0)
	{
        $Id = (!$Id) ? $this->auth->id : $Id;
        $past = ($request->input('past')) ? 'past=1&' : '';

        $orderBy   = ($request->input('order_by')) ? $request->input('order_by') : 'start_date';
        $sort      = ($request->input('sort'))? $request->input('sort') : 'asc';

        $today = Carbon::now();
        $related = ['location', 'room'];
        if ( in_array($orderBy, $related) ) {
            $reservations = RoomReservation::where('user_id',$Id)
                                        ->where('start_date', '>=', $today)->get();
            
            $SORTING = ($sort == 'desc') ? 'sortByDesc' : 'sortBy';
            $reservations = $reservations->$SORTING(function($reservations) use ($orderBy) {
                return $reservations->$orderBy->name;
            });
        }
        else {
            $reservations = RoomReservation::where('user_id',$Id)
                                        ->where('start_date', '>=', $today)
                                        ->orderBy($orderBy, $sort)->get();
        }

        // Reverse sorting
        $sort = ($sort == 'asc')? 'desc' : 'asc';
        $roomParams = [
                'location' => $this->auth->LocationUser->location_id
            ];

        return view('room.reservations.index', compact('reservations', 'roomParams', 'sort', 'past'));
	}

	public function getScheduleByRoom($locationId, $roomId = 0)
	{
        $where = [];
        $where['location_id'] = $locationId;
        if ($roomId) 
            $where['room_id'] = $roomId;

        $reservations = RoomReservation::where($where)
                                    ->where( function($q) {
                                            $q->where('status', 'active')
                                              ->orWhere('status', 'modified');
                                        })
                                    ->get();

        return $reservations;
	}

    public function requests($locationId = 0, $roomId = 0)
    {
        $where = array();
        $where['location_id'] = ($locationId) ? $locationId : $this->auth->LocationUser->location_id;
        if ($roomId) $where['room_id'] = $roomId;

        $locations = Location::all();
        $location = Location::find($where['location_id']);

        $pendings = RoomReservation::where($where) 
                                    ->where('approved', 0)
                                    ->where(function($q) {
                                            $q->where('status', 'modified')
                                              ->orWhere('status', 'active');
                                        })->get();

        $roomParams = array();
        $roomParams['location'] = $this->auth->LocationUser->location_id;
        if ($roomId) $roomParams['room'] = $roomId;

        return view('room.reservations.pending', compact('pendings', 'locations', 'location', 'locationId', 'roomId', 'roomParams'));
    }

    public function confirm($Id)
    {
        if(Gate::denies('confirm_reservation')){
            // Message Alert
            \Session::flash('denied_message', 'Ooop! You don\'t have the permission to confirm a reservation.');
            return redirect()->route('reservation_item', $Id);
        }

        $reservation = RoomReservation::find($Id);
        $request = $reservation;
        $request->approved = 1;
        $request->status = 'active';
        $request->save();

        event( new ApprovedReservation($reservation) );

        return redirect()->route('reservation_item', $reservation->id);
    }

    public function cancel(Request $request, $Id)
    {
        $input = $request->all();

        $reservation = RoomReservation::find($Id);
        $requested = $reservation;

        $allowCancel = (!Gate::denies('cancel_reservation')) ? true : false;
        $allowCancel = ($reservation->user_id == $this->auth->id) ? true : $allowCancel;

        if ($allowCancel) {
            $user = ['id' => $this->auth->id, 'reason' => $input['reason'] ];

            $requested->approved = 0;
            $requested->status = 'cancelled';
            $requested->reason = $input['reason'];
            $requested->action_by = $this->auth->id;

            if ($requested->save()) {
                // Sending Email Event
                event( new CancelledReservation($reservation, $user) );

                $a = $this->makeResponse(0, 'Reservation cancelled.', 'cancel', 'room_reservations', true);
                return array_merge($requested->toArray(), $a);
            }
            else return $this->makeResponse(1, 'Something goes wrong.', 'cancel', 'room_reservations');
        } 
        else { 
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'cancel', 'room_reservations'),
                $this->notifyResponse('DENIED', 'You are not allowed to cancel other members\'s reservations.')
                );
        }
        // return redirect()->route('reservation_item', $reservation->id);
    }

    public function reject(Request $request, $Id)
    {
        if(Gate::denies('reject_reservation')){
            return array_merge(
                $this->makeResponse(1, 'Something goes wrong.', 'reject', 'room_reservations'),
                $this->notifyResponse('PERMISSION ERROR', 'Ooops! You don\'t have permission to reject this reservation.')
                );
        }

        $input = $request->all();

        $reservation = RoomReservation::find($Id);
        $requested = $reservation;
        
        $user = ['id' => $this->auth->id, 'reason' => $input['reason'] ];

        $requested->approved = 0;
        $requested->status = 'rejected';
        $requested->reason = $input['reason'];
        $requested->action_by = $this->auth->id;

        if ($requested->save()) {
            // Sending Email Event
            event( new RejectedReservation($reservation, $user) );

            $a = $this->makeResponse(0, 'Reservation rejected.', 'reject', 'room_reservations', true);
            return array_merge($requested->toArray(), $a);
        }
        else return $this->makeResponse(1, 'Something goes wrong.', 'reject', 'room_reservations');
    }

    public function show($Id)
    {
        $reservation = RoomReservation::find($Id);
        $roomParams = [
            'location' => $reservation->location_id, 
            'room' => $reservation->room_id
            ];
        return view('room.reservations.show', compact('reservation', 'roomParams'));
    }

}
