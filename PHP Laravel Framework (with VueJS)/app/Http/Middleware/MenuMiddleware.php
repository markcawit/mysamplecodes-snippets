<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Menu;
use Auth;
use Helpers;
use Session;
use RoboPlanner\Helper\CartHelper;

class MenuMiddleware
{
    use CartHelper;

	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	public function handle($request, Closure $next)
	{
		Menu::make('AdminMenu', function($menu) use ($request){
			if (Auth::check()) {

				$menu->add('Dashboard', ['route' => 'dashboard']);

				if (Auth::user()->can('view_clients')) {
					$menu->add('Clients', ['route' => 'clients']);
					if($request->segment(2) == 'clients'){
						$menu->clients->attr(['class' => 'active']);
					}
				}

				if (Auth::user()->can('view_users')) {
					$menu->add('Users', ['route' => 'users']);
					if($request->segment(2) == 'users'){
						$menu->users->attr(['class' => 'active']);
					}
				}

				if (Auth::user()->can('view_roles')) {
					$menu->add('Roles', ['route' => 'role_list']);
					if($request->segment(2) == 'roles'){
						$menu->roles->attr(['class' => 'active']);
					}
				}

				if (Auth::user()->can('view_permissions')) {
					$menu->add('Permissions', ['route' => 'permission_list']);
					if($request->segment(2) == 'permissions'){
						$menu->permissions->attr(['class' => 'active']);
					}
				}

			}
		});
		Menu::make('UserNav', function($menu) use ($request) {
			$toggle = ['data-toggle'=>'tooltip', 'data-placement'=>'bottom'];
			$segment1 = $request->segment(1);

			if (Auth::guest())
			{
				$menu->add('Login', [ 'url'=>url('login') ]);
				// $menu->add('Register', [ 'url'=>url('register') ]);
			}
			else {
                if ( Auth::user()->hasRole('client') )
                {
                    $menu->add('Drinkings™', [ 'route'=>'shop' ]);
				
					// Displaying Cart icon
					$itemCount = $this->cartItems();
					$loaded = ($itemCount) ? '-loaded' : '';
					$cartItems = ($itemCount) ? '<span id="cart-loaded" v-html="cartItemCount"></span>' : ''; // '.$itemCount.'
					$is_cart = ($segment1 == 'cart') ? 'active' : '';
					$props = array_merge([ 'class'=>'shop-cart '.$is_cart,
					'url'=>route('cart'),
					'title'=>'My Cart' ], $toggle);
					$menu->add('<i class="icon-basket'.$loaded.'" aria-hidden="true"></i>'.$cartItems, $props);
				}

				//User Account Settings
				$userName = Auth::user()->username;
				${$userName} = $menu->add( $userName, array('url' => '#', 'class' => 'dropdown') );
				$menu->group(['prefix' => 'settings'], function($a) use ($menu, $userName) {

					if ( Auth::user()->hasRole('administrator') ) { //Helpers::getUserRole() == 'administrator'
						$menu->{$userName}->add('<i class="icon-user"></i> Profile',
						array_merge(['route' => 'user_show', 'title'=>'My Profile']) );
						$menu->{$userName}->add('<i class="icon-people"></i> Users',
						array_merge(['route' => 'users', 'title'=>'Manage Users']) );
						$menu->{$userName}->add('<i class="icon-settings"></i> Roles',
						array_merge(['route' => 'role_list', 'title'=>'Manage Roles']) );
						$menu->{$userName}->add('<i class="icon-key"></i> Permissions',
						array_merge(['route' => 'permission_list', 'title'=>'Users Permissions']) )
						->divide( ['class' => 'divider', 'role' => 'presentation']);

						// Membership and Amenities Entry
						$menu->{$userName}->add('<i class="icon-bell"></i> Membership Plans',
						array_merge(['route' => 'membership', 'title'=>'Manage Membership Plans']) );
						$menu->{$userName}->add('<i class="icon-call-out"></i> Amenities',
						array_merge(['route' => 'amenities', 'title'=>'Manage Amenities']) );
						$menu->{$userName}->add('<i class="icon-home"></i> Rooms',
						array_merge(['route' => 'manage_room', 'title'=>'Manage Room']) )
						->divide( ['class' => 'divider', 'role' => 'presentation']);
					}

					$menu->{$userName}->add('<i class="icon-power"></i> Logout',
					array_merge(['route' => 'logout']) );
				});
			}
		});

		Menu::make('SideNav', function($menu) use ($request) {
			$toggle = ['data-toggle'=>'tooltip', 'data-placement'=>'right'];
			$segment1 = $request->segment(1);
			$segment2 = $request->segment(2);

			if (Auth::check())
			{
				// Add Dashboard Menu
				$is_dashboard = ($segment1 == 'dashboard') ? 'active' : '';
				$props = array_merge([ 'class'=>$is_dashboard,
				'url'=>route('dashboard'),
				'title'=>'Dashboard' ], $toggle);
				$menu->add('<i class="icon-speedometer"></i> <span class="sidenav-menu">Dashboard</span>',$props,
				[ 'class'=>$is_dashboard, 'url'=>route('dashboard') ]);

				if ( Auth::user()->hasRole('administrator') )
				{
                    if (Auth::user()->can('view_clients')) {
    					// Member Management Menu
    					$is_member_management = ( $segment1 == 'clients') ? 'active' : '';
    					$props = array_merge([ 'class'=>$is_member_management,
    					'url'=>route('clients'),
    					'title'=>'Member Management' ], $toggle);
    					$menu->add('<i class="icon-people"></i> <span class="sidenav-menu">Member Management</span>', $props);
                    }

					// Location Management Menu
					$is_location_management = ( $segment1 == 'location') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_location_management,
					'url'=>route('location'),
					'title'=>'Location Management' ], $toggle);
					$menu->add('<i class="icon-location-pin"></i> <span class="sidenav-menu">Location Management</span>', $props);

					// Events Menu
					$is_events = ( $segment1 == 'events') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_events,
					'url'=>route('events'),
					'title'=>'Events' ], $toggle);
					$menu->add('<i class="icon-calendar"></i> <span class="sidenav-menu">Events</span>', $props);

					// Meeting Room Schedule Menu
					$is_meeting_scheduler = ( $segment1 == 'meetings') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_meeting_scheduler,
					'url'=>route('rooms', ['location'=>Auth::user()->LocationUser->Location->id]),
					'title'=>'Meeting Room Schedule' ], $toggle);
					$menu->add('<i class="icon-bubbles"></i> <span class="sidenav-menu">Meeting Room</span>', $props);

					// Message Board Menu
					$is_message_board = ( $segment1 == 'messages') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_message_board,
					'url'=>route('message_board'),
					'title'=>'Message Board' ], $toggle);
					$menu->add('<i class="icon-speech"></i> <span class="sidenav-menu">Message Board</span>', $props);

					// Snack Shop Dashboard Menu
					$is_snack_shop = ( $segment1 == 'shop') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_snack_shop,
					'url'=>route('shop-dashboard'),
					'title'=>'Drinkings™ Dashboard' ], $toggle);
					$menu->add('<i class="icon-handbag" aria-hidden="true"></i> <span class="sidenav-menu">Snack Shop</span>', $props);
				}
				else {

                    if (Auth::user()->can('update_account')) {
    					// Account Management Menu
    					$is_account = ( $segment1 == 'account') ? 'active' : '';
    					$props = array_merge([ 'class'=>$is_account,
    					'url'=>route('user_show'),
    					'title'=>'My Account' ], $toggle);
    					$menu->add('<i class="icon-user"></i><span class="sidenav-menu">My Account</span>', $props);
                    }

					// Events Menu
					$is_events = ( $segment1 == 'events') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_events,
					'url'=>route('events'),
					'title'=>'Events' ], $toggle);
					$menu->add('<i class="icon-calendar"></i><span class="sidenav-menu">Events</span>', $props);

					// Meeting Room Schedule Menu
					$is_meeting_scheduler = ( $segment1 == 'meetings') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_meeting_scheduler,
					'url'=>route('rooms', ['location'=>Auth::user()->LocationUser->Location->id]),
					'title'=>'Meeting Room Schedule' ], $toggle);
					$menu->add('<i class="icon-list"></i><span class="sidenav-menu">Meeting Room Schedule</span>', $props);

					// Message Board Menu
					$is_message_board = ( $segment1 == 'messages') ? 'active' : '';
					$props = array_merge([ 'class'=>$is_message_board,
					'url'=>route('message_board'),
					'title'=>'Message Board' ], $toggle);
					$menu->add('<i class="icon-speech" aria-hidden="true"></i><span class="sidenav-menu">Message Board</span>', $props);
				}
			}
		});
		return $next($request);
	}
}
