<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

// Public Shop
Route::get('/store', 'Kiosk\ShopController@index');


Route::group(['prefix' => '/', 'middleware' => 'auth'], function() {
	Route::get('/', function () {
	    return redirect('dashboard');
	});

	Route::group(['prefix' => 'messages'], function() {
		Route::get('/', ['as' => 'message_board', 'uses' => 'MessageBoardController@index']);
		Route::get('create', ['as' => 'message_create', 'uses' => 'MessageBoardController@create']);
		Route::post('store', ['as' => 'message_store', 'uses' => 'MessageBoardController@store']);
		Route::get('edit/{id}', ['as' => 'message_edit', 'uses' => 'MessageBoardController@edit']);
		Route::get('show/{id}', ['as' => 'message_show', 'uses' => 'MessageBoardController@show']);
		Route::post('update/{id}', ['as' => 'message_update', 'uses' => 'MessageBoardController@update']);
		Route::get('pin/{id}', ['as' => 'message_pin', 'uses' => 'MessageBoardController@pin']);
		Route::get('unpin/{id}', ['as' => 'message_unpin', 'uses' => 'MessageBoardController@unpin']);
		Route::get('delete/{id}', ['as' => 'message_destroy', 'uses' => 'MessageBoardController@destroy']);
	});

	// Clients
	Route::group(['prefix' => 'clients', 'middleware' => 'web'],function(){
	    Route::get('/', ['as' => 'clients', 'uses' => 'ClientController@index']);
	    Route::get('/{id}', ['as'=> 'client_by_location', 'uses'=>'ClientController@index']);
	    Route::get('plan/{id}', ['as' => 'client_by_plan', 'uses'=>'ClientController@plan']);
	    Route::get('profile/{id}', ['as' => 'client_profile', 'uses' => 'ClientController@profile']);
	    Route::get('edit/{id}', ['as' => 'client_edit', 'uses' => 'ClientController@edit']);
	    Route::post('store',['as' => 'client_store', 'uses' => 'ClientController@store']);
	    Route::post('update/{id}',['as' => 'client_update', 'uses' => 'ClientController@update']);
	    Route::post('profile/{id}', ['as' => 'client_profile', 'uses' => 'ClientController@profile']);
	    Route::get('delete/{id}', ['as' => 'client_destroy', 'uses' => 'ClientController@destroy']);
	});

	//
	Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);
	Route::group(['prefix' => 'permissions', 'middleware' => 'web'],function(){
	    Route::get('/', ['as' =>'permission_list', 'uses' => 'PermissionController@index']);
	    Route::get('/add', ['as' =>'permission_add', 'uses' => 'PermissionController@create']);
	    Route::post('/store', ['as' =>'permission_store', 'uses' => 'PermissionController@store']);
	    Route::get('/edit/{id}', ['as' =>'permission_edit', 'uses' => 'PermissionController@edit']);
	    Route::post('/update/{id}', ['as' =>'permission_update', 'uses' => 'PermissionController@update']);
	    Route::get('/delete/{id}',	['as' =>'permission_delete',    'uses' => 'PermissionController@destroy']);
	});

	Route::group(['prefix' => 'roles', 'middleware' => 'web'],function(){
	    Route::get('/', ['as' =>'role_list', 'uses' => 'RoleController@index']);
	    Route::get('/add', ['as' =>'role_add', 'uses' => 'RoleController@create']);
	    Route::post('/store', ['as' =>'role_store', 'uses' => 'RoleController@store']);
	    Route::get('/edit/{id}', ['as' =>'role_edit', 'uses' => 'RoleController@edit']);
	    Route::post('/update/{id}', ['as' =>'role_update', 'uses' => 'RoleController@update']);
	    Route::get('/delete/{id}',	['as' =>'role_delete',    'uses' => 'RoleController@destroy']);
	});

	Route::group(['prefix' => 'users', 'middleware' => 'web'],function(){
	    Route::get('/',['as' => 'users', 'uses' => 'UserController@index']);
	    Route::get('/create',['as' => 'user_create', 'uses' => 'UserController@create']);
	    Route::get('/edit/{id}',['as' => 'user_edit', 'uses' => 'UserController@edit']);

	    Route::post('/store',['as' => 'user_store', 'uses' => 'UserController@save']);
	    Route::post('/update/{id}',['as' => 'user_update', 'uses' => 'UserController@update']);
	    Route::get('/delete/{id}',['as' => 'user_destroy', 'uses' => 'UserController@destroy']);

	    Route::get('/profile/{id?}',['as' => 'user_show', 'uses' => 'UserController@show']);

	    Route::post('/assign_role/{id}',['as' => 'user_assign_role', 'uses' => 'UserController@assign_role']);
	});

	Route::group(['prefix' => 'logs', 'middleware' => 'web'],function(){
	    Route::get('/',['as' => 'logs', 'uses' => 'LogController@index']);
	    Route::get('/user/{id}',['as' => 'user_logs', 'uses' => 'LogController@getUserLog']);
	});


	Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

	Route::get('fetch-locations', 'LocationController@fetchLocations');
	Route::group(['prefix' => 'location'], function() {
		Route::get('/', ['as' => 'location', 'uses' => 'LocationController@index']);
		Route::get('edit/{id}', ['as' => 'edit-location', 'uses' => 'LocationController@edit']);
		Route::post('store', ['as' => 'store-location', 'uses' => 'LocationController@store']);
		Route::post('update/{id}', ['as' => 'update-location', 'uses' => 'LocationController@update']);
		Route::get('trash/{id}', ['as' => 'location-trash', 'uses' => 'LocationController@trash']);
		Route::get('users/{id}', ['as' => 'location-users', 'uses' => 'LocationController@getUsers']);
	});

	Route::group(['prefix' => 'membership', 'middleware'=>'web'], function() {
		Route::get('/', ['as' => 'membership', 'uses' => 'MembershipController@index']);
		Route::get('edit/{id}', ['as' => 'membership-edit', 'uses' => 'MembershipController@edit']);
		Route::post('store', ['as'=>'membership-store', 'uses'=>'MembershipController@store']);
		Route::post('update/{id}', ['as'=>'membership-update', 'uses'=>'MembershipController@update']);
		Route::get('delete/{id}', ['as' => 'membership-destroy', 'uses' => 'MembershipController@destroy']);
	});

	Route::group(['prefix' => 'amenities', 'middleware' => 'web'], function() {
		Route::get('/', ['as' => 'amenities', 'uses' => 'AmenityController@index']);
		Route::get('edit/{id}', ['as' => 'amenities-edit', 'uses' => 'AmenityController@edit']);
		Route::post('store', ['as'=>'amenities-store', 'uses'=>'AmenityController@store']);
		Route::post('update/{id}', ['as'=>'amenities-update', 'uses'=>'AmenityController@update']);
		Route::get('delete/{id}', ['as' => 'amenities-destroy', 'uses' => 'AmenityController@destroy']);
	});

	Route::group(['prefix' => 'events'], function() {
		Route::get('/{id?}', ['as' => 'events', 'uses' => 'EventsController@index']);
		Route::get('/fetch/{id}', ['as' => 'fetch_events', 'uses' => 'EventsController@getEventsByLocation']);
		Route::get('/edit/{id}', 'EventsController@edit');
		Route::get('/show/{id}', ['as' => 'show_events', 'uses' => 'EventsController@show']);
		Route::post('/add', 'EventsController@store');
		Route::post('/update/{id}', 'EventsController@update');
		Route::get('/delete/{id}', 'EventsController@destroy');
	});

	Route::group(['prefix' => 'rooms'], function() {
		Route::group(['prefix' => 'management'], function() {
			Route::get('/{id?}', ['as' => 'manage_room',  'uses' => 'RoomManageController@index']);
			Route::post('/store', ['as' => 'add_room',  'uses' => 'RoomManageController@store']);
			Route::get('/edit/{id}', ['as' => 'edit_room',  'uses' => 'RoomManageController@edit']);
			Route::post('/update/{id}', ['as' => 'update_room',  'uses' => 'RoomManageController@update']);
			Route::get('/delete/{id}', ['as' => 'delete_room',  'uses' => 'RoomManageController@destroy']);
		});

		Route::group(['prefix' => 'reservation'], function() {
			Route::get('/me', ['as' => 'user_reservations', 'uses' => 'RoomReservationController@user']);
			Route::get('/user/{id}', ['as' => 'this_user_reservations', 'uses' => 'RoomReservationController@user']);
			Route::post('/store', ['as' => 'store_reservations', 'uses' => 'RoomReservationController@store']);
			Route::get('/edit/{id}', ['as' => 'user_reservations', 'uses' => 'RoomReservationController@edit']);
			Route::post('/update/{id}', ['as' => 'update_reservations', 'uses' => 'RoomReservationController@update']);

			Route::group(['prefix' => 'request'], function() {
				Route::get('/reserve/{id}', ['as' => 'reservation_item', 
											 'uses' => 'RoomReservationController@show']);
				Route::get('/confirm/{id}', ['as'=>'reservation_confirm', 
											 'uses'=>'RoomReservationController@confirm']);
				Route::post('/cancel/{id}', ['as'=>'reservation_cancel', 
											'uses'=>'RoomReservationController@cancel']);
				Route::post('/reject/{id}', ['as'=>'reservation_reject', 
											'uses'=>'RoomReservationController@reject']);

				Route::get('/{location?}/{room?}', ['as' => 'reservation_request', 
												   'uses' => 'RoomReservationController@requests']);

			});

			Route::get('/{id?}', ['as' => 'all_reservations', 'uses' => 'RoomReservationController@all']);
		});

		Route::get('/fetch/{location}/{room?}', [
							'as' => 'fetch_schedules', 
							'uses' => 'RoomReservationController@getScheduleByRoom']);
		Route::get('/fetch-room/{id}', [
							'as' => 'fetch_room_schedules', 
							'uses' => 'RoomReservationController@getScheduleByLocation']);

		Route::get('/{location}/{room?}', ['as' => 'rooms', 'uses' => 'RoomReservationController@index']);
	});

	Route::group(['prefix' => 'shop'], function() {
		Route::get('/', ['as'=>'shop', 'uses'=>'Kiosk\ShopController@index']);

		// Shop Controllers
	    Route::group(['prefix' => 'dashboard'], function() {
	    	Route::get('/', ['as'=>'shop-dashboard' , 'uses'=>'Kiosk\ShopController@dashboard']);
	    	Route::get('fetch-categories', 'Kiosk\ShopController@fetchCategories');
	    	Route::get('product/edit/{id}', ['as'=>'edit-product' , 'uses'=>'Kiosk\ShopController@edit']);
	    	Route::post('product/add', ['as'=>'add-product' , 'uses'=>'Kiosk\ShopController@store']);
	    	Route::post('product/update/{id}', ['as'=>'update-product' , 'uses'=>'Kiosk\ShopController@update']);
		    Route::get('product/delete/{id}', ['as'=>'delete-product' , 'uses'=>'Kiosk\ShopController@deleteProduct']);

		    Route::group(['prefix' => 'category'], function() {
		    	Route::get('/', ['as'=>'get-categories', 'uses'=>'Kiosk\ShopController@fetchCategories']);
		    	Route::get('/edit/{id}', ['as'=>'edit-categories', 'uses'=>'Kiosk\ShopController@editCategory']);
		    	Route::post('/add', ['as'=>'add-categories', 'uses'=>'Kiosk\ShopController@addCategory']);
		    	Route::post('/update/{id}', ['as'=>'update-categories', 'uses'=>'Kiosk\ShopController@updateCategory']);
		    	Route::get('/remove/{id}', ['as'=>'remove-categories', 'uses'=>'Kiosk\ShopController@removeCategory']);
		    });
	    });

	    // Cart Controllers
	    // Route::get('cart', 'Kiosk\CartController@index');
	});
	
	Route::get('fetch-charges', 'Kiosk\CartController@fetchCharges');
	Route::group(['prefix' => 'cart'], function() {
		Route::get('/', ['as'=>'cart', 'uses'=>'Kiosk\CartController@showCart']);
		Route::get('member/{id}', ['as'=>'cart-member', 'uses'=>'Kiosk\CartController@showMemberCart']);
		Route::get('count', ['as'=>'cart-count', 'uses'=>'Kiosk\CartController@getCartCount']);
		Route::post('add/{productId}', ['as'=>'cart-add', 'uses'=>'Kiosk\CartController@addItem']);
		Route::post('update', ['as'=>'cart-update', 'uses'=>'Kiosk\CartController@updateCart']);
		Route::post('additional', ['as'=>'cart-add-charges', 'uses'=>'Kiosk\CartController@additionalCharges']);
		Route::get('remove/{productId}', ['as'=>'cart-remove', 'uses'=>'Kiosk\CartController@removeItem']);
		Route::get('checkout/{cartId?}', ['as'=>'cart-checkout', 'uses'=>'Kiosk\CartController@checkout']);
		Route::post('payment/{id}', ['as'=>'cart-payment', 'uses'=>'Kiosk\CartController@payment']);
	});

	// Route::group(['prefix'=>'paypal'], function() {
	// 	Route::get('checkout', 'PayPalController@getCheckout');
	// 	Route::get('done', 'PayPalController@getDone');
	// 	Route::get('cancel', 'PayPalController@getCancel');
	// });
	Route::controller('paypal', 'PayPalController');

});