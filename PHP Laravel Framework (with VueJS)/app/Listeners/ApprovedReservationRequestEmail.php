<?php

namespace App\Listeners;

use App\Events\ApprovedReservation;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class ApprovedReservationRequestEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApprovedReservation  $event
     * @return void
     */
    public function handle(ApprovedReservation $event)
    {
        $data = $event->inputs;

        // Sending of email to welcome the user
        Mail::send('emails.reservations.approved', compact('data'), function ($m) use ($data) {
            $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                ->to($data->user->email, $data->user->full_name)
                ->subject('Approved Reservation Request');
        });/**/
    }
}
