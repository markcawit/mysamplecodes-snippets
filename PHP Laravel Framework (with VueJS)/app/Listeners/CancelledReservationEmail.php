<?php

namespace App\Listeners;

use App\Events\CancelledReservation;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class CancelledReservationEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CancelledReservation  $event
     * @return void
     */
    public function handle(CancelledReservation $event)
    {
        $data = $event->inputs;
        $user = $event->user;

        // Sending of email for cancelled reservation
        Mail::send('emails.reservations.cancelled', compact('data', 'user'), function ($m) use ($data) {
            $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                ->to($data->user->email, $data->user->full_name)
                ->subject('Cancelled Room Reservation');
        });/**/
    }
}
