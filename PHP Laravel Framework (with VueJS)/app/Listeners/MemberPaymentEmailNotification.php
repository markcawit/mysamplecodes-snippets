<?php

namespace App\Listeners;

use App\Events\NotifyMemberPayment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

use App\User;

class MemberPaymentEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotifyMemberPayment  $event
     * @return void
     */
    public function handle(NotifyMemberPayment $event)
    {
        $data = $event->inputs;
        $gateway = $data['gateway'];

        if ($gateway == 'otc') {
            $cart = $data['cart'];
            $txn = json_decode($cart->ref_payment);

            // Sending of email for PayPal Checkout
            Mail::send('emails.cart.checkout.manual', compact('cart', 'txn'), function ($m) use ($cart) {
                $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                    ->to($cart->user->email, $cart->user->full_name)
                    ->subject('Checkout Payment Notification (Transaction ID: '.$cart->txn_id.')');
            });
        }
        else {
            $cart = $data['cart'];
            $executePayment = $data['executePayment'];
            $txn = $data['txn'];
            $itemList = $data['itemList'];
            $sale = $data['sale'];
            $paymentDate = $data['paymentDate'];

            // Sending of email for PayPal Checkout
            Mail::send('emails.cart.checkout.paypal', compact('cart', 'executePayment', 'txn', 'itemList', 'sale', 
                                                                'paymentDate'), function ($m) use ($cart) {
                $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                    ->to($cart->user->email, $cart->user->full_name)
                    ->subject('Checkout Payment Notification (Transaction ID: '.$cart->tnx_id.')');
            });
        }
    }
}
