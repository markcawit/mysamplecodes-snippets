<?php

namespace App\Listeners;

use App\Events\RejectedReservation;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class RejectedReservationRequestEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RejectedReservation  $event
     * @return void
     */
    public function handle(RejectedReservation $event)
    {
        $data = $event->inputs;
        $user = $event->user;

        // Sending of email for rejected request
        Mail::send('emails.reservations.rejected', compact('data', 'user'), function ($m) use ($data) {
            $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                ->to($data->user->email, $data->user->full_name)
                ->subject('Rejected Reservation Request');
        });
    }
}
