<?php

namespace App\Listeners;

use App\Events\WelcomeUserEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class WelcomeUserEmailer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(WelcomeUserEvent $event)
    {
        $data = $event->inputs;

        // Sending of email to welcome the user
        Mail::send('emails.welcome', compact('data'), function ($m) use ($data) {
            $m->from(config('mail.noreply.address'), config('mail.noreply.name'))
                ->to($data['email'], $data['first_name'].' '.$data['last_name'])
                ->subject('Bienvenido a Workings!');
        });
    }
}
