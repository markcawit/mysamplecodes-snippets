<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Carbon\Carbon;
use Auth;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'slug', 'pinned', 'pinned_by', 'user_id', 'category_id'
    ];

    protected $append = ['excerpt', 'short_title'];

    public function User()
    {
    	return $this->belongsTo(User::class);
    }  

    public function PostCategory()
    {
        return $this->hasOne(PostCategory::class, 'id', 'category_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getExcerptAttribute($i = 4)
    {
        return Str::words($this->content, $i);
    }

    public function getShortTitleAttribute()
    {
        return Str::limit($this->title, 15);
    }

    public function formatDatePosted()
    {
        return Carbon::parse($this->created_at)->format('F j, Y');
    }

    public function formatLastUpdate()
    {
        return Carbon::parse($this->updated_at)->format('F j, Y');
    }

    public function isOwned()
    {
        if ($this->user_id == Auth::user()->id) {
            return '<a href="'. route('message_edit', $this->id) .'" data-toggle="tooltip" data-placement="top" title="Edit" class="icon-note small-icon"></a>';
        }
    }

    public function canPin()
    {
        if ( Auth::user()->hasRole('administrator') ) {
            if ( $this->pinned == 'no') {
                    return '<a href="' . route('message_pin', $this->id) . '" data-toggle="tooltip" data-placement="top" title="Pin Post" class="icon-pin small-icon"></a>';
            }
            else {
                    return '<a href="' . route('message_unpin', $this->id) . '" data-toggle="tooltip" data-placement="top" title="Unpin Post" class="icon-pin text-red small-icon"></a>';
            }
        }
    }
}
