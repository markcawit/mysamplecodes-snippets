<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\WelcomeUserEvent' => [
            'App\Listeners\WelcomeUserEmailer',
        ],
        'App\Events\ApprovedReservation' => [
            'App\Listeners\ApprovedReservationRequestEmail',
        ],
        'App\Events\CancelledReservation' => [
            'App\Listeners\CancelledReservationEmail',
        ],
        'App\Events\RejectedReservation' => [
            'App\Listeners\RejectedReservationRequestEmail',
        ],
        'App\Events\NotifyMemberPayment' => [
            'App\Listeners\MemberPaymentEmailNotification',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
