<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Carbon\Carbon;
use Auth;
use App\User;

class RoomReservation extends Model
{
     protected $fillable = [
        'note', 'start_date', 'end_date', 'room_id', 'location_id', 'user_id', 'status', 'approved'
    ];

    protected $appends = ['title', 'request_title', 'date', 'start', 'end', 'date_time', 'date_time_a', 'start_time', 'end_time', 'color', 'stat', 'time_ago', 'status_approved', 'created_date'];

    public function getTitleAttribute()
    {
    	return ($this->approved) 
                    ? $this->room->name . ' at ' . $this->location->name 
                    : $this->room->name . ' - Pending for approval';
    }

    public function getRequestTitleAttribute()
    {
        return $this->user->first_name . ' requested to reserve ' . $this->room->name . 
                ' on ' . Carbon::parse($this->start_date)->format('l \a\t h:iA'); //$this->room->name . ' at ' . $this->location->name;
    }

    public function getDateAttribute()
    {
    	return Carbon::parse($this->start_date)->format('Y-m-d');
    }

    public function getStartAttribute()
    {
        return Carbon::parse($this->start_date)->format('Y-m-d H:i:s');
    }

    public function getEndAttribute()
    {
        return Carbon::parse($this->end_date)->format('Y-m-d H:i:s');
    }

    public function getStartTimeAttribute()
    {
        return Carbon::parse($this->start_date)->format('H:i:s');
    }

    public function getEndTimeAttribute()
    {
        return Carbon::parse($this->end_date)->format('H:i:s');
    }

    public function getDateTimeAttribute()
    {
        return Carbon::parse($this->start_date)->format('D F j, Y \a\t h:i A') .
                Carbon::parse($this->end_date)->format(' - h:i A');
    }

    public function getDateTimeAAttribute()
    {
        return Carbon::parse($this->start_date)->format('m/d/y \a\t h:iA') .
                Carbon::parse($this->end_date)->format(' - h:iA');
    }

    public function getTimeAgoAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    }

    public function getStatAttribute()
    {
        return (!$this->approved) ? ' <i class="text-sm text-red">Unconfirmed</i>' : '';
    }

    public function getStatusApprovedAttribute()
    {
        if ($this->approved) 
            return 'Approved';
        else {
            return ($this->status == 'active') ? 'Not Approved' : ucwords($this->status) ;
        }
    }

    public function getColorAttribute()
    {
        $color = ($this->approved) ? 'green' : 'orange';

        if ( !$this->approved && ($this->user_id == Auth::user()->id) )
            $color = 'red';

        return $color;
    }

    public function getCreatedDateAttribute()
    {       
        return ($this->status !== 'modified') 
                    ? Carbon::parse($this->created_at)->format('F j, Y h:iA') 
                    : Carbon::parse($this->updated_at)->format('F j, Y h:iA');
    }

    public function Room()
    {
    	return $this->belongsTo(Room::class);
    }
    
    public function Location()
    {
    	return $this->belongsTo(Location::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function getActionByAttribute($value)
    {
        if ($value) 
            return User::find($value)->username;
        else 
            return $value;
    }

    public function scopeConflictSchedule($query, $input)
    {
        return $query->where('room_id', $input['room_id'])
                    ->where('status', 'active')
                    ->where(function($q) use ($input) {
                            $q->where( function($s) use ($input) {
                                $s->where('start_date', '>=', $input['start_date'])
                                  ->where('start_date', '<=', $input['end_date']);
                            })->orWhere( function($e) use ($input) {
                                $e->where('end_date', '>', $input['start_date'])
                                  ->where('end_date', '<', $input['end_date']);
                            });
                        })->get();
    }

    public function scopeThreeDaysPerWeekOnly($query, $input)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $sunday = Carbon::parse($input['start_date'])->startOfWeek();
        $saturday = Carbon::parse($input['start_date'])->endOfWeek();

        $reservations = $query->where('user_id', $input['user_id'])
                    ->where('status', 'active')
                    ->where( function($q) use ($sunday, $saturday) {
                        $q->where('start_date', '>=', $sunday)
                          ->where('start_date', '<=', $saturday)->get();
                    })->get();

        $reserved = array();
        foreach ($reservations as $res) {
            $date = Carbon::parse($res->start_date)->format('Y-m-d');
            if (!in_array($date, $reserved))
                $reserved[$date] = $res;
        }

        return $reserved;
    }

    public function scopeThreeHoursPerDay($query, $input)
    {
        $bot = Carbon::parse($input['start_date'])->format('Y-m-d 00:00:00');

        Carbon::setTestNow(Carbon::parse($input['start_date']));
        $eot = new Carbon('tomorrow');
        Carbon::setTestNow();

        $reserves = $query->where('user_id', $input['user_id'])
                    ->where('status', 'active')
                    ->where( function($q) use ($bot, $eot) {
                        $q->where('start_date', '>=', $bot)
                          ->where('start_date', '<=', $eot)->get();
                    })->get();
        
        return $reserves;
    }

    public function scopeEveryOtherDay($query, $input)
    {
        // Get Today's Date
        $today = Carbon::parse($input['start_date'])->format('Y-m-d 00:00:00');

        // Get Tomorrow's Date
        Carbon::setTestNow(Carbon::parse($input['start_date']));
        $tom = new Carbon('tomorrow');
        Carbon::setTestNow();
        $tomorrow = Carbon::parse($tom)->format('Y-m-d 23:59:59');

        // Get Yesterday's Date
        Carbon::setTestNow(Carbon::parse($input['start_date']));
        $yesterday = new Carbon('yesterday');
        Carbon::setTestNow();

        $reserves = $query->where('user_id', $input['user_id'])
                    ->where('status', 'active')
                    ->where( function($q) use ($today, $yesterday) {
                        $q->where('start_date', '>=', $yesterday)
                          ->where('start_date', '<', $today)->get();
                    })
                    ->orWhere( function($q) use ($today, $tomorrow) {
                        $today = Carbon::parse($today)->format('Y-m-d 23:59:59');
                        $q->where('start_date', '>', $today)
                          ->where('start_date', '<=', $tomorrow)->get();
                    })->get();
        
        return $reserves;
    }

}
