<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

use App\Role;

class User extends Authenticatable
{
    use HasRoles;
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'nickname', 'username', 'email', 'password', 'parent', 'status', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        if (Auth::user()) {
            static::updating(function($user) {
                $user->logs()->attach(Auth::user()->id, ['module_name' => 'User', 'log' => Auth::user()->first_name . ' ' . Auth::user()->last_name . ' is updating ' . $user->first_name . ' ' . $user->last_name ]);
            });

            static::creating(function($user){
                $user->logs()->attach(Auth::user()->id, ['module_name' => 'User', 'log' => Auth::user()->first_name . ' ' . Auth::user()->last_name . ' is creating ' . $user->first_name . ' ' . $user->last_name ]);
            });

            static::deleting(function($user){
                $user->logs()->attach(Auth::user()->id, ['module_name' => 'User', 'log' => Auth::user()->first_name . ' ' . Auth::user()->last_name . ' is deleting ' . $user->first_name . ' ' . $user->last_name ]);
            });
        }
    }

    public function Role(){
        return $this->belongsToMany(Role::class);
    }

    public function assignedRole($id)
    {
        return $this->roles()->save(
            Role::where('id',$id)->firstOrFail()
        );
    }

    public function usermeta(){
        return $this->hasMany(UserMeta::class);
    }

    public function logs(){
        return $this->belongsToMany(User::class, 'logs', 'module_id')
            ->withTimestamps()
            ->latest('pivot_updated_at');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function LocationUser()
    {
        return $this->hasOne(LocationUser::class);
    }
    
    public function PersonalInfo()
    {
        // $this->hasOne('App\PersonalInfo','user_id','id');
        return $this->hasOne(PersonalInfo::class);
    }

    public function UserMembership()
    {
        return $this->hasOne(UserMembership::class);
    }

    public function RoomReservation()
    {
        return $this->hasMany(RoomReservation::class);
    }

}
