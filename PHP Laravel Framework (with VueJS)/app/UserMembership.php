<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserMembership extends Eloquent
{
    protected $table = 'user_membership';
    
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'membership_id'
    ];

    public function Membership()
    {
        return $this->belongsTo(Membership::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}