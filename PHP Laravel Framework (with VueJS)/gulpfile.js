var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 | 
 | 'vendors/jquery.min.js',
 */

elixir(function(mix) {
    mix.sass('app.scss')
    	.scripts([
    		'vendors/bootstrap.min.js'
    		], 'public/js/vendors.js')
      .styles([
        'calendar/fullcalendar.min.css'
        ], 'public/css/calendar.styles.css')
      .scripts([
        'components/*.js',
        'app.js',
        'calendar.js'
      ],'public/js/app.js')
      .scripts([
        'vendors-calendar/jquery-v2-1-3.min.js',
        'vendors-calendar/moment-v2-9-0.min.js',
        'vendors-calendar/fullcalendar-v2-9-1.min.js'
      ],'public/js/vendors-calendar.js')
      .copy('resources/assets/fonts', 'public/fonts');
      elixir.Task.find('sass').watch('resources/assets/sass/components/*.scss');
    mix.browserify('main.js');
});
