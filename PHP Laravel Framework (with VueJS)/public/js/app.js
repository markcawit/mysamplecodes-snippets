$(function() {
	var $panelClose = $(".panel-close");
	var $panel = $(".panel-toggle");
	var isVisible = true;

	$panelClose.on("click", function(e) {
		e.preventDefault();
		togglePanel($(this));
	});

	function showPanel($closeButton) {
		$closeButton.parents(".panel-toggle").removeClass("toggled");
		isVisible = true;
	}
	function hidePanel($closeButton) {
		$closeButton.parents(".panel-toggle").addClass("toggled");
		isVisible = false;
	}
	function togglePanel($closeButton) {
		if(isVisible) {
			hidePanel($closeButton);
		} else {
			showPanel($closeButton);
		}
	}
});

$(function() {
	$closeButton = $(".close-button");
	// $mask = $(".mask");

	$closeButton.on("click", function() {
		if (!$(this).hasClass('no-close')) {
			$mask = $(this).closest(".mask");
			$mask.hide();
		}
	});
});

$(function() {
	var sideNavWidth = 100;
	var sideNavWidthMob = 180;
	var $sideNavContainer = $(".side-nav-container");
	var $contentContainer = $(".content-container");
	var $navBar = $(".navbar-with-sidebar");

	var $sidebarToggleButton = $("#sidebar-toggle-button");
	var $w = $(window);
	var isVisible = true;

	//user interaction
	$sidebarToggleButton.on("click", function() {
		toggleSidebar();
	});

	//functions
	checkWidth();
	$w.resize(function() {
		checkWidth();
	});

	function showSidebar() {
		isVisible = true;
		$sidebarToggleButton.find("i")
		.removeClass("icon-menu")
		.addClass("icon-arrow-left");

		$sideNavContainer.css("margin-left", 0);
		$navBar.css("margin-left", sideNavWidth+"px");
		if($w.width() > 768) {
			$contentContainer.css("padding-left", sideNavWidth+"px");
$sideNavContainer.css("width", sideNavWidth+"px")
		}
		if($w.width() < 768) {
			$navBar.css("margin-left", sideNavWidthMob+"px");
			$sideNavContainer.css("width", sideNavWidthMob+"px")
		}
	}

	function hideSidebar() {
		isVisible = false;
		$sidebarToggleButton.find("i")
		.removeClass("icon-arrow-left")
		.addClass("icon-menu");

		$sideNavContainer.css("margin-left", -sideNavWidth+"px");
		$navBar.css("margin-left", 0);
		$contentContainer.css("padding-left", 0);
		if($w.width() > 768) {
			$sideNavContainer.css("margin-left", -sideNavWidth+"px");
		}
		if($w.width() < 768) {
			$sideNavContainer.css("margin-left", -sideNavWidthMob+"px");
		}
	}

	function toggleSidebar() {
		if(isVisible) {
			hideSidebar();
		} else {
			showSidebar();
		}
	}

	function checkWidth() {
		if($w.width() < 768) {
			hideSidebar();
		} else {
			showSidebar();
		}
	}
})

$(function() {
	// App
	$('[data-toggle="tooltip"]').tooltip();

	// dashboard anchor items
	$('[data-anchor]').on('click', function() {
		var link = $(this).find('[data-internal]').attr('href');
		window.open(link);
	});
	$('[data-internal]').on('click', function(e) {
		e.preventDefault();
	});


	// change Calendar UI Icons (ui-icon-circle-triangle-e)
	$('.ui-icon').each(function(index, el) {
		if (el.hasClass('ui-icon-circle-triangle-e')) {
			el.removeClass('ui-icon-circle-triangle-e')
				.addClass('icon-arrow-right');
		}
		if (el.hasClass('ui-icon-circle-triangle-w')) {
			el.removeClass('ui-icon-circle-triangle-w')
				.addClass('icon-arrow-left');
		}
	});

	$('#upload-photo').on('click', function() {
		$('#imageInput').click();
	});
});
/*
 *	All Javascript/jQuery about the Calendar should go here.
 */
function instanceCalendar(calendar, module, eventsURL, locationID, roomID) {
    var leftButtons = 'prev,today,next';
    if (module == 'rooms') {

        if (window.userAccess == 'admin')
            leftButtons += ' allReservationButton';
        else
            leftButtons += ' myCustomButton';
    }

    $(calendar).fullCalendar({
        customButtons: {
            myCustomButton: {
                text: 'My Reservations',
                click: function() {
                    window.location.href = '/rooms/reservation/me';
                }
            },
            allReservationButton: {
                text: 'List of Reservations',
                click: function() {
                    window.location.href = '/rooms/reservation'
                }
            }
        },
        theme: true,
        header: {
            left: leftButtons,
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: eventsURL, //Get List of Events from this URL
        defaultView: 'month',
        dayClick: function(date) {
            var view = $(calendar).fullCalendar('getView');
            var start = date.format('HH:mm:ss');
            var end = date.clone().add(120, 'minute').format('HH:mm:ss');
            thisDate = date.format('YYYY-MM-DD');

            // check if clicked on the hours on Day View
            if (view.title == date.format('MMMM D, YYYY')) {
                if (window.canAddEvent) {
                    console.log('Date: '+thisDate+' (Start: '+start+ ' - End: '+end+')');
                    if (module == 'events')
                        window.location.href = '#!/eventCal/add/'+locationID+'/'+thisDate+'/'+start+'/'+end;
                }

                if (window.canReserveRoom) {
                    if (module == 'rooms')
                        window.location.href = '#!/reservation/request/'+locationID+'/'+roomID+'/'+thisDate+'/'+start+'/'+end;
                }
            } else {
                $(calendar).fullCalendar('gotoDate', date);
                $(calendar).fullCalendar('changeView', 'agendaDay');
            }
        },
        eventClick: function (event, jsEvent, view) {
            console.log(event);
            console.log(jsEvent);
            console.log(view);

            if (module == 'events') {
                if (window.canAddEvent)
                    window.location.href = '#!/eventCal/get-action/'+event.id;
                else
                    window.location.href = '/events/show/'+event.id;
            }
            else if (module == 'rooms') {
                if (event.user_id == window.userId)
                    window.location.href = '#!/reservation/option/room_reservations/'+event.id+'/view_reservation/change_reservation';
                else 
                    window.location.href = '#!/reservation/view/'+event.id;
            }
        },
    });
    $(calendar).fullCalendar('refetchEvents');
    $(calendar).fullCalendar('refresh');
    $(calendar).fullCalendar('render');


    // $('#prev-button').click(function() {
    //     $(calendar).fullCalendar('prev');
    // });

    // $('#next-button').click(function() {
    //     $(calendar).fullCalendar('next');
    // });
}

//# sourceMappingURL=app.js.map
