$(function() {
	// App
	$('[data-toggle="tooltip"]').tooltip();

	// dashboard anchor items
	$('[data-anchor]').on('click', function() {
		var link = $(this).find('[data-internal]').attr('href');
		window.open(link);
	});
	$('[data-internal]').on('click', function(e) {
		e.preventDefault();
	});


	// change Calendar UI Icons (ui-icon-circle-triangle-e)
	$('.ui-icon').each(function(index, el) {
		if (el.hasClass('ui-icon-circle-triangle-e')) {
			el.removeClass('ui-icon-circle-triangle-e')
				.addClass('icon-arrow-right');
		}
		if (el.hasClass('ui-icon-circle-triangle-w')) {
			el.removeClass('ui-icon-circle-triangle-w')
				.addClass('icon-arrow-left');
		}
	});

	$('#upload-photo').on('click', function() {
		$('#imageInput').click();
	});
});