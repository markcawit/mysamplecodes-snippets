/*
 *	All Javascript/jQuery about the Calendar should go here.
 */
function instanceCalendar(calendar, module, eventsURL, locationID, roomID) {
    var leftButtons = 'prev,today,next';
    if (module == 'rooms') {

        if (window.userAccess == 'admin')
            leftButtons += ' allReservationButton';
        else
            leftButtons += ' myCustomButton';
    }

    $(calendar).fullCalendar({
        customButtons: {
            myCustomButton: {
                text: 'My Reservations',
                click: function() {
                    window.location.href = '/rooms/reservation/me';
                }
            },
            allReservationButton: {
                text: 'List of Reservations',
                click: function() {
                    window.location.href = '/rooms/reservation'
                }
            }
        },
        theme: true,
        header: {
            left: leftButtons,
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: eventsURL, //Get List of Events from this URL
        defaultView: 'month',
        dayClick: function(date) {
            var view = $(calendar).fullCalendar('getView');
            var start = date.format('HH:mm:ss');
            var end = date.clone().add(120, 'minute').format('HH:mm:ss');
            thisDate = date.format('YYYY-MM-DD');

            // check if clicked on the hours on Day View
            if (view.title == date.format('MMMM D, YYYY')) {
                if (window.canAddEvent) {
                    console.log('Date: '+thisDate+' (Start: '+start+ ' - End: '+end+')');
                    if (module == 'events')
                        window.location.href = '#!/eventCal/add/'+locationID+'/'+thisDate+'/'+start+'/'+end;
                }

                if (window.canReserveRoom) {
                    if (module == 'rooms')
                        window.location.href = '#!/reservation/request/'+locationID+'/'+roomID+'/'+thisDate+'/'+start+'/'+end;
                }
            } else {
                $(calendar).fullCalendar('gotoDate', date);
                $(calendar).fullCalendar('changeView', 'agendaDay');
            }
        },
        eventClick: function (event, jsEvent, view) {
            console.log(event);
            console.log(jsEvent);
            console.log(view);

            if (module == 'events') {
                if (window.canAddEvent)
                    window.location.href = '#!/eventCal/get-action/'+event.id;
                else
                    window.location.href = '/events/show/'+event.id;
            }
            else if (module == 'rooms') {
                if (event.user_id == window.userId)
                    window.location.href = '#!/reservation/option/room_reservations/'+event.id+'/view_reservation/change_reservation';
                else 
                    window.location.href = '#!/reservation/view/'+event.id;
            }
        },
    });
    $(calendar).fullCalendar('refetchEvents');
    $(calendar).fullCalendar('refresh');
    $(calendar).fullCalendar('render');


    // $('#prev-button').click(function() {
    //     $(calendar).fullCalendar('prev');
    // });

    // $('#next-button').click(function() {
    //     $(calendar).fullCalendar('next');
    // });
}
