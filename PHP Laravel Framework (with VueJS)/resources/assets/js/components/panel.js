$(function() {
	var $panelClose = $(".panel-close");
	var $panel = $(".panel-toggle");
	var isVisible = true;

	$panelClose.on("click", function(e) {
		e.preventDefault();
		togglePanel($(this));
	});

	function showPanel($closeButton) {
		$closeButton.parents(".panel-toggle").removeClass("toggled");
		isVisible = true;
	}
	function hidePanel($closeButton) {
		$closeButton.parents(".panel-toggle").addClass("toggled");
		isVisible = false;
	}
	function togglePanel($closeButton) {
		if(isVisible) {
			hidePanel($closeButton);
		} else {
			showPanel($closeButton);
		}
	}
});
