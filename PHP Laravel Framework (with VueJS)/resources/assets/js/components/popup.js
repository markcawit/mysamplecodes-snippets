$(function() {
	$closeButton = $(".close-button");
	// $mask = $(".mask");

	$closeButton.on("click", function() {
		if (!$(this).hasClass('no-close')) {
			$mask = $(this).closest(".mask");
			$mask.hide();
		}
	});
});
