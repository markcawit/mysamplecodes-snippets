$(function() {
	var sideNavWidth = 100;
	var sideNavWidthMob = 180;
	var $sideNavContainer = $(".side-nav-container");
	var $contentContainer = $(".content-container");
	var $navBar = $(".navbar-with-sidebar");

	var $sidebarToggleButton = $("#sidebar-toggle-button");
	var $w = $(window);
	var isVisible = true;

	//user interaction
	$sidebarToggleButton.on("click", function() {
		toggleSidebar();
	});

	//functions
	checkWidth();
	$w.resize(function() {
		checkWidth();
	});

	function showSidebar() {
		isVisible = true;
		$sidebarToggleButton.find("i")
		.removeClass("icon-menu")
		.addClass("icon-arrow-left");

		$sideNavContainer.css("margin-left", 0);
		$navBar.css("margin-left", sideNavWidth+"px");
		if($w.width() > 768) {
			$contentContainer.css("padding-left", sideNavWidth+"px");
$sideNavContainer.css("width", sideNavWidth+"px")
		}
		if($w.width() < 768) {
			$navBar.css("margin-left", sideNavWidthMob+"px");
			$sideNavContainer.css("width", sideNavWidthMob+"px")
		}
	}

	function hideSidebar() {
		isVisible = false;
		$sidebarToggleButton.find("i")
		.removeClass("icon-arrow-left")
		.addClass("icon-menu");

		$sideNavContainer.css("margin-left", -sideNavWidth+"px");
		$navBar.css("margin-left", 0);
		$contentContainer.css("padding-left", 0);
		if($w.width() > 768) {
			$sideNavContainer.css("margin-left", -sideNavWidth+"px");
		}
		if($w.width() < 768) {
			$sideNavContainer.css("margin-left", -sideNavWidthMob+"px");
		}
	}

	function toggleSidebar() {
		if(isVisible) {
			hideSidebar();
		} else {
			showSidebar();
		}
	}

	function checkWidth() {
		if($w.width() < 768) {
			hideSidebar();
		} else {
			showSidebar();
		}
	}
})
