import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueValidator from 'vue-validator'

import mainpage from './vue-components/mainpage.vue'
import UserForm from './vue-components/UserForm.vue'
import LocationForm from './vue-components/LocationForm.vue'
import ProductForm from './vue-components/ProductForm.vue'
import AddToCart from './vue-components/AddToCart.vue'
import AddCharges from './vue-components/AddCharges.vue'
import AddPlan from './vue-components/AddPlan.vue'
import AddAmenity from './vue-components/AddAmenity.vue'
import AddMessage from './vue-components/AddMessage.vue'

// Product Categories
import CategoryListComponents from './vue-components/CategoryListComponents.vue'
import CategoryAddComponents from './vue-components/CategoryAddComponents.vue'
import CategoryEditComponents from './vue-components/CategoryEditComponents.vue'
import CategoryDeleteComponents from './vue-components/CategoryDeleteComponents.vue'
    
// Event Components
import EventComponent from './vue-components/EventComponent.vue'
import AddEventComponent from './vue-components/AddEventComponent.vue'
import EditEventComponent from './vue-components/EditEventComponent.vue'

// Room Management
import RoomComponent from './vue-components/RoomComponent.vue'
import AddRoomComponent from './vue-components/AddRoomComponent.vue'
import EditRoomComponent from './vue-components/EditRoomComponent.vue'

// Room Management
import ReservationComponent from './vue-components/ReservationComponent.vue'
import ReservationRequestComponent from './vue-components/ReservationRequestComponent.vue'
import ReservationChangeComponent from './vue-components/ReservationChangeComponent.vue'
import ReservationViewComponent from './vue-components/ReservationViewComponent.vue'
import ReservationCancelComponent from './vue-components/ReservationCancelComponent.vue'
import ReservationRejectComponent from './vue-components/ReservationRejectComponent.vue'

// Notification
import ActionOptionComponent from './vue-components/ActionOptionComponent.vue'
import notificationSubRoute from './vue-subroutes/notification.js'
import Success from './vue-components/success.vue'
import helperFunctions from './vue-mixins/helperFunctions.js'
import CartUpdater from './vue-mixins/CartUpdater.js'

import PayCashComponent from './vue-components/PayCashComponent.vue'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueValidator)

/**
 * Converts a snake case string to title case.
 * Example: snake_case => Snake Case
 * 
 * @param  {String} str the string to convert
 * @return {String}
 */
Vue.filter('snakeToTitle', function (str) {  
    return str.split('_').map(function (item) {
        return item.charAt(0).toUpperCase() + item.substring(1);
    }).join(' ');
});

Vue.filter('singularize', function (str) {
    return str.slice('s', -1);
});

// Global Declaration of CSRF Token
var token = document.querySelector('input[name="_token"]');
if (token) {
    Vue.http.headers.common['X-CSRF-TOKEN'] =  token.value;
    Vue.http.options.emulateJSON = true;
}

// AJAX Drirect for Form Submission
Vue.directive('ajax', {
    bind: function() {
        this.el.addEventListener('submit', this.onSubmit.bind(this));
    },
    update: function( value ) {
        if (value) {
            console.log(value);
        }
    },
    onSubmit: function(e) {
        e.preventDefault();
        var fields = this.vm.fields;

        // Enabled if VM is requesting loader to show
        if (this.vm.loader !== undefined) {
            router.app.loading = true;
            router.app.loading_msg = 'Please wait while we are processing your request...';
        }

        var goSubmit = true;

        if ( this.vm.validForm !== undefined )
            goSubmit = this.vm.validForm;

        if (this.el.enctype == 'multipart/form-data')
            fields = this.onSubmitWithFile();
        
        if (goSubmit) {
            this.vm
                .$http[this.getRequestType()](this.el.action, fields  )
                .then(this.onComplete.bind(this))
                .catch(this.onError.bind(this));
        }
        else {
            console.log('Invalid Form Submission');
        }

    },
    onSubmitWithFile() {
        var files = this.vm.$els.imageInput.files;
        var data = new FormData();

        var data = this.generateFields(data);

        // console.log(files);
        // console.log(data);

        if (files[0]) {
            data.append('image', files[0]);
        }

        return data;
    },
    onComplete: function(response) {
        console.log('onComplete: '+response.data);

        router.app.loading_msg = 'Loading...';

        // this condition applies if VM has enabled loader
        if (this.vm.loader !== undefined)
            router.app.loading = false;

        if (!response.data.error) {

            if ( this.vm.validForm === undefined )
                this.vm.fields = [];

            if (this.vm.profilepage) 
                router.go('/success/Member profile has been updated.');
            else if (this.vm.categorylist !== undefined) {
                router.go('/categories/list');
            }
            else 
                router.go('/');

            if (response.data.reload)
                window.location.reload();
        }
        else {
            if (response.data.popupnotify !== undefined) {
                this.displaySubRouteError(response.data);
            }
        }
    },
    onError: function(response) {
        console.log('Response: '+response);
        if (response == 'Unauthorized.')
            window.location.reload();

        if (this.vm.loader !== undefined)
            router.app.loading = false;
        
        this.displayLoginError(response.data);
    },
    getRequestType: function() {
        var method = this.el.querySelector('input[name="_method"]');
        return (method ? method.value : this.el.method).toLowerCase();
    },
    displayLoginError: function(data) {
        // Display logged in error messages
        alert('Error submission.');
    },
    displaySubRouteError: function(data) {
        this.vm.error.header = data.header;
        this.vm.error.message = data.msg;

        router.go(this.vm.$route.path + data.url);   
    },
    generateFields(data) {
        if (this.vm.profilepage) {
            // For profile upload
            data.append('first_name', this.vm.first_name);
            data.append('last_name', this.vm.last_name);
            data.append('age', this.vm.age);
            data.append('gender', this.vm.gender);
            data.append('email', this.vm.email);
        } else {
            // For product upload
            data.append('product_name', this.vm.fields.product_name);
            data.append('sku', this.vm.fields.sku);
            data.append('price', this.vm.fields.price);
            data.append('category_id', this.vm.fields.category_id);
        }

        return data;
    }
})



var App = Vue.extend({
    mixins: [helperFunctions, CartUpdater],
    data: function() {
        return {
            location: 0,
            room: 0,
            cartItemCount: 0,
            genders: ['Male', 'Female'],
            image: '',
            access: 0,
        };
    },
    created: function() {
    	this.initializeApp();
    },
    methods: {
    	initializeApp() {
    		console.log('Vue application created.');

            // get user cart items
            this.$http.get('/cart/count', function (response) {
                    this.cartItemCount = response;
                }.bind(this));
    	},
        previewThumbnail() {
            var input = event.target;

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = this;

                reader.onload = function(e) {
                    img.image = e.target.result;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    },
})

var router = new VueRouter()

router.map({
    '/success/:m': {
        component: Success,
        subRoutes: notificationSubRoute,
    },
    '/addclient': {
    	name: 'addclient',
        component: UserForm,
        subRoutes: notificationSubRoute,
    },
    '/editclient/:id': {
    	name: 'editclient',
    	component: UserForm,
        subRoutes: notificationSubRoute,
    },
    '/addlocation': {
        name: 'addlocation',
        component: LocationForm,
        subRoutes: notificationSubRoute,
    },
    '/editlocation/:id': {
        name: 'editlocation',
        component: LocationForm,
        subRoutes: notificationSubRoute,
    },
    '/addproduct': {
        name: 'addproduct',
        component: ProductForm,
        subRoutes: notificationSubRoute,
    },
    '/editproduct/:id': {
        name: 'editproduct',
        component: ProductForm,
        subRoutes: notificationSubRoute,
    },
    '/addcart/:id/:name/:price' : {
        name: 'addtocart',
        component: AddToCart,
        subRoutes: notificationSubRoute,
    },
    '/charge/:id/:name': {
        name: 'addcharges',
        component: AddCharges,
        subRoutes: notificationSubRoute,
    },
    '/addplan': {
        name: 'addplan',
        component: AddPlan,
        subRoutes: notificationSubRoute,
    },
    '/editplan/:id': {
        name: 'editplan',
        component: AddPlan,
        subRoutes: notificationSubRoute,
    },
    '/addamenity': {
        name: 'addamenity',
        component: AddAmenity,
        subRoutes: notificationSubRoute,
    },
    '/editamenity/:id': {
        name: 'editamenity',
        component: AddAmenity,
        subRoutes: notificationSubRoute,
    },
    '/pay-cash/:id': {
        name: 'process-payment',
        component: PayCashComponent,
        subRoutes: notificationSubRoute,
    },
    '/message': {
        name: 'newmessage',
        component: AddMessage,

        subRoutes: {
            '/edit/:id': {
                component: AddMessage,
                subRoutes: notificationSubRoute,
            }
        }
    },
    '/categories': {
        component: {
            template: '<router-view></router-view>',
        },
        subRoutes: {
            '/list': {
                name: 'categories',
                component: CategoryListComponents,
                subRoutes: notificationSubRoute,
            },
            '/add': {
                name: 'addcategory',
                component: CategoryAddComponents,
                subRoutes: notificationSubRoute,
            },
            '/edit/:id': {
                name: 'editcategory',
                component: CategoryEditComponents,
                subRoutes: notificationSubRoute,
            },
            '/delete/:id': {
                name: 'deletecategory',
                component: CategoryDeleteComponents,
                subRoutes: notificationSubRoute,
            }
        }
    },
    '/eventCal': {
        component: {
            template: '<router-view></router-view>',
        },
        subRoutes: {
            '/get-action/:id': {
                component: EventComponent,
                subRoutes: notificationSubRoute,
            },
            '/add/:location/:date/:start/:end': {
                name: 'addevent',
                component: AddEventComponent,
                subRoutes: notificationSubRoute,
            },
            'edit/:id': {
                name: 'editevent',
                component: EditEventComponent,
                subRoutes: notificationSubRoute,
            },
        }
    },
    '/room': {
        component: RoomComponent,
        subRoutes: {
            '/add/:location': {
                name: 'addroom',
                component: AddRoomComponent,
                subRoutes: notificationSubRoute,
            },
            '/edit/:id': {
                name: 'editroom',
                component: EditRoomComponent,
                subRoutes: notificationSubRoute,
            }
        }
    },
    '/reservation': {
        component: ReservationComponent,
        subRoutes: {
            '/option/:module/:id/:viewname/:editname': {
                component: ActionOptionComponent,
            },
            '/view/:id': {
                name: 'view_reservation',
                component: ReservationViewComponent,
                subRoutes: notificationSubRoute,                
            },
            '/request/:location/:room/:date/:start/:end': {
                name: 'request_reservation',
                component: ReservationRequestComponent,
                subRoutes: notificationSubRoute,
            },
            '/change/:id': {
                name: 'change_reservation',
                component: ReservationChangeComponent,
                subRoutes: notificationSubRoute,
            },
            '/cancel/:id': {
                name: 'cancel_reservation',
                component: ReservationCancelComponent,
                subRoutes: notificationSubRoute,
            },
            '/reject/:id': {
                name: 'reject_reservation',
                component: ReservationRejectComponent,
                subRoutes: notificationSubRoute,
            }
        }
    },
    '/': {
        component: mainpage,
    },
})

    // router.beforeEach(function (transition) {
    //     // Authentical Session Access

    //     var authenticated = window.userId;
    //     if (!authenticated) {
    //         window.location.reload();
    //     }

    //     transition.next();
    // })


router.start(App, 'body')
