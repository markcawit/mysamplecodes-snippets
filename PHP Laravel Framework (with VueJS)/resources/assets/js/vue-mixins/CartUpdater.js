export default {
	data() {
		return {
			item_price: null,
			total_price: null,
			quantity: null,
			discount: 0,
			discount_view: '',
			total_amount_due: 0,
			membership: 0,
			total_recurring: 0,
		};
	},

	ready() {
		var discount = (this.discount*1); // multiplied by one (1) to convert to number/integer
		this.discount_view = '$' + discount.toFixed(2) + ' USD';
	},

	methods: {
        handleCartItemUpdate(ind) {
            if ( this.quantity[ind] < 1 ) {
                this.quantity[ind] = 1;
            }

            var total = this.item_price[ind] * this.quantity[ind];
            this.total_price[ind] = '$' + total.toFixed(2) + ' USD';

            this.getTotalAmountDue();
        },

        getTotalAmountDue() {
            var key;
            var qty = this.quantity;
            var price = this.item_price;
            var totalDue = (this.total_recurring*1); // multiplied by one (1) to convert to number/integer
            for (key in qty) {
			    if (String(parseInt(key, 10)) === key && qty.hasOwnProperty(key)) {
			        totalDue += qty[key] *  price[key];
			    }
			}
			this.total_amount_due = '$' + totalDue.toFixed(2) + ' USD';
        },

        updateCart() {
        	var data = {
        		quantity: this.quantity,
        		price: this.item_price
        	}
        	this.$http.post('cart/update', data, function (response) {
        		console.log(response.error);
        		if(!response.error)
        			this.$root.submitSuccess('Cart Updated!');
			}.bind(this));
        },
	}
}