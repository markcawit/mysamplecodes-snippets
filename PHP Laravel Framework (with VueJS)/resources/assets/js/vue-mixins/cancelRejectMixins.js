import formValidator from '../vue-mixins/formValidator.js'

export default {
    mixins: [formValidator],
    data() {
        return {
            title: 'Cancel Reservation',
            action: '/rooms/reservation/request/cancel',
            method: 'POST',
            fields: {
                reason: '',
            },
            error: {
                header: '',
                message: ''
            },
            loader: true,
        };
    },

    ready() {
        console.log('CancelReject Reservation Components..');
        this.$root.loading = false;
    },

    methods: {
        invalidFormEntry() {
            this.showPopup('/required', 'Required Field', 'Please state the reason for this action.');
        },
    },
}