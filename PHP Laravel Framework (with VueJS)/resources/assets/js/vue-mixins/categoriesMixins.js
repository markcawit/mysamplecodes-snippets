import formValidator from '../vue-mixins/formValidator.js'

export default {
    mixins: [formValidator],

    data() {
        return {
            title: 'List of Categories',
            action: '/shop/dashboard/category/add',
            method: 'POST',
            categories: [],
            fields: {
                category_name: '',
                slug: '',
            },
            loader: true,
            categorylist: true,
            button: 'Submit',

            error: {
                header: '',
                message: '',
            },
        };
    },

    ready() {
        console.log('CategoriesComponent initialized.');
    },

    methods: {
        getCategories() {
            this.$http.get('/shop/dashboard/fetch-categories', function (response) {
                    var categories = response;                    
                    for (var i=0; i<categories.length; i++) {
                        this.categories.push({
                                id: categories[i].id,
                                name: categories[i].category_name,
                                slug: categories[i].slug,
                            });
                    }

                    this.$root.loading = false;
                }.bind(this));

        }
    },
}