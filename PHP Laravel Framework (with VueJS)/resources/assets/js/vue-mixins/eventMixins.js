import formValidator from '../vue-mixins/formValidator.js'

export default {
    mixins: [formValidator],

    data() {
        return {
            action: '/events/add',
            title: 'New Event',
            method: 'POST',
            fields: {
                title: '',
                description: '',
                date: null,
                start_time: '00:00:00',
                end_time: '00:00:00',
                color: '',
                location_id: 0,
            },

            error: {
                header: 'Header',
                message: 'Message',
            },
            colors: ['orange','blue','green','yellow'],
            button: 'Save Event',
        };
    },

    ready() {
        console.log('EventComponent initialized.');

        if (this.$route.params.id) {
            this.fetchEvent();
        }
        else {
            this.fields.date = this.$route.params.date;
            this.fields.start_time = this.$route.params.start;
            this.fields.end_time = this.$route.params.end;

            this.fields.location_id = this.$route.params.location;

            this.$root.loading = false;
        }
    },

    methods: {
        fetchEvent() {
            var id  = this.$route.params.id;

            // Change form action attribute with update
            this.action = '/events/update/'+id;

            this.$http.get('/events/edit/'+id, function (response) {
                    // console.log(response);
                    this.fields = {
                        title: response.event_title,
                        description: response.description,
                        date: response.event_date,
                        start_time: response.start_time,
                        end_time: response.end_time,
                        color: response.color,
                        location_id: response.location_id,
                    }

                    this.title = 'Edit Event';
                    this.button = 'Update';

                    this.$root.loading = false;
                }.bind(this));

        }
    },
}