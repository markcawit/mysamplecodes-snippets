export default {
    validators: { // `numeric` and `url` custom validator is local registration
        numeric: function (val/*,rule*/) {
            return /^[-+]?[0-9]+$/.test(val)
        },
        url: function (val) {
            return /^(http\:\/\/|https\:\/\/)(.{4,})$/.test(val)
        },
        notzero: function (val) {
            return (parseInt(val) === 0) ? false : true;
        }
    },

    methods: {
        invalidFormSubmit() {
            alert('Please fill required fields with *.');
        },
        requiredField() {
            this.showPopup('/required', 'Invalid Entry', 'Please complete all required (<span class="text-red">*</span>) fields');
        },
        validClassIndicator() {
            return { 
                valid: 'valid-entry', 
                invalid: 'invalid-entry' 
            };
        },
        showPopup(route, header, message) {
            this.error.header = header;
            this.error.message = message;

            var path = (this.error.path !== undefined) ? this.error.path : this.$route.path;
            
            // window.setTimeout( function() {
                this.$route.router.go(path + route);   
            // }.bind(this), 50);
        },
    }
};