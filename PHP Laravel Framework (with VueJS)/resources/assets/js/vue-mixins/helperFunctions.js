export default {
	data() {
		return {
			loading: true,
			loading_msg: 'Loading...',
		};
	},

	methods: {
		hideLoader() {
            window.setTimeout(function(){
                this.loading = false;
            }.bind(this), '1000');
		},
		selectLocation() {
			// View users by location
			window.location.href = '/clients/' + this.location;
		},
		changeEventLocation() {
			window.location.href = '/events/' + this.location;
		},
		changeRoomLocation() {
			window.location.href = '/rooms/management/' + this.location;
		},
		changeReservationLocation() {
			window.location.href = '/rooms/' + this.location;
		},
		changeReservationRoom() {
			window.location.href = '/rooms/' + this.location + '/' + this.room;
		},
        submitSuccess(title) {
            this.$router.go('/success/'+title);
        },
        refreshPendingRequestsLocation() {
        	window.location.href = '/rooms/reservation/request/'+this.location;
        },
        refreshPendingRequests() {
        	window.location.href = '/rooms/reservation/request/'+this.location+'/'+this.room;
        },
	},

};
