import formValidator from '../vue-mixins/formValidator.js'

export default {
    mixins: [formValidator],
    data() {
        return {
            title: 'Reservation Request',
            action: '/rooms/reservation/store',
            method: 'POST',
            fields: {
                note: '',
                room_id: 0,
                location_id: 0,
                start_date: null,
                end_date: null,
                date: null,
                start_time: null,
                end_time: null,
            },
            rooms: [],
            invalid_entry: false,
            button: 'Submit',
            error: {
                header: '',
                message: '',
            },
            validForm: false,
            allowSubmit: false,
            cancelButton: false,
        };
    },

    ready() {
        console.log('ReservationComponent initialized.');
        this.rooms = window.rooms;
    },

    watch: {
        'fields.start_time': function(newTime, oldTime) {
            var startdate = new Date(this.fields.date + ' ' + newTime);
            var enddate = new Date ( startdate );
            var syear = startdate.getFullYear(),
                smont = ((startdate.getMonth()+1) < 10) ? "0"+(startdate.getMonth()+1): (startdate.getMonth()+1),
                stday = (startdate.getDate() < 10) ? "0"+startdate.getDate(): startdate.getDate(),
                sdate = syear + "-" + smont + "-" + stday;
            var shour = (startdate.getHours() < 10) ? "0"+startdate.getHours() : startdate.getHours(),
                smins = (startdate.getMinutes() < 10) ? "0"+startdate.getMinutes() : startdate.getMinutes(),
                ssecs = ":00",
                stime = shour + ":" + smins + ssecs;

            // Add two hours from Start Date/Time
            enddate.setHours ( startdate.getHours() + 2 );
            var syear = enddate.getFullYear(),
                smont = ((enddate.getMonth()+1) < 10) ? "0"+(enddate.getMonth()+1): (enddate.getMonth()+1),
                stday = (enddate.getDate() < 10) ? "0"+enddate.getDate(): enddate.getDate(),
                edate = syear + "-" + smont + "-" + stday;
            var ehour = (enddate.getHours() < 10) ? "0"+enddate.getHours() : enddate.getHours(),
                emins = (enddate.getMinutes() < 10) ? "0"+enddate.getMinutes() : enddate.getMinutes(),
                esecs = ":00",
                etime = ehour + ":" + emins + esecs;

            this.fields.start_date = sdate + " " + stime;
            this.fields.end_date = edate + " " + etime;

            this.fields.end_time = etime;
        },
        'fields.end_time': function(newTime, oldTime) {
            this.checkEndTime( newTime, oldTime );
        },
    },

    computed: {
        validForm: function() {
            this.checkSelectedRoom();
            this.checkEndTime(this.fields.end_time, 0);

            return (this.allowSubmit) ? true : false;
        },
        halfbutton: function() {
            return (this.cancelButton) ? 'col-sm-6' : 'col-xs-12';
        }
    },

    methods: {
        submitForm(e) {
            e.preventDefault();
            return this.validForm;
        },
        invalidFormEntry() {
            this.showPopup('/required', 'Invalid Entry', 'Please complete all required (*) fields');
        },
        formatDate(dt) {
        },
        checkEndTime( newTime, oldTime ) {
            var startdate = new Date(this.fields.start_date);
            var enddate = new Date(this.fields.date + ' ' + newTime);

            var timelimit = new Date( this.fields.start_date );
            timelimit.setHours ( startdate.getHours() + 2 );

            if ( enddate.getTime() <= startdate.getTime() ){
                this.allowSubmit = false;
                this.showPopup('/error', 'Invalid Time', 'End time should not be earlier or the same time with Start time.');
            }
            else if ( enddate.getTime() > timelimit.getTime() ) {
                this.allowSubmit = false;
                this.showPopup('/error', 'Time Limit', 'You can only reserve a room for a limited time of 2 hours for each reservation.');
            }
            else {
                var syear = enddate.getFullYear(),
                    smont = ((enddate.getMonth()+1) < 10) ? "0"+(enddate.getMonth()+1): (enddate.getMonth()+1),
                    stday = (enddate.getDate() < 10) ? "0"+enddate.getDate(): enddate.getDate(),
                    edate = syear + "-" + smont + "-" + stday;
                var ehour = (enddate.getHours() < 10) ? "0"+enddate.getHours() : enddate.getHours(),
                    emins = (enddate.getMinutes() < 10) ? "0"+enddate.getMinutes() : enddate.getMinutes(),
                    esecs = ":00",
                    etime = ehour + ":" + emins + esecs;

                this.fields.end_date = edate + " " + etime;
                this.allowSubmit = true;
            }
        },
        checkSelectedRoom() {
            if (parseInt(this.fields.room_id) == 0) {
                this.allowSubmit = false;
                this.showPopup('/required', 'Room Selection', 'Oops! You haven\'t selected a room yet.');
            }
            else {
                this.allowSubmit = true;
            }
        },
    },
}