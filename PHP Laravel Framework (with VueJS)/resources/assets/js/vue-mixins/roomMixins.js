import formValidator from '../vue-mixins/formValidator.js'

export default {
    mixins: [formValidator],

    data() {
        return {
            title: 'Add New Room',
            action: '/rooms/management/store',
            method: 'POST',
            fields: {
                name: '',
                location_id: 0,
            },
            invalid_entry: false,
            loaded: false,
            button: 'Submit',

            error: {
                header: '',
                message: '',
            },
        };
    },

    ready() {
        console.log('RoomComponent initialized.');
    },

    methods: {

    },
}