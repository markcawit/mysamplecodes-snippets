import ErrorNotifyComponent from '../vue-components/ErrorNotifyComponent.vue'
import LoaderComponent from '../vue-components/LoaderComponent.vue'

export default {
    '/error': {
    	name: 'error-notify',
        component: ErrorNotifyComponent,
    },
    '/required': {
    	name: 'invalid-form',
    	component: ErrorNotifyComponent,
    },
    '/invalid': {
    	name: 'invalid-entries',
    	component: ErrorNotifyComponent,
    },
    '/loading': {
        name: 'loader',
        component: LoaderComponent,
    }
}