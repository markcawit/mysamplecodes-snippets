@extends('layouts.default.master')

@section('title', 'Amenities | ')
@section('pageTitle', 'Amenities Management' . Helpers::breadcrumbs(['amenities'=>'List of Amenities'], 'amenities'))

@section('content')
<div class="container-fluid">
    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
					<div class="form-group">
                    	<a v-link="{ path: '/addamenity' }" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Amenity</a>
					</div>
				</div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="action-controls"></th>
                        <th>Amenities</th>
                        <th>Status</th>
                        <th>Fee</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $amenities as $amenity )
                        <tr>
                            <td>
                                @can('edit_membership')
                                    <a data-toggle="tooltip" data-placement="top" title="Edit Amenity" v-link="{ name: 'editamenity', params: { id: {{$amenity->id}} }}">
                                        <i class="icon-note"></i>
                                    </a>&nbsp;
                                @endcan
                                @can('delete_membership')
                                    <a data-toggle="tooltip" data-placement="top" title="Delete Amenity" href = "{{ route('amenities-destroy', $amenity->id) }}" onclick="return confirm('Are you sure you want to Delete this Amenity?')">
                                        <i class="icon-trash"></i>
                                    </a>&nbsp;
                                @endcan
                            </td>
                            <td>{{ $amenity->name }}</td>
                            <td>{{ $amenity->status }}</td>
                            <td class="price">{{ currency($amenity->fee) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection
