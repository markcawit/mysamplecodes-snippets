@extends('layouts.default.master-no-auth')

@section('head_script')
<script>
	var userId = 1;
</script>
@endsection

@section('content')
<div class="container-fluid">
	<div class="centered-form-container">
		<div class="panel panel-default centered-panel">
			<div class="panel-heading">Login</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
					{{ csrf_field() }}
					<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
						<label for="username">Username</label>
						<input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">

						@if ($errors->has('username'))
						<span class="help-block">
							<strong>{{ $errors->first('username') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password">Password</label>
						<input id="password" type="password" class="form-control" name="password">

						@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group">
						<div class="checkbox">
							<span class="clearfix">
								<span class="pull-left">
									<label class="remember-me">
									<input type="checkbox" name="remember"> Remember Me
								</label>
								</span>
								<span class="pull-right">
									<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
								</span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							<span class="clearfix">
								<span class="pull-left">Login</span>
								<span class="pull-right"><i class="fa fa-btn fa-check-circle"></i></span>
							</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>

</style>
@endsection
