@extends('layouts.default.master')

<!-- Main Content -->
@section('content')
<div class="container-fluid">
	<div class="centered-form-container">
		<div class="panel panel-default centered-panel">
			<div class="panel-heading">Reset Password</div>
			<div class="panel-body">
				@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
					{{ csrf_field() }}

					<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
						<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="enter your email address">
						@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							<i class="fa fa-btn fa-paper-plane"></i> Send Password Reset Link
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
