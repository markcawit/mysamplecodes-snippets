@extends('layouts.default.master-no-auth')

@section('content')
<div class="container">
	<div class="centered-form-container">
		<div class="panel panel-default centered-panel">
			<div class="panel-heading">Register</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Name</label>
						<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
						<label for="name">Username</label>
						<input id="name" type="text" class="form-control" name="username" value="{{ old('username') }}">

						@if ($errors->has('username'))
						<span class="help-block">
							<strong>{{ $errors->first('username') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">E-Mail Address</label>
						<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

						@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password">Password</label>
						<input id="password" type="password" class="form-control" name="password">

						@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<label for="password-confirm">Confirm Password</label>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation">

						@if ($errors->has('password_confirmation'))
						<span class="help-block">
							<strong>{{ $errors->first('password_confirmation') }}</strong>
						</span>
						@endif
					</div>
					<br>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							<span class="clearfix">
								<span class="pull-left">Register</span>
								<span class="pull-right"><i class="fa fa-btn fa-check-circle"></i></span>
							</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
