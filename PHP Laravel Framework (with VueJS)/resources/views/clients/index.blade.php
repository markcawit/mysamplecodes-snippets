@extends('layouts.default.master')

@section('title', 'Clients | ')
@section('pageTitle', 'Member Management' . Helpers::breadcrumbs( $breadcrumbs, $active_page) )

@section('content')
<div class="container-fluid">

    @include('partials.message')

    <div class="panel panel-default">
        {{-- List Heading --}}
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">

                        <a v-link="{ path: '/addclient' }" class="btn btn-primary">
                            <i class="icon-plus"></i> Add Member
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">

                    <form method="get" action="{{ route('clients')}}">
                        <div class="input-group search-group">
                            <input type="text" class="form-control" name="search" placeholder="Search for..." value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                    <select v-model="location" @change="selectLocation()" class="form-control">
                        <option value="">View Users by Location</option>
                        @foreach($locations as $loc)
                        <option {{ $location == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">{{$loc->location_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="action-controls"></th>
                        <th><a href="{{ route('clients').'/'.$location.'?page='.$page_number.'&search='.$search.'&order_by=email&sort='.$sort }}">Email</a></th>
                        <th><a href="{{ route('clients').'/'.$location.'?page='.$page_number.'&search='.$search.'&order_by=username&sort='.$sort }}">Username</a></th>
                        <th><a href="{{ route('clients').'/'.$location.'?page='.$page_number.'&search='.$search.'&order_by=first_name&sort='.$sort }}">First Name</a></th>
                        <th><a href="{{ route('clients').'/'.$location.'?page='.$page_number.'&search='.$search.'&order_by=last_name&sort='.$sort }}">Last Name</a></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $row)
                    <tr>
                        <td style="text-align:center">
                            <a data-toggle="tooltip" data-placement="top" title="View Profile of {{ $row->full_name  }}" href = "{{ route('client_profile', $row->id) }}"><i class="icon-eye"></i></a>&nbsp;

                            @can('update_user')
                                <a data-toggle="tooltip" data-placement="top" title="Edit Member" v-link="{ name: 'editclient', params: { id: {{$row->id}} }}">
                                    <i class="icon-note"></i>
                                </a>&nbsp;
                            @endcan
                            @can('delete_user')
                                <a data-toggle="tooltip" data-placement="top" title="Delete Member" href = "{{ route('client_destroy', $row->id) }}" onclick="return confirm('Are you sure you want to Delete this client?')">
                                    <i class="icon-trash"></i>
                                </a>&nbsp;
                            @endcan

                            </td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->username }}</td>
                            <td>{{ $row->first_name  }}</td>
                            <td>{{ $row->last_name  }}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @if($users->count() == 0)
                <div style="text-align:center; color:#333; font-size:18px">No records to show</div>
                @if($search)
                <div style="text-align:center; color:#333; font-size:18px">for {{ $search }}</div>
                @endif
                @endif
        </div>

    </div>
</div>
{!! csrf_field() !!}
@endsection
