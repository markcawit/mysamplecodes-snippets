@extends('layouts.default.master')

@section('title', $user->full_name . ' Profile - ')
@section('pageTitle', 'Member Management')

@section('content')
<div class="container">
    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="">{{ $header = (Auth::user()->id != $user->id) ? $user->full_name ." Profile" : "My Account" }}</div>
        </div>
        <div class="panel-body panel-profile">
            <div class="row">
                <div class="col-md-6">
                    <h4><i class="icon-user"></i>&nbsp;</span>Personal Details</h4>
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" 
                                action="{{ url('clients/profile') . '/'. $user->id }}" novalidate 
                                v-ajax
                                >
                        <input type="hidden" v-model="profilepage" value="1">
                        <input type="hidden" v-model="image" value="{{ $user->PersonalInfo->image }}">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-8">
                                    <img style="width:200px; height:auto; padding-bottom:10px;"
                                            v-bind:src="image"
                                            >
                                </div>
                            </div>
                            <label class="col-sm-4 control-label" for="exampleInputFile">File input</label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control inputForm" 
                                            v-el:image-input id="imageInput" name="imageInput"
                                            @change="previewThumbnail"
                                            >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">First Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="first_name"  placeholder="First Name"
                                                        value="{{ $user->first_name }}"
                                                        v-model="first_name"
                                                        >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Last Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="last_name" placeholder="Last Name"
                                                        value="{{ $user->last_name }}"
                                                        v-model="last_name"
                                                        >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Age</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="age" placeholder=""
                                                        value="{{ $user->PersonalInfo->age }}"
                                                        v-model="age"
                                                        >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Gender</label>
                            <div class="col-sm-8">
                                <select class="form-control"
                                    v-model="gender">
                                    <option value="">Select Gender</option>
                                    <option v-for="gend in genders" 
                                            value="@{{ gend }}"
                                            :selected="gend == {{ json_encode($user->PersonalInfo->gender) }} ? true : false"
                                            >@{{ gend }}</option>
                                </select>
                            </div>
                        </div>
                        <h4>Account Details</h4>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="email">Email address</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="email" placeholder="Email" 
                                    value="{{ $user->email }}"
                                    v-model="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="email">Username</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="username" disabled placeholder="Username" 
                                    value="{{ $user->username }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-primary">
                                    Save Changes &nbsp;<i class="icon-cloud-upload"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            @if ($show_details)
                <div class="col-md-6">
                    <h4><i class="icon-notebook"></i>&nbsp;Membership Details</h4>
                    <form action="#" class="form-horizontal">
                    @if ($membership)
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12">Membership Plan</label>
                            <div class="col-sm-8 col-xs-12">
                                <strong>{{ $membership->name  }}</strong>
                            </div>
                        </div>
                    @endif

                    <br>

                    <h4><i class="icon-list"></i>&nbsp;Meeting Room Reservations</h4>
                    <p>All room reservation requests. <a href="{{ route('this_user_reservations', $user->id) }}">(View List)</a></p>
                    
                    <br>
                    
                    <h4><i class="icon-basket-loaded"></i>&nbsp; Current Additional Charges</h4>
                    <table class="table table-hover">
                        <tbody>
                            {{-- Additional Plans Added for Open Space (except Full Access) --}}
                            @if ($addplans->count())
                                @foreach($addplans as $plan)
                                <tr>
                                    <td>{{ $plan->Membership->name }}</td>
                                    {{-- <td>{{ $amenity->quantity }}</td> --}}
                                    <td>{{ $plan->created_at->format('m/d/Y') }}</td>
                                    <td>{{ currency($plan->total) }}</td>
                                </tr>
                                @endforeach
                            @endif

                            {{-- Additional Charges use of Amenities --}}
                            @if ($amenities->count())
                                @foreach($amenities as $amenity)
                                <tr>
                                    <td>{{ $amenity->Amenity->name }}</td>
                                    <td>{{ $amenity->created_at->format('m/d/Y') }}</td>
                                    <td>{{ currency($amenity->total) }}</td>
                                </tr>
                                @endforeach
                            @endif

                            @if ($addplans->count() == 0 && $amenities->count() == 0)
                                <tr>
                                    <td colspan="3">No Additional Charges</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <br><br>
                    @can('add_charges')
                    <div class="form-group">
                        <div class="col-xs-12">
                            <a class="btn btn-primary"
                                v-link="{ name: 'addcharges', params: {
                                            id: {{ $user->id }},
                                            name: '{{ $user->full_name }}',
                                            } 
                                        }">
                                Add Charges to <i class="icon-basket"></i>
                            </a>

                            <a href="{{ route('cart-member', ['id'=>$user->id]) }}" class="btn btn-primary">
                                View Cart <i class="icon-basket-loaded"></i>
                            </a>
                        </div>
                    </div>
                    @endcan
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection
