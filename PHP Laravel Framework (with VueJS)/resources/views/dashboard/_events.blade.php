	<div class="panel panel-default panel-toggle">
		<div class="panel-heading">
			<span class="clearfix">
				<span class="pull-left">
					<i class="icon-calendar"></i> Upcoming Events
				</span>
				<span class="pull-right">
					<a href="#" class="panel-close">
						<i class="icon-arrow-right"></i>
					</a>
				</span>
			</span>
		</div>
		<div class="panel-body">
			@if ($events->count())
				@foreach($events as $event)
					<div class="notification-item" data-anchor>
						<span class="clearfix">
							<span class="pull-right">
								<span class="notification-hour">{{ $event->formatEventDate() }}</span>
							</span>
						</span>
						<span class="clearfix">
							<span class="pull-left">
								<h5><a href="{{ route('show_events', $event->id) }}" data-internal target="_blank">{{ $event->event_title }}</a></h5> 
								<h6>at <span>{{ $event->location->name }}</span></h6>
							</span>
						</span>
						<p>{{ $event->getDescriptionExcerpt(30) }}</p>
					</div>
				@endforeach
			@else
				<span class="text-sm text-gray"><i>No upcoming events for the next 30 days.</i></span>
			@endif
		</div>
	</div>
