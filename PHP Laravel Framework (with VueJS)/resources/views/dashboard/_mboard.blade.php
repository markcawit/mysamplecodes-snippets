	<div class="panel panel-default panel-toggle">
		<div class="panel-heading">
			<span class="clearfix">
				<span class="pull-left">
					<i class="icon-pin"></i> Message Board
				</span>
				<span class="pull-right">
					<a href="#" class="panel-close">
						<i class="icon-arrow-right"></i>
					</a>
				</span>
			</span>
		</div>
		<div class="panel-body">
			@if($posts->count())
				@foreach($posts as $post)
					<div class="notification-item" data-anchor>
						<span class="clearfix">
							<span class="pull-left">
								<h5><a href="{{ route('message_show', $post->id) }}" data-internal targe="_blank">{{ $post->short_title }}</a></h5>
							</span>
							<span class="pull-right">
								<span class="notification-hour">{{ $post->formatDatePosted() }}</span>
							</span>
						</span>
						<p>{{ $post->getExcerptAttribute(30) }}</p>
					</div>
				@endforeach
			@else
				<span class="text-sm text-gray"><i>No announcement/pinned post yet.</i></span>
			@endif
		</div>
	</div>
