			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<span class="clearfix">
						<span class="pull-left">
							<i class="icon-feed"></i> Notifications
						</span>
						<span class="pull-right">
							<a href="#" class="panel-close">
								<i class="icon-arrow-right"></i>
							</a>
						</span>
					</span>
				</div>{{-- reservation_request --}}
				<div class="panel-body">
					<div class="clearfix">
						<div class="pull-right">
							<a href="{{ route('all_reservations') }}"{{-- reservation_request --}}
								class="text-sm text-underline">All Reservation Requests</a>
						</div>
					</div>
					<br>
					@if ($reservations->count())
						@foreach($reservations as $reservation)
							<div class="notification-item" data-anchor>
								<span class="clearfix">
									<span class="pull-left">
										<h6><a href="{{ 
												route('reservation_item', $reservation->id) 
												}}" data-internal target="_blank"
												title="{{ $reservation->date_time_a }}"
												>{{ $reservation->request_title }}</a></h6> 
										{{-- <h6>{!! $reservation->date_time_a !!}</h6> --}}
									</span>
								</span>
								<span class="clearfix">
									<span class="pull-right">
										<span class="notification-hour">{{ $reservation->time_ago }}</span>
									</span>
								</span>
								{{-- <p>
									@if(!empty($reservation->note))
										{!! $reservation->note !!}
									@else
									@endif
								</p> --}}
							</div>
						@endforeach
					@else
						<p class="text-sm text-gray"><i>You don't have any reservation request at this time.</i></p>
					@endif
					{{-- <div class="notification-item">
						<span class="clearfix">
							<span class="pull-left">
								<h5>Notification item</h5>
							</span>
							<span class="pull-right">
								<span class="notification-hour">2h ago</span>
							</span>
						</span>
						<p>Lawrence ipsum dolor sit amet, consectetur adipiscing elit </p>
					</div>
					 
					<div class="notification-item">
						<span class="clearfix">
							<span class="pull-left">
								<h5>Notification item</h5>
							</span>
							<span class="pull-right">
								<span class="notification-hour">2h ago</span>
							</span>
						</span>
						<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>

					<div class="notification-item">
						<span class="clearfix">
							<span class="pull-left">
								<h5>Notification item</h5>
							</span>
							<span class="pull-right">
								<span class="notification-hour">2h ago</span>
							</span>
						</span>
						<p>Lorem ipsum dolor sit amet, consectetur  et dolore magna aliqua.</p>
					</div> --}}
				</div>
			</div>
