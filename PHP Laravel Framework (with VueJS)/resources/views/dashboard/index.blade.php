@extends('layouts.default.master')

@section('title', 'Dashboard | ')
@section('pageTitle', 'Dashboard')
@section('content')

<div class="container-fluid">
	<div class="row">
		@if (Auth::user()->hasRole('administrator'))
		<div class="col-md-4 col-sm-6 notification-dashboard">
			@include('dashboard._notif')
		</div>
		@endif
		<div class="
			@if(Auth::user()->hasRole('administrator')) col-sm-4 
			@else col-sm-6
			@endif
			col-xs-12 message-board-dashboard">
			@include('dashboard._mboard')
		</div>
		<div class="
			@if(Auth::user()->hasRole('administrator')) col-sm-4 
			@else col-sm-6
			@endif
			col-xs-12 events-dashboard">
			@include('dashboard._events')
		</div>
		{{-- <div class="col-md-4 col-sm-6">
			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<span class="clearfix">
						<span class="pull-left">
							Dashboard
						</span>
						<span class="pull-right">
							<a href="#" class="panel-close">
								<i class="icon-arrow-right"></i>
							</a>
						</span>
					</span>
				</div>
				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6">
			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<span class="clearfix">
						<span class="pull-left">
							Dashboard
						</span>
						<span class="pull-right">
							<a href="#" class="panel-close">
								<i class="icon-arrow-right"></i>
							</a>
						</span>
					</span>
				</div>
				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6">
			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<span class="clearfix">
						<span class="pull-left">
							Dashboard
						</span>
						<span class="pull-right">
							<a href="#" class="panel-close">
								<i class="icon-arrow-right"></i>
							</a>
						</span>
					</span>
				</div>
				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div> --}}
	</div>
</div>
@endsection

{{--@include('layouts.includes.edit-form')--}}
