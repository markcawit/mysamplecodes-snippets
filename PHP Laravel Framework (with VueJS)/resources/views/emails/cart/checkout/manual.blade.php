{{--*/ $payment = json_decode($cart->ref_payment) /*--}}
<p>Hi {{ $cart->user->first_name }},</p>

<br>

<p>Thank you for your payment amounting {{ currency($payment->amount) }} Over-the-Counter with your Cart/Invoice Number {{ $cart->invoice_number }}. </p>

<br>

<p>Your Membership Subscription/Cart has been reset.</p>

<br>

<p>See below the details of your payment: <br>
{{ Carbon\Carbon::parse($payment->or_date)->format('m/d/Y H:i:sA') }}| Transaction ID: {{ $cart->tnx_id }}</p>


<ul style="list-style: none; padding-left: 0; margin-top: 15px;">
    <li style="padding-bottom: 10px;">
        <strong>
            OR Number: 
            <span style="float: right;">#{{ $payment->or_number }}</span>
            </strong></li>
    <li style="padding-bottom: 10px;">
        <strong>
            Date Paid: 
            <span style="float: right;">
                {{ Carbon\Carbon::parse($payment->or_date)->format('m/d/Y h:i:sA') }}</span>
            </strong></li>
    <li style="padding-bottom: 10px;"></li>
    <li style="padding-bottom: 10px;">
        <strong>
            Amount Due: 
            <span style="float: right;">{{ currency($payment->amount) }}</span>
        </strong></li>
    <li style="padding-bottom: 10px;">
        Cash Tendered: <span style="float: right;">{{ currency($payment->cash) }}</span></li>
    <li style="padding-bottom: 10px;">
        Change: <span style="float: right;">{{ currency($payment->change) }}</span></li>
    
    @if ($payment->discount)
    <li style="padding-bottom: 10px;">
        Discount: <span style="float: right;">{{ currency($payment->discount) }}</span></li>
    @endif
</ul>
