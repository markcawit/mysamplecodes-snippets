<p>Hi {{ $cart->user->first_name }},</p>

<br>

<p>Thank you for your payment amounting ${{ $txn->amount->total }} {{ $txn->amount->currency }} to {{ $txn->payee->email}} from your Cart/Invoice Number {{ $txn->invoice_number }}. </p>

<br>

<p>Your Membership Subscription/Cart has been reset.</p>

<br>

<p>See below the details of your payment: <br>
{{ $paymentDate }}| Transaction ID: {{ $sale->id }}</p>

<br>
<br>
<p>{{ $txn->description }}</p>
<br>
<ul style="list-style: none; padding-left: 0; margin-top: 15px;">
    @foreach( $itemList->items as $items)
    <li style="padding-bottom: 10px;">
        Description: {{ $items->name }} <br>
        Unit price: ${{ $items->price }} {{ $items->currency }} <br>
        Qty: {{ $items->quantity }} <br>
        Amount: ${{ $items->price }} {{ $items->currency }}
    </li>
    @endforeach

    <li style="padding-bottom: 10px; padding: 5px 0px 10px 10px; border-bottom: 1px solid rgba(50, 50, 50, 0.1);">&nbsp;</li>
    <li style="padding-bottom: 10px;">&nbsp;</li>
    <li style="padding-bottom: 10px; margin-top: 15px;">
        Subtotal: ${{ $sale->amount->total }} {{ $sale->amount->currency }}
    </li>

    <li style="padding-bottom: 10px; margin-top: 15px;">
        Total: ${{ $sale->amount->details->subtotal }} {{ $sale->amount->currency }}
    </li>

    <li style="padding-bottom: 10px; padding: 5px 0px 10px 10px; border-bottom: 1px solid rgba(50, 50, 50, 0.1);">&nbsp;</li>
    <li style="padding-bottom: 10px;">&nbsp;</li>
    <li style="padding-bottom: 10px; margin-top: 15px;">
        Payment: ${{ $txn->amount->total }} {{ $txn->amount->currency }} <br>
        Payment sent to: {{ $txn->payee->email}} <br>
        <br>
        Invoice ID: {{ $txn->invoice_number }}
    </li>
</ul>
