<p>
  Your request for reservation has been APPROVED.
</p>
<p>
  Your Reservation Details:
</p>
<ul>
  <li>Room: <strong>{{ $data->room->name }}</strong></li>
  <li>Location <strong>{{ $data->location->name }}</strong></li>
  <li>Date/Time: <strong>{{ $data->date_time }}</strong></li>
</ul>
<hr>
<p>
  Note: Whenever you modify or change your reservation, that approved reserved slot will be automatically cancelled.
</p>
<br />
<br />