@if ($user['id'] == $data->user->id)
<p>
	You have CANCELLED your room reservation for {{ $data->date_time }} at the {{ $data->room->name }}
</p>
@else
<p>
	We have CANCELLED your reservation for {{ $data->date_time }} at the {{ $data->room->name }}
</p>
@endif
<p>
	Due to this reason: <br>
	{{ nl2br($user['reason']) }}
</p>
<br />
<br />
<p>
	Reservation Details:
</p>
<ul>
	<li>Room: <strong>{{ $data->room->name }}</strong></li>
	<li>Location <strong>{{ $data->location->name }}</strong></li>
	<li>Date/Time: <strong>{{ $data->date_time }}</strong></li>
</ul>
<hr>
<p>
	Note: Incase you haven't requested or accidentally made this cancellation of reservation. Please see our Meeting Room Coordinator as soon as possible to reinstate your reservation.
</p>
<br />
<br />