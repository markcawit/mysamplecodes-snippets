<p>
  Your request for reservation has been REJECTED.
</p>
<p>
	Due to this reason: <br>
	{{ nl2br($user['reason']) }}
</p>
<p>
	Your Reservation Details:
</p>
<ul>
	<li>Room: <strong>{{ $data->room->name }}</strong></li>
	<li>Location <strong>{{ $data->location->name }}</strong></li>
	<li>Date/Time: <strong>{{ $data->date_time }}</strong></li>
</ul>
<hr>
<p>
	Note: You may clarify your issue with regards to rejected request reservation. Please approach our Meeting Room Coordinator
</p>
<br />
<br />