<p>
  Welcome new Member.
</p>
<p>
  Here are the account details:
</p>
<ul>
  <li>Name: <strong>{{ $data['first_name'] . ' ' . $data['last_name'] }}</strong></li>
  <li>Email: <strong>{{ $data['email'] }}</strong></li>
  <li>Location: <strong>{{ $data['location'] }}</strong></li>
  <li>Membership: <strong>{{ $data['membership'] }}</strong></li>
</ul>
<hr>
<p>
  Your user ID: {{ $data['id'] }}
</p>
<p>
  Your login credentials:<br>
  Username: {{ $data['username'] }} <br>
  Password: default888 <i>(this is a temporary password, please change it as soon as you login.)</i>
</p>
<br />
<br />