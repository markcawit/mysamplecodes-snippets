@extends('layouts.default.master')

@section('title', 'Events | ')
@section('pageTitle', 'Events Calendar' . Helpers::breadcrumbs(['events'=>'Events'], 'events'))

@section('content')
<div class="container-fluid">
    
    @include('partials.alert.messages')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    {{-- <div class="form-group">
                        <a v-link="" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Event</a>
                    </div> --}}
                </div>
                <div class="col-sm-6">
                    <select v-model="location" @change="changeEventLocation()" class="form-control">
                        @foreach($locations as $loc)
                        <option {{ $locationId == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">{{$loc->location_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body no-max-height">
            <div id="event-calendar"></div>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var locId = {{ $locationId }}
        instanceCalendar('#event-calendar', 'events', '{{ route('fetch_events', $locationId) }}', locId)
    });
</script>
@endsection