@extends('layouts.default.master')

@section('title', $event->event_title.' | ')
@section('pageTitle', 'Events' . Helpers::breadcrumbs( 
                                                ['events'=>'Event Calendar', 
                                                'show_event'=>$event->event_title], 'show_event') )

@section('content')
<div class="container-fluid message-post">
    <div class="panel panel-default">
        {{-- List Heading --}}
        <div class="panel-heading">
            <h4>{{ $event->event_title }}</h4>
            <h5>at <strong>{{ $event->location->name }}</strong> | Date: <strong>{{ $event->formatedDate($event->event_date) }}</strong> | 
                Time: <strong>{{ $event->formatedTime( $event->start ) }} -  {{ $event->formatedTime( $event->end ) }}</strong><h5>
        </div>

        <div class="panel-body no-max-height">   
            {!! nl2br($event->description) !!}
        </div>

    </div>
</div>
@endsection