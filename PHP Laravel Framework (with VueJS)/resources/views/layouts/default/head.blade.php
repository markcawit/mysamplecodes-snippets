<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') {{ config('app.site.company') }}</title>

	@if (app()->environment() !== 'local')
	@endif
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,700,800" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Calendar Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/calendar.styles.css')}}">
    @yield('custom_style')
	
	@if (Auth::user())
    <script>
    	var userId = {{ Auth::user()->id }};
    	var userAccess = '{{ (Auth::user()->hasRole('administrator')) ? 'admin' : 'client' }}';
    	var canAddEvent = {{ (Auth::user()->hasRole('client')) ? 'false' : 'true' }};
    	var canReserveRoom = {{ Auth::user() ? 'true': 'false' }};
    </script>
    @endif

    @yield('head_script')
</head>
