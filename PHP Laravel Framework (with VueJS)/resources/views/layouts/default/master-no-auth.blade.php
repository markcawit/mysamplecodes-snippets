<!DOCTYPE html>
<html id="app" lang="">
@include('layouts.default.head')
<body>
	@include('layouts.default.nav-no-sidenav')
	<div class="main-container">
		@yield('content')
	</div>
	@include('layouts.default.vendor-js')
	@yield('script')
</body>
</html>
