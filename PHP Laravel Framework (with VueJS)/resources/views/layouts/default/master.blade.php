<!DOCTYPE html>
<html id="app" lang="">
@include('layouts.default.head')
<body>
	<router-view></router-view>

	<div class="side-nav-container">
		@include('layouts.default.side-nav')
	</div>
	<div class="content-container">
		@include('layouts.default.header-nav')
		<div class="main-container">
			@yield('content')
		</div>
	</div>
	@include('layouts.default.vendor-js')
	@yield('script')
</body>
</html>
