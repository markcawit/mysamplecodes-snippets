<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" id="sidebar-toggle-button" class="navbar-toggle collapsed" >
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<button type="button" class="navbar-toggle collapsed pull-right c-menu-button" data-toggle="collapse" data-target="#app-navbar-collapse">
				<i class="icon-grid"></i>
			</button>
			<div class="navbar-brand">
				@yield('pageTitle')
				{{-- <ul class="breadcrumb">
					<li><a href="{{ route('dashboard') }}">Dashboard</a></li>
					<li><a href="{{ route('permission_list') }}">Permissions</a></li>
				</ul> --}}
				{{-- @if (Auth::check())
				{!! Helpers::breadcrumbs(['permission_list'=>'Permissions']) !!}
				@endif --}}
			</div>
		</div>
		<div class="collapse navbar-collapse" id="app-navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				@include(config('laravel-menu.views.bootstrap-items'), array('items' => $UserNav->roots()))
			</ul>
		</div>
	</div>
</nav>
