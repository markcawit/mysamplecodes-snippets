<div class="side-nav">
	<div class="side-nav-brand">
		<img src="{{asset('images/logo.svg')}}" class="side-nav-logo">
	</div>
	{!! Menu::get('SideNav')->asUl(['class' => 'nav-link-container']) !!}
</div>
