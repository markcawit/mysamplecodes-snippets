		<!-- UX Vue Application -->
		<script src="{{ asset('js/main.js') }}"></script>
		<!-- Calendar Vendors JavaScript - with jQuery -->
		<script src="{{ asset('js/vendors-calendar.js') }}"></script>
		<!-- Vendors JavaScript -->
		<script src="{{ asset('js/vendors.js') }}"></script>
		<!-- Application JavaScript -->
		<script src="{{ asset('js/app.js') }}"></script>
