<div class="mask">
	<div class="centered-form-container">
		<div class="panel panel-default centered-panel">
			<div class="panel-heading">
				<span class="clearfix">
					<span class="pull-left">Edit Something</span>
					<span class="pull-right">
						<a href="#" class="close-button"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
					</span>
				</span>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" method="POST" action="">
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" name="username" value="">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" name="email" value="">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="text" class="form-control" name="password" value="">
					</div>
					<div class="form-group">
						<label>Repeat password</label>
						<input type="text" class="form-control" name="repeat-password" value="">
					</div>
					<br>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							<span class="clearfix">
								<span class="pull-left">Submit</span>
								<span class="pull-right"><i class="icon-check"></i></span> 
							</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
