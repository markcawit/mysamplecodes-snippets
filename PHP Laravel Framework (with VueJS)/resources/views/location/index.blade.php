@extends('layouts.default.master')

@section('title', 'Locations | ')
@section('pageTitle', 'Location Management' . Helpers::breadcrumbs(['location'=>'Location Management'], 'location'))

@section('content')
<div class="container-fluid">
    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
					<div class="form-group">
                    	<a v-link="{ path: '/addlocation' }" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Location</a>
					</div>
				</div>
                <div class="col-sm-6">
                    {{-- <form class="" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">Search</button>
                            </span>
                        </div>
                    </form> --}}
                </div>
            </div>
        </div>

        <div class="panel-body">
        	<table class="table table-hover">
        		<thead>
        			<tr>
                        <th class="action-controls"></th>
        				<th>Location Name</th>
        				<th>Address</th>
        			</tr>
        		</thead>
        		<tbody>
					@foreach ($locations as $location)
					<tr>
                        <td>
                            <a v-link="{ name: 'editlocation', params: { id : {{ $location->id }} }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon-note"></i></a>
                            <a href="{{ route('location-trash', $location->id) }}" data-toggle="tooltip" data-placement="top" title="Trash"><i class="icon-trash"></i></a>
                            <a href="{{ route('client_by_location', $location->id) }}" data-toggle="tooltip" data-placement="top" title="View Members"><i class="icon-people"></i></a>
                        </td>
						<td>{{ $location->location_name }}</td>
						<td>{{ $location->address }}</td>
					</tr>
					@endforeach
        		</tbody>
        	</table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection
