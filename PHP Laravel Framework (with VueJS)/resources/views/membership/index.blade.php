@extends('layouts.default.master')

@section('title', 'Membership | ')
@section('pageTitle', 'Membership Management' . Helpers::breadcrumbs(['membership'=>'List of Membership Plans'], 'membership'))

@section('content')
<div class="container-fluid">
    @include('partials.alert.messages')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
					<div class="form-group">
                    	<a v-link="{ path: '/addplan' }" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Plan</a>
					</div>
				</div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="action-controls"></th>
                        <th>Plan</th>
                        <th>Status</th>
                        <th>Fee</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $membership as $plan )
                        <tr>
                            <td>
                                @can('edit_membership')
                                    <a data-toggle="tooltip" data-placement="top" title="Edit Plan" v-link="{ name: 'editplan', params: { id: {{$plan->id}} }}">
                                        <i class="icon-note"></i>
                                    </a>&nbsp;
                                @endcan
                                @can('delete_membership')
                                    <a data-toggle="tooltip" data-placement="top" title="Delete Plan" href = "{{ route('membership-destroy', $plan->id) }}" onclick="return confirm('Are you sure you want to Delete this Plan?')">
                                        <i class="icon-trash"></i>
                                    </a>&nbsp;
                                @endcan
                                {{-- @can('view_clients')
                                <a href="{{ route('client_by_plan', $plan->id) }}" data-toggle="tooltip" data-placement="top" title="View Members"><i class="icon-people"></i></a>
                                @endcan --}}
                            </td>
                            <td>{{ $plan->name }}</td>
                            <td>{{ $plan->status }}</td>
                            <td class="price">{{ currency($plan->fee) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection
