
                <div class="form-group">
                    <label>Title 
                        <span class="text-red">*</span>{!! $errors->first('title', '<i class="text-red">:message</i>') !!}
                    </label>
                    <input type="text" class="form-control inputForm" id="title" name="title" placeholder="Message Title"
                            value="{{ isset($post) ? $post->title : old('title') }}">
                </div>

                <div class="form-group">
                    <label>Content 
                        <span class="text-red">*</span>{!! $errors->first('content', '<i class="text-red">:message</i>') !!}
                    </label>
                    <textarea class="form-control inputForm" id="content" name="content" rows="10" placeholder="Message Content">{{ isset($post) ? $post->content : old('content') }}</textarea>
                </div>

                <div class="form-group">
                    <label>Category<span class="text-red">*</span></label>
                    <select class="form-control" id="category_id" name="category_id">
                        @foreach( $categories as $category )
                        <option value="{{ $category->id }}" 
                                {{ (isset($post) && $post->category_id == $category->id) ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <br>

                <div class="form-group">
                    <a href="{{ route('message_board') }}" class="btn btn-primary">Back</a>

                    <button type="submit" class="btn btn-primary">
                        <span class="clearfix">
                            <span class="pull-left">{{ $submitButton }} <i class="icon-check"></i></span>
                        </span>
                    </button>

                </div>
            {!! Form::close() !!}


            {{-- <form>
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="{{ method }}" novalidate>

                <div class="form-group">
                    <label>Title*</label>
                    <input type="text" class="form-control inputForm" placeholder="Message Title" 
                                v-validate:title="['required']"
                                v-model="fields.title">
                </div>

                <div class="form-group">
                    <label>Content*</label>
                    <textarea class="form-control inputForm"  placeholder="Message Content"
                                v-validate:content="['required']"
                                v-model="fields.content"></textarea>
                </div>

                <div class="form-group">
                    <label>Date Post</label>
                    <input type="date" class="form-control inputForm"
                                v-model="fields.date_posted">
                </div>

                <div class="form-group">
                    <label>Pin Post</label>
                    <select class="form-control inputForm" 
                            v-model="fields.pinned">
                        <option v-for="pin in pinned" 
                            value="{{ pin }}"
                            :selected="pin == fields.pinned ? true : false">{{ pin }}</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" v-if="$validation.valid">
                        <span class="clearfix">
                            <span class="pull-left">Submit</span>
                            <span class="pull-right"><i class="icon-check"></i></span>
                        </span>
                    </button>
                    <button type="button" class="btn btn-primary btn-block" 
                                        v-if="!$validation.valid" 
                                        @click="invalidFormSubmit()"
                                        >
                        <span class="clearfix">
                            <span class="pull-left">Submit</span>
                            <span class="pull-right"><i class="icon-check"></i></span>
                        </span>
                    </button>
                </div>

            </form> --}}
