@extends('layouts.default.master')

@section('title', 'Clients | ')
@section('pageTitle', 'Message Board' . Helpers::breadcrumbs( 
                                                ['message_board'=>'List of Messages', 
                                                'message_create'=>'New Post'], 'message_create') )

@section('content')
<div class="container-fluid">

    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        {{-- List Heading --}}
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        Create New Post
                    </div>
                </div>
            </div>

        </div>

        <div class="panel-body no-max-height">   
            {!! Form::open(array('route' => 'message_store', 'class'=>'form-horizontal')) !!}
            @include('messages._form', ['submitButton'=>'Submit', 'errors', $errors])
        </div>

    </div>
</div>
@endsection
