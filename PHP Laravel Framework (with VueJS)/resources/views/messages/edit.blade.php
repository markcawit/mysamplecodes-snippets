@extends('layouts.default.master')

@section('title', 'Clients | ')
@section('pageTitle', 'Message Board' . Helpers::breadcrumbs( 
                                                ['message_board'=>'List of Messages', 
                                                'message_edit'=>'Edit Post'], 'message_edit') )

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        {{-- List Heading --}}
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        Edit Post
                    </div>
                </div>
            </div>

        </div>

        <div class="panel-body no-max-height">   
            {!! Form::open(array('route' => array('message_update', $post->id), 'class'=>'form-horizontal')) !!}
            @include('messages._form', ['submitButton'=>'Update'])
        </div>

    </div>
</div>
@endsection
