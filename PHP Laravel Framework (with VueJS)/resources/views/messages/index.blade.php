@extends('layouts.default.master')

@section('title', 'Clients | ')
@section('pageTitle', 'Message Board' . Helpers::breadcrumbs( ['message_board'=>'List of Messages'], 'message_board') )

@section('content')
<div class="container-fluid message-board-list">
    <div class="panel panel-default">

        @include('partials.alert.messages')
        {{-- List Heading --}}
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">

                        <a href="{{ route('message_create') }}" class="btn btn-primary">
                            <i class="icon-plus"></i> New Message
                        </a>
                    </div>
                </div>
                {{-- <div class="col-sm-6">

                    <form method="get" action="{{ route('clients')}}">
                        <div class="input-group search-group">
                            <input type="text" class="form-control" name="search" placeholder="Search for..." value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>

                </div> --}}
            </div>

        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        {{-- <th class="action-controls"></th> --}}
                        <th><a href="#">Title</a></th>
                        <th width="15%"><a href="#">Date Posted</a></th>
                        <th width="15%"><a href="#">Last Updated</a></th>
                        <th width="15%"><a href="#">Posted By</a></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                        {{-- <td style="text-align:center">
                            <a data-toggle="tooltip" data-placement="top" title="View Post" href="{{ route('message_show', $post->id) }}"><i class="icon-eye"></i></a>&nbsp;
                            </td> --}}
                            <td>@if ($post->pinned === 'yes') <i class="icon-pin"></i> @endif
                                <a href="{{ route('message_show', $post->id) }}">{{ $post->title }}</a>
                                
                                <span class="post-actions">
                                {{-- @can('update_message') --}}
                                    @if ( Auth::user()->hasRole('administrator') || $post->user_id == Auth::user()->id )
                                    <a data-toggle="tooltip" data-placement="top" title="Edit Post" 
                                        href="{{ route('message_edit', $post->id) }}">
                                        <i class="icon-note"></i>
                                    </a>
                                    @endif
                                {{-- @endcan --}}
                                {{-- @can('delete_message') --}}
                                    @if ( Auth::user()->hasRole('administrator') || $post->user_id == Auth::user()->id )
                                    <a data-toggle="tooltip" data-placement="top" title="Delete Post" href = "{{ route('message_destroy', $post->id) }}" onclick="return confirm('Are you sure you want to Delete this message?')">
                                        <i class="icon-trash"></i>
                                    </a>
                                    @endif
                                {{-- @endcan --}}
                                </span>
                                </td>
                            <td>{{ $post->formatDatePosted() }}</td>
                            <td>{{ $post->formatLastUpdate() }}</td>
                            <td>{{ $post->user->username }}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @if($posts->count() == 0)
                <div style="text-align:center; color:#333; font-size:18px">No records to show</div>
                @if($search)
                <div style="text-align:center; color:#333; font-size:18px">for {{ $search }}</div>
                @endif
                @endif
        </div>

    </div>
</div>
{!! csrf_field() !!}
@endsection
