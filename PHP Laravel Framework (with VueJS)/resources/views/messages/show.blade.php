@extends('layouts.default.master')

@section('title', $post->title.' | ')
@section('pageTitle', 'Message Board' . Helpers::breadcrumbs( 
                                                ['message_board'=>'List of Messages', 
                                                'message_create'=>$post->title], 'message_create') )

@section('content')
<div class="container-fluid message-post">

    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        {{-- List Heading --}}
        <div class="panel-heading">
            <h4>{{ $post->title }} {!! $post->isOwned() !!} {!! $post->canPin() !!}</h4>
            <h5>Category: <strong>{{ $post->PostCategory->name }}</strong> | 
            	Posted by <strong>{{ $post->user->full_name }}</strong><h5>
        </div>

        <div class="panel-body no-max-height">   
            {!! nl2br(e($post->content)) !!}
        </div>

    </div>
</div>
@endsection