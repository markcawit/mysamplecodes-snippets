        @if (Session::has('denied_message'))
            <div class="alert alert-danger">
                {!! Session::get('denied_message') !!}
                {{--*/ Session::remove('denied_message')  /*--}}
            </div>
        @endif

        @if (Session::has('success_message'))
            <div class="alert alert-success">
                {!! Session::remove('success_message') !!}
            </div>
        @endif