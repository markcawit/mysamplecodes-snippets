        @if (Session::has('denied_message'))
            <div class="alert alert-danger">
                {{ Session::get('denied_message') }}
            </div>
        @endif