@extends('layouts.default.master')

@section('title', 'Permissions | ')
@section('pageTitle', 'Permission Settings' . Helpers::breadcrumbs(['permission_list'=>'Permissions'], 'permission_list') )

@section('content')
<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						@can("add_permission")<a title="Add a New Permission" class="btn btn-primary" href="{{ route('permission_add')}}"><i class="icon-plus"></i> Add Permission</a>@endcan
					</div>
				</div>
				<div class="col-sm-6">
					<form method="get" action="{{ route('permission_list')}}">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="search-form form-control" placeholder="Search for..." name="search" value="{{ $search }}">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="submit">Search</button>
								</span>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-body">
			@if(Session::has('message'))
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ Session::get('message') }}
			</div>
			@elseif(count($errors) > 0)
			@foreach ($errors->all() as $error)
			<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<li style="list-style:none">{{ $error }}</li>
			</div>
			@endforeach
			@endif
			<table class="table">
				<thead>
					<tr>
						<th class="action-controls"></th>
						<th><a href="{{ route('permission_list').'?page='.$page_number.'&search='.$search.'&order_by=name&sort='.$sort }}">Name</a></th>
						<th><a href="{{ route('permission_list').'?page='.$page_number.'&search='.$search.'&order_by=label&sort='.$sort }}">Description</a></th>

					</tr>
				</thead>
				<tbody>
					@foreach($permission as $row)
					<tr>
						<td>
							@can("update_permission")
							<a href="{{ route('permission_edit', $row->id)}}" data-toggle="tooltip" data-placement="top" title="Edit user">
								<i class="icon-note"></i>
							</a>
							@endcan
							@can("delete_permission")
							<a href="{{ route('permission_delete', $row->id) }}" onclick="return confirm('Are you sure you want to Delete this permission?')" data-toggle="tooltip" data-placement="top" title="Delete user">
								<i class="icon-trash"></i>
							</a>
							@endcan
						</td>
						<td>{{ $row->name }}</td>
						<td>{{ $row->label }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@if($permission->count() == 0)
			<div style="text-align:center; color:#999; font-size:18px">No records to show</div>
			@if($search)
			<div style="text-align:center; color:#999; font-size:18px">for {{ $search }}</div>
			@endif
			@endif
		</div>
		{!! str_replace('/?', '?', $permission->render()) !!}
	</div>
</div>
</div>
@endsection
