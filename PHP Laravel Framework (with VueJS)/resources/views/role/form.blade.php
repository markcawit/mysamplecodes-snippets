@extends('layouts.default.master')

@section('title', 'Roles | ')
@section('pageTitle', 'User Role Settings')

@section('content')
<div class="container-fluid">
	@include('partials.alert.messages')

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="text-muted bootstrap-admin-box-title">{{ $header }}</div>
		</div>
		<form class="form-horizontal" action="{{ $action }}" method="post">
		<div class="panel-body">
			@if(Session::has('message'))
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ Session::get('message') }}
			</div>
			@elseif(count($errors) > 0)
			@foreach ($errors->all() as $error)
			<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<li style="list-style:none">{{ $error }}</li>
			</div>
			@endforeach
			@endif
				<div class="form-group">
					<input class="form-control" name="name" placeholder="Role Name" value="{{ (isset($role->name)?$role->name : old('name')) }}">
				</div>
				<div class="form-group">
					<input class="form-control" name="label" placeholder="Role Description" value="{{ (isset($role->label)?$role->label : old('label')) }}">
				</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th class="action-controls" style="width:10%"></th>
							<th>Roles</th>
						</tr>
					</thead>
					<tbody>
						@foreach($permissions as $permission)
						<tr>
							<td style="text-align:center;">
								<input type="checkbox" name="permission[]" id="permission_{{$permission->id}}" value="{{$permission->id}}" class="permission" {{ (isset($permission_role)?((in_array($permission->id, $permission_role) )? 'checked="checked"' : ''):'')}}>
							</td>
							<td>
								<label for="permission_{{$permission->id}}" >{{$permission->label}}</label>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{!! csrf_field() !!}

		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary" ><i class="icon-note"></i> &nbsp;{{ $header }}</button>
		</div>
		</form>
	</div>
</div>
@endsection
