@extends('layouts.default.master')

@section('title', 'Roles | ')
@section('pageTitle', 'User Role Settings' . Helpers::breadcrumbs(['users'=>'User Role Settings'], 'role'))

@section('content')
<div class="container-fluid">
	@include('partials.alert.messages')
	
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
					<div class="form-group">
                    	<a href="{{ route('role_add')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Role</a>
					</div>
				</div>
                <div class="col-sm-6">
                    <form class="" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">Search</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel-body">
			@if(Session::has('message'))
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"><span class="icon-close"></span></span>
					</button>
					{{ Session::get('message') }}
				</div>
			@elseif(count($errors) > 0)
				@foreach ($errors->all() as $error)
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><span class="icon-close"></span></span>
						</button>
						<li style="list-style:none">{{ $error }}</li>
					</div>
				@endforeach
			@endif
        	<table class="table table-hover">
				<thead>
				<tr>
					<th action-controls></th>
					<th><a href="{{ route('role_list').'?page='.$page_number.'&search='.$search.'&order_by=name&sort='.$sort }}">Name</a></th>
					<th><a href="{{ route('role_list').'?page='.$page_number.'&search='.$search.'&order_by=label&sort='.$sort }}">Role</a></th>
				</tr>
				</thead>
				<tbody>
				@foreach($role as $row)
					<tr>
						<td>
							@can("update_role")
							<a href="{{ route('role_edit', $row->id)}}" data-toggle="tooltip" data-placement="top" title="Edit user">
								<i class="icon-note"></i>
							</a>
							@endcan
							@can("delete_role")
							<a href="{{ route('role_delete', $row->id) }}" onclick="return confirm('Are you sure you want to Delete this role?')" data-toggle="tooltip" data-placement="top" title="Delete user">
								<i class="icon-trash"></i>
							</a>
							@endcan
						</td>
						<td>{{ $row->name }}</td>
						<td>{{ $row->label }}</td>

					</tr>
				@endforeach
				</tbody>
				@if($role->count() == 0)
					<div style="text-align:center; color:#333; font-size:18px">No records to show</div>
					@if($search)
					<div style="text-align:center; color:#333; font-size:18px">for {{ $search }}</div>
					@endif
				@endif
        	</table>
			{!! str_replace('/?', '?', $role->render()) !!}
        </div>

    </div>
</div>
@endsection
