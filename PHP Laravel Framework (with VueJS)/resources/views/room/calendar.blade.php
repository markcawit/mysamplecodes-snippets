@extends('layouts.default.master')

@section('title', 'Room Reservations | ')
@section('pageTitle', 'Room Reservations Calendar' . Helpers::breadcrumbs(['room_reservations'=>'RoomReservations'], 'room_reservations'))

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    {{-- <div class="form-group">
                        <a v-link="" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Add"><i class="icon-plus"></i> Add Event</a>
                    </div> --}}
                </div>
                <div class="col-sm-6">
                    <select v-model="location" @change="changeEventLocation()" class="form-control">
                        @foreach($locations as $loc)
                        <option {{ $locationId == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">{{$loc->location_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body no-max-height">
            <div id="rooms_reservation-calendar"></div>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var locId = {{ $locationId }}
        instanceCalendar('#rooms_reservation', 'room_reservaations', '{{ route('fetch_events', $locationId) }}', locId)
    });
</script>
@endsection