@extends('layouts.default.master')

@section('title', 'Meeting Room Calendar | ')
@section('pageTitle', 'Meeting Room Calendar' . Helpers::breadcrumbs(['rooms'=>'Room Reservations'], 'rooms'))

@section('custom_style')
    <style type="text/css">
        .form-control.inputForm.invalid.pristine {
            border: 1px solid red;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <select v-model="room" @change="changeReservationRoom()" class="form-control">
                        <option value="">View All Room Reservations in {{ $location->name }}</option>
                        @foreach($location->rooms as $room)
                        <option {{ $roomId == $room->id ? 'selected' : '' }} value="{{$room->id}}">
                            View {{$room->name}} Reservations Only</option>
                        @endforeach
                    </select>
                    <script>
                        var rooms = {!! json_encode($location->rooms) !!};
                    </script>
                </div>
                <div class="col-sm-6">
                    <select v-model="location" @change="changeReservationLocation()" class="form-control">
                        @foreach($locations as $loc)
                        <option {{ $location->id == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">{{$loc->location_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body no-max-height">
            <div id="room-calendar"></div>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection

@section('script')
<script>
    $(document).ready(function() {  
        var locId = {{ $location->id }}
        instanceCalendar('#room-calendar', 'rooms', '{{ route($fetchBy, $roomParams) }}', locId, {{ $roomId }})
    });
</script>
@endsection