@extends('layouts.default.master')

@section('title', 'Room Management | ')
@section('pageTitle', 'Room Management' . Helpers::breadcrumbs(['manage_room'=>'List of Rooms for '. $location->name], 'manage_room'))

@section('content')
<div class="container-fluid">
    @include('partials.alert.messages')
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
					<div class="form-group">
                    	<a v-link="{ name: 'addroom', params: { location: {{ $location->id }} } }" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Add"><i class="icon-plus"></i> Add Room</a>
					</div>
				</div>

                <div class="col-sm-6">
                    <select v-model="location" @change="changeRoomLocation()" class="form-control">
                        @foreach($locations as $loc)
                        <option {{ $location->id == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">
                        	{{$loc->name}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body">
        	<table class="table table-hover">
        		<thead>
        			<tr>
                        <th class="action-controls"></th>
        				<th>Room Name</th>
        				<th></th>
        			</tr>
        		</thead>
        		<tbody>
        		@if($location->rooms->count())
					@foreach ($location->rooms as $room)
					<tr>
                        <td>
                            <a v-link="{ name: 'editroom', params: { id : {{ $room->id }} }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="icon-note"></i></a>
                            <a href="{{ route('delete_room', $room->id) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="icon-trash"></i></a>
                            <a href="{{ route('rooms', ['location'=>$location->id, 'room'=>$room->id]) }}" data-toggle="tooltip" data-placement="top" title="View Reservations"><i class="icon-people"></i></a>
                        </td>
						<td>{{ $room->name }}</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td></td>
						<td colspan="2">No Rooms Added Yet.</td>
					</tr>
				@endif
        		</tbody>
        	</table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection