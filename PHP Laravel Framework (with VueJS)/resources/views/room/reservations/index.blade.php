@extends('layouts.default.master')

@section('title', 'Meeting Rooms Calendar | ')
@section('pageTitle', 'Meeting Rooms Calendar' . Helpers::breadcrumbs([
                                                    'rooms'=>['Room Reservations', $roomParams],
                                                    'user_reservations'=>'My Reservations'], 'user_reservations'))

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                List of Reservations
                </div>
                <div class="col-sm-6">
                &nbsp;
                </div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th><a href="?{{ $past }}order_by=room&sort={{ $sort }}">Room Name</a></th>
                        <th><a href="?{{ $past }}order_by=start_date&sort={{ $sort }}">Date & Time</a></th>
                        <th><a href="?{{ $past }}order_by=user&sort={{ $sort }}">Username</a></th>
                        <th><a href="?{{ $past }}order_by=location&sort={{ $sort }}">Location</a></th>
                        <th><a href="?{{ $past }}order_by=status&sort={{ $sort }}">Status</a></th>
                    </tr>
                </thead>
                <tbody>
                @if($reservations->count())
                    @foreach ($reservations as $reservation)
                    <tr>
                        <td>
                            <a class="text-underline" href="{{ route('reservation_item', $reservation->id) }}">
                                {{ $reservation->room->name }}
                            </a></td>
                        <td>{!! $reservation->date_time !!} 
                            @if ($reservation->status == 'active')
                            {!! $reservation->stat !!}
                            @endif
                            </td>
                        <td>
                            <a class="text-underline" href="{{ route('this_user_reservations', $reservation->user->id) }}">
                            {{ $reservation->user->username }}
                            </a></td>
                        <td>{{ $reservation->location->name }}</td>
                        <td>{{ $reservation->status }}</td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td></td>
                        <td colspan="2">You Don't have Reservations Yet.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection