@extends('layouts.default.master')

@section('title', 'Rooms Reservations | ')
@section('pageTitle', 'Rooms Reservations' . Helpers::breadcrumbs([
                                                    'reservation_request'=>['List of Pending Reservation Request', $roomParams]], 'reservation_request'))

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                Pending Reservations
                </div>
                <div class="col-sm-6">
                &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <select v-model="room" @change="refreshPendingRequests()" class="form-control">
                        <option value="">View All Room Request in {{ $location->name }}</option>
                        @foreach($location->rooms as $room)
                        <option {{ $roomId == $room->id ? 'selected' : '' }} value="{{$room->id}}">
                            View {{$room->name}} Request Only</option>
                        @endforeach
                    </select>
                    <script>
                        var rooms = {!! json_encode($location->rooms) !!};
                    </script>
                </div>
                <div class="col-sm-6">
                    <select v-model="location" @change="refreshPendingRequestsLocation()" class="form-control">
                        @foreach($locations as $loc)
                        <option {{ $location->id == $loc->id ? 'selected' : '' }} value="{{$loc->id}}">{{$loc->location_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        {{-- <th class="action-controls"></th> --}}
                        <th>Room Name</th>
                        <th>Date & Time</th>
                        <th>Requested By</th>
                        <th>Location</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                @if($pendings->count())
                    @foreach ($pendings as $reservation)
                    <tr>
                        {{-- <td>
                            &nbsp;
                        </td> --}}
                        <td>
                            <a class="text-underline" href="{{ route('reservation_item', $reservation->id) }}">
                                {{ $reservation->room->name }}
                            </a></td>
                        <td>{!! $reservation->date_time_a !!}</td>
                        <td>
                            <a class="text-underline" href="{{ route('this_user_reservations', $reservation->user->id) }}">
                            {{ $reservation->user->username }}
                            </a></td>
                        <td>{{ $reservation->location->name }}</td>
                        <td>{!! $reservation->stat !!}</td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td></td>
                        <td colspan="2">You Don't have Reservations Yet.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection