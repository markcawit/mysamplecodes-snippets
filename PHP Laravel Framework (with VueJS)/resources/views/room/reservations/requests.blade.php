@extends('layouts.default.master')

@section('title', 'Rooms Reservations | ')
@section('pageTitle', 'Rooms Reservations' . Helpers::breadcrumbs([
                                                    'reservation_request'=>['List of Pending Reservation Request', $roomParams],
                                                    'reservation_item'=>'My Reservations'], 'reservation_item'))

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                Pending Reservations
                </div>
                <div class="col-sm-6">
                &nbsp;
                </div>
            </div>
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        {{-- <th class="action-controls"></th> --}}
                        <th>Room Name</th>
                        <th>Date & Time</th>
                        <th>Location</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                @if($pendings->count())
                    @foreach ($pendings as $reservation)
                    <tr>
                        {{-- <td>
                            &nbsp;
                        </td> --}}
                        <td>
                            <a class="text-underline" href="{{ route('reservation_item', $reservation->id) }}">
                                {{ $reservation->room->name }}
                            </a></td>
                        <td>{!! $reservation->date_time_a !!}</td>
                        <td>
                            <a class="text-underline" href="{{ route('this_user_reservations', $reservation->user->id) }}">
                            {{ $reservation->user->username }}
                            </a></td>
                        <td>{{ $reservation->location->name }}</td>
                        <td>{!! $reservation->stat !!}</td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td></td>
                        <td colspan="2">You Don't have Reservations Yet.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection