@extends('layouts.default.master')

@section('title', 'Rooms Reservations | ')
@section('pageTitle', 'Rooms Reservations' . Helpers::breadcrumbs([
                                                    'reservation_request'=>['List of Reservation Request', $roomParams],
                                                    'reservation_item'=>'Reservation Request'], 'reservation_item'))

@section('content')
<div class="container-fluid">
    <div class="panel panel-default">
        @include('partials.denied')

        <div class="panel-heading">
            <h4>{{ $reservation->request_title }}</h4>
            <h5>
                {{ $reservation->date_time }}
                @if ($reservation->approved)
                <span class="text-green text-sm">(RESERVED)</span>
                @endif
            <h5>
        </div>

        <div class="panel-body">
            <p>Status: {!! ($reservation->status_approved == 'Approved') 
                            ? '<strong class="text-green">'.$reservation->status_approved.'</strong>'
                            : '<strong class="text-red">'.$reservation->status_approved.'</strong>' !!}</p>
            <p>Requested by: <strong>{{ $reservation->user->full_name }}</strong> ({{$reservation->user->username}})</p>
            <p>On <strong>{{ $reservation->date_time }}</strong></p>
            <p>Date Requested: {{ $reservation->created_date }}</p>
            <p>{{ $reservation->note }}</p>
    
            @if($reservation->status == 'rejected' || $reservation->status == 'cancelled')
            <br>
            <br>
            <p>{{ ucwords($reservation->status) }} by <strong>{{ $reservation->action_by }}</strong></p>
            <p>Reason: <i>{{ nl2br($reservation->reason) }}</i></p>
            @endif

            <p>{{ $reservation->note }}</p>

            @if ( Auth::user()->hasRole('administrator'))
            <p>
                <ul class="inline-action">
                @if (!$reservation->approved)
                    <li>
                        <a href="{{ route('reservation_confirm', $reservation->id) }}" class="text-green">
                            <i class="icon-like"></i> Approve</a>
                        </li>
                    @if ($reservation->status !== 'cancelled' && $reservation->status !== 'rejected')
                    <li>
                        <span> | </span>
                    </li>
                    <li>
                        <a class="text-red"
                            v-link="{ 
                                    name: 'reject_reservation', 
                                    params: { 
                                        id: {{ $reservation->id }}
                                        } 
                                    }">
                            <i class="icon-dislike"></i> Reject</a>
                        </li>
                    @endif
                @else
                    <li>
                        <a class="text-red"
                            v-link="{ 
                                    name: 'cancel_reservation', 
                                    params: { 
                                        id: {{ $reservation->id }}
                                        } 
                                    }">
                            <i class="icon-close"></i> Cancel Reservation</a>                        
                        </li>
                @endif
                </ul>
            </p>
            @endif
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection