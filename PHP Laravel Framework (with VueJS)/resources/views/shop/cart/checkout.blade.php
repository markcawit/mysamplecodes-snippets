@extends('layouts.default.master')

@section('title', 'Checkout | ')
@section('pageTitle', 'Checkout' . Helpers::breadcrumbs(['cart-checkout'=>'Checkout'], 'cart-checkout'))

@section('content')
	<div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
            	{{ $cart->user->full_name }}
            </div>

            <div class="panel-body no-overflow no-max-height">
				<div class="col-sm-6 col-xs-12 checkout-left">
                @if ($cart->status == 'paid')
                    <p>This Invoice was already paid in Cash/OTC last {{ $cart->formatted_payment_date }}.</p>
                    <p>See details below:</p>

                    <div class="col-sm-10 col-xs-12">
                    @if ( $cart->gateway == 'otc')
                        {{--*/ $payment = json_decode($cart->ref_payment) /*--}}
                        <ul class="item-list">
                            <li class="large-margin-top">
                                <strong>
                                    OR Number: 
                                    <span class="pull-right">#{{ $payment->or_number }}</span>
                                    </strong></li>
                            <li>
                                <strong>
                                    Date Paid: 
                                    <span class="pull-right">
                                        {{ Carbon\Carbon::parse($payment->or_date)->format('m/d/Y h:i:sA') }}</span>
                                    </strong></li>
                            <li class="line-separator"></li>
                            <li>
                                <strong>
                                    Amount Due: 
                                    <span class="pull-right">{{ currency($payment->amount) }}</span>
                                </strong></li>
                            <li>
                                Cash Tendered: <span class="pull-right">{{ currency($payment->cash) }}</span></li>
                            <li>
                                Change: <span class="pull-right">{{ currency($payment->change) }}</span></li>
                            
                            @if ($payment->discount)
                            <li class="large-margin-top">
                                Discount: <span class="pull-right">{{ currency($payment->discount) }}</span></li>
                            @endif
                        </ul>
                    @else

                    @endif
                    </div>
                @else
                    @if (Auth::user()->hasRole('administrator'))
                        <p>In order to flag this Member's Cart as paid, process their payment first from Cashier, then get the Official Receipt and encode the OR Number, OR Date, Cash Tendered, Amout Paid, Change and Discounted Amount if there's any by clicking the "OTC Cart Payment" button below.</p>
                        <p>&nbsp;</p>
                        <a class="btn btn-primary btn-block center"
                            v-link="{
                                    name: 'process-payment',
                                    params: {
                                        id: {{ $cart->id }}
                                    }
                                }">
                            <i class="icon-wallet pull-left"></i> OTC Cart Payment
                        </a>
                    @else
                        <p>If you want to pay using your credit card/paypal balance you may choose to "Pay with PayPal" below. However, if you want to pay OTC (Over-The-Counter) you may approach our gorgeous assistant to receive your payment wether it be credit card or cash.</p>
                        <p>&nbsp;</p>
    					<a href="{{ url('paypal/checkout') }}/{{ $cart->id }}" class="btn btn-primary btn-block center">
                            <i class="icon-credit-card pull-left"></i> Pay with PayPal*
                        </a>
                        <p>&nbsp;</p>
                        <p>*Paying with PayPal will require you to pay additioanl 5% for the transaction fee.</p>
                    @endif
                @endif
				</div>
				<div class="col-sm-6 col-xs-12">
					<h5>
						Cart Item(s) <span class="pull-right">#{{ $cart->invoice_number }}</span>
					</h5>
					<hr>
					<ul class="item-list">
                        @if($items->count())
                            <li class="large-margin-top">
                            	<strong>Snacks</strong>
                            	<span class="pull-right">Subtotal: 
                            		<strong>{{ currency($total_snacks) }}</strong>
                            	</span>
                            </li>
                            @foreach($items as $item)
                            <li class="line-separator">
                            	{{ $item->product->product_name }} <span class="pull-right">{{ currency($item->total) }}</span>
                            	<br>
                            	{{ $item->quantity }} x {{ currency($item->price) }}
                            </li>
                            @endforeach
                        @endif

                        <li class="extra-large-margin-top">
                        	<strong>Membership Fee &amp; Others</strong>
                        	<span class="pull-right">
                        		Subtotal: 
                        		<strong>{{ currency($total_recurring) }}</strong>
                        	</span>
                        </li>

                        @foreach( $membership as $plan)
                        <li class="line-separator">
                        	{{ $plan->Membership->name }}*
                        	<span class="pull-right">{{ currency($plan->Membership->fee) }}</span>
                        </li>
                        @endforeach

                        @foreach($amenities as $amenity)
                        <li class="line-separator">
                        	{{ $amenity->Amenity->name }}
                        	<span class="pull-right">{{ currency($amenity->total) }}</span>
                        	<br>
                        	{{ $amenity->created_at->format('m/d/Y') }} &mdash;
                        	{{ $amenity->quantity }} x {{ currency($amenity->price) }}
                        </li>
                        @endforeach

                        @foreach($addplans as $adds)
                        <li class="line-separator">
                        	{{ $adds->Membership->name }}
                        	<span class="pull-right">{{ currency($adds->total) }}</span>
                        	<br>
                        	{{ $adds->created_at->format('m/d/Y') }} &mdash;
                        	{{ $adds->quantity }} x {{ currency($adds->Membership->fee) }}
                        </li>
                        @endforeach
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>
							Discount/Coupon:
							<span class="pull-right">
								{{ currency($cart->discount) }}
							</span>
							<br>
							{{ $cart->coupon }}
						</li>
						<li>
							<strong>
								Total Amount Due: 
								<span class="pull-right">
									{{ currency($cart->amount_due) }}
								</span>
							</strong>
						</li>
					</ul>
				</div>
            </div>
        </div>
    </div>
{!! csrf_field() !!}
@endsection
