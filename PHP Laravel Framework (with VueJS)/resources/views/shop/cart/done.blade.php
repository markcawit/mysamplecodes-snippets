@extends('layouts.default.master')

@section('title', 'Payment Completed | ')
@section('pageTitle', 'Payment Completed')

@section('content')
	<div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
            	Payment Details (Transaction ID: {{ $cart->txn_id }})
            </div>

            <div class="panel-body no-overflow no-max-height">
                <div class="col-sm-8 col-xs-12">
                    <p>Hi {{ $cart->user->first_name }},</p>
                    
                    <br>

                    <p>Thank you for your payment amounting ${{ $txn->amount->total }} {{ $txn->amount->currency }} to {{ $txn->payee->email}} from your Cart/Invoice Number {{ $txn->invoice_number }}. </p>
                    
                    <br>

                    <p>Your Membership Subscription/Cart has been reset.</p>

                    <br>

                    <p>See below the details of your payment: <br>
                    {{ $paymentDate }}| Transaction ID: {{ $sale->id }}</p>

                    <br>
                    <br>
                    <p>{{ $txn->description }}</p>
                    <br>
                    <ul class="item-list large-margin-top">
                        @foreach( $itemList->items as $items)
                        <li>
                            Description: {{ $items->name }} <br>
                            Unit price: ${{ $items->price }} {{ $items->currency }} <br>
                            Qty: {{ $items->quantity }} <br>
                            Amount: ${{ $items->price }} {{ $items->currency }}
                        </li>
                        @endforeach

                        <li class="line-separator">&nbsp;</li>
                        <li>&nbsp;</li>
                        <li class="large-margin-top">
                            Subtotal: ${{ $sale->amount->total }} {{ $sale->amount->currency }}
                        </li>

                        <li class="large-margin-top">
                            Total: ${{ $sale->amount->details->subtotal }} {{ $sale->amount->currency }}
                        </li>

                        <li class="line-separator">&nbsp;</li>
                        <li>&nbsp;</li>
                        <li class="large-margin-top">
                            Payment: ${{ $txn->amount->total }} {{ $txn->amount->currency }} <br>
                            Payment sent to: {{ $txn->payee->email}} <br>
                            <br>
                            Invoice ID: {{ $txn->invoice_number }}
                        </li>
                    </ul>
                </div>

                {{-- <div class="col-sm-12 col-xs-12">
                {{ dump($paymentRef) }}
                </div> --}}

            </div>
        </div>
    </div>
{!! csrf_field() !!}
@endsection
