@extends('layouts.default.master')

@section('title', 'Shopping Cart | ')
@section('pageTitle', 'Shopping Cart' . Helpers::breadcrumbs(['cart'=>'Shopping Cart'], 'cart'))

@section('content')
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
            @if(isset($cart_name))
                {{ $cart_name }}
            @endif
                Cart
            </div>

            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="width: 50px;"></th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th class="price center">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($items->count())
                        @foreach($items as $item)
                        <tr>
                            <td>
                            {{-- @can('delete_membership') --}}
                                @if ($cart->status == 'unpaid')
                                <a data-toggle="tooltip" data-placement="top" title="Remove Item" href = "{{ route('cart-remove', $item->id) }}" onclick="return confirm('Are you sure you want to Remove this item from your cart?')">
                                    <i class="icon-trash"></i>
                                </a>&nbsp;
                                @endif
                            {{-- @endcan --}}
                            </td>
                            <td>
                            {{ $item->product->product_name }}  @ {{ currency($item->price) }}
                            <input type="hidden"  value="{{ $item->price }}" 
                                        v-model="item_price[{{$item->id}}]"
                                        >
                            <input type="hidden" value="${{ $item->total }}" 
                                        v-model="total_price[{{$item->id}}]"
                                        >
                            </td>
                            <td>
                                @if ($cart->status == 'unpaid')
                                <input type="number" size="4" maxlength="4" style="width: 50px; text-align: center;" 
                                     value="{{ $item->quantity }}"
                                    v-model="quantity[{{$item->id}}]"
                                    @change="handleCartItemUpdate({{$item->id}})">
                                @else {{ $item->total }} @endif
                                </td>
                            <td class="price"
                                    v-text="total_price[{{$item->id}}]"></td>
                        </tr>
                        @endforeach
                    @else 
                        <tr>
                            <td colspan="4">Snack shop cart empty</td>
                        </tr>
                    @endif
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>

                        {{-- Account Membership and Amenities Added --}}
                        <tr class="divider">
                            <td colspan="4">
                                <strong>Membership Fee, Amenities &amp; Extra Access</strong>
                                <input type="hidden" value="{{ $total_recurring }}" 
                                        v-model="total_recurring">
                            </td>
                        </tr>
                        @foreach( $membership as $plan)
                        <tr>
                            <td colspan="2">
                                {{ $plan->Membership->name }}  @ {{ currency($plan->Membership->fee) }}</td>
                            <td></td>
                            <td class="price">{{ currency($plan->Membership->fee) }}</td>
                        </tr>
                        @endforeach

                        @foreach($amenities as $amenity)
                        <tr>
                            <td colspan="2">
                                {{ $amenity->Amenity->name }}  @ {{ currency($amenity->price) }}</td>
                            <td>{{ $amenity->created_at->format('m/d/Y') }}</td>
                            <td class="price">{{ currency($amenity->total) }}</td>
                        </tr>
                        @endforeach

                        @foreach($addplans as $adds)
                        <tr>
                            <td colspan="2">
                                {{ $adds->Membership->name }}  @ {{ currency($adds->Membership->fee) }}</td>
                            <td>{{ $adds->created_at->format('m/d/Y') }}</td>
                            <td class="price">{{ currency($adds->total) }}</td>
                        </tr>
                        @endforeach

                        <tr class="divider">
                            <td colspan="4"></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td></td>
                            <td class="price">
                                <input type="hidden" value="{{ $discount }}" 
                                        v-model="discount">
                                Discount: <span class="amount" v-html="discount_view"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td></td>
                            <td class="price">
                                <input type="hidden" v-model="total_amount_due" value="{{ currency($total) }}">
                                Total Amount Due: <span class="amount" v-html="total_amount_due"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td align="center">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>

            </div>
            <div class="panel-footer clearfix">
            @if ($cart->status == 'unpaid')
                <a href="{{ ($cart->user_id == Auth::user()->id) 
                                ? route('cart-checkout') 
                                : route('cart-checkout', $cart->id) }}" 
                                class="btn btn-primary pull-right">Checkout&nbsp;<i class="icon-bag"></i></a>
                @if($items->count())
                <a @click="updateCart" class="btn btn-primary pull-right margin-right">Update Cart</a>&nbsp;
                @endif
            @endif
            </div>
        </div>
    </div>
{!! csrf_field() !!}
@endsection
