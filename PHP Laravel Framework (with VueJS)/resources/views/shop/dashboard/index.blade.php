@extends('layouts.default.master')

@section('title', 'Snack Shop Dashboard | ')
@section('pageTitle', 'Snack Shop Dashboard' . 
                Helpers::breadcrumbs(['shop-dashboard'=>'Snack Shop'], 'shop-dashboard'))

@section('content')
<div class="container-fluid">
    @include('partials.alert.messages')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <a v-link="{ path: '/addproduct' }" class="btn btn-primary">
                            <i class="icon-plus"></i> Add Product
                        </a>
                        <a v-link="{ name: 'categories' }" class="btn btn-primary">
                            <i class="icon-options-vertical"></i> Categories
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">

                    {{-- <form method="get" action="{{ route('shop-dashboard')}}">
                        <div class="input-group search-group">
                            <input type="text" class="form-control" name="search" placeholder="Search for..." value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form> --}}

                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="action-controls" style="width:120px">&nbsp;</th>
                        <th>Product Name</th>
                        <th>SKU</th>
                        <th>Category</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td align="center">
                            <a v-link="{ name: 'editproduct', params: { id : {{$product->id}} } }" data-toggle="tooltip" data-placement="left" title="Edit"><i class="icon-note" aria-hidden="true"></i></a>
                             &nbsp; 
                            <a href="{{ route('delete-product', $product->id) }}" data-toggle="tooltip" data-placement="left" title="Delete" onclick="return confirm('Are you sure you want to remove this product from the list?')"><i class="icon-trash" aria-hidden="true"></i></a>
                        </td>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->sku }}</td>
                        <td>{{ $product->category->category_name }}</td>
                        <td align="right">$ {{ $product->price }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! csrf_field() !!}
@endsection
