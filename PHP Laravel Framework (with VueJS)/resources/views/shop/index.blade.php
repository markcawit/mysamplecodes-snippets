@extends('layouts.default.master')

@section('pageTitle', 'Snack Shop' . Helpers::breadcrumbs(['shop'=>'Snack Shop'], 'shop'))

@section('content')
<div class="container-fluid">
    <div class="row">
        @foreach($products as $product)
        <div class="col-sm-6 col-md-3">
            <div class="shop-item">
                <div class="shop-item-content">
                    <img src="{{ $product->image }}"/>
                    <br>
                    <div class="clearfix">
                        <div class="pull-left"><h5>{{ $product->product_name }}</h5></div>
                        <div class="pull-right"><h4>$ {{ $product->price }}</h4></div>
                    </div>
                </div>
                <a class="btn btn-primary btn-block"
                    v-link="{ name: 'addtocart', params: { 
                                id: {{$product->id}},
                                name: '{{clean_quotes($product->product_name)}}',
                                price: {{$product->price}} 
                                } 
                            }">
                        <i class="icon-basket-loaded"></i> &nbsp;Add To Cart</a>
            </div>
        </div>
        @endforeach
    </div>
</div>

{!! csrf_field() !!}
@endsection
