@extends('layouts.default.master')

@if($action_name == 'Add')
@section('title', 'Add User | ')
@else
@section('title', 'Edit '. $first_name . ' | ')
@endif
@section('pageTitle', 'User Management')

@section('content')
<style type="text/css">
input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
.uneditable-input {
    padding: 6px 12px;
    min-width: 206px;
    font-size: 14px;
    font-weight: normal;
    height: 34px;
    color: #333;
    background-color: #fff;
    border: 1px solid #e5e5e5;
}
.inline-file
{
    position:absolute;
    top: 0;
    right:0;
    margin-right: 14px;
}
</style>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="">@if($action_name == 'Add')Add User @else Edit {{ $first_name  }} @endif</div>
        </div>
        <div class="panel-body panel-profile">
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ Session::get('message') }}
            </div>
            @elseif(count($errors) > 0)
            @foreach ($errors->all() as $error)
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <li style="list-style:none">{{ $error }}</li>
            </div>
            @endforeach
            @endif
            <form id="user_form" class="" action="{{ $action }}" method="post" class="form idealforms">
                <h4><i class="icon-user"></i>&nbsp;</span>Personal Details</h4>
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-8">
                                    <img src="{{ asset('images/default_user.png') }}" style="width:200px; height:auto; padding-bottom:10px">
                                </div>
                                <label for="exampleInputFile" class="col-md-4">File input</label>
                                <input type="file" id="exampleInputFile" class="col-md-8" style="display:block;">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label">First Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="first_name" placeholder="First Name" class="required prefill" value="{{ $first_name }}"><span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label">Last Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="last_name" placeholder="Last Name" class="required prefill" value="{{ $last_name }}"><span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <h4>Account Login</h4>
                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="email" placeholder="Email" class="required prefill" value="{{ $email }}"><span class="error"></span></br>
                                </div>
                            </div>
                        </div>
                        @if($action_name!="Add")
                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label"><a href="javascript:void(0);" data-toggle="collapse" data-target="#password_field" style="color:blue;">Edit Password</a></label>
                            </div>
                        </div>
                        @endif
                        <div id="password_field" @if($action_name!="Add") class="collapse" @endif>
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label">Password</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="password" type="password" class="required prefill"><span class="error"></span></br>
                                </div>
                            </div>
                            <div class="row">
                                <label for="inputEmail3" class="col-sm-4 control-label">Confirm</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="confirm" type="password" class="required prefill"><span class="error"></span></br>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="clearfix">
                                <div class="pull-right display-flex">
                                    <button type="submit" class="btn btn-primary btn-block">Submit&nbsp;<i class="icon-login"></i></button>
                                    <button type="submit" class="btn btn-danger">Cancel&nbsp;<i class="icon-close"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <!-- <div class="row">
        <div class="col-sm-3">
    </div>
    <div class="col-sm-6">
    <div class="panel panel-primary">
    <div class="panel-heading"><i class="fa fa-user"></i>&nbsp;</span>Personal Details</div>
    <div class="panel-body">
    <fieldset>
    <div class="row">
    <div class="col-sm-3">
    <label class="pull-right">Photo</label>
</div>
<div class="col-sm-4">
<img src="{{ asset('images/default_user.png') }}" style="width:200px; height:auto; padding-bottom:10px">
</div>
<div class="col-sm-5">
</div>
</div>
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-9">
<label for="file-upload">
<button class="btn">Select Image</button>
</label>
<input id="file-upload" type="file"/></br>
</div>
</div>
<div class="row">
<div class="col-sm-3">
<label class="pull-right">First Name</label>
</div>
<div class="col-sm-9">
<input class="form-control" name="first_name" placeholder="First Name" class="required prefill" value="{{ $first_name }}"><span class="error"></span></br>
</div>
</div>
<div class="row">
<div class="col-sm-3">
<label class="pull-right">Last Name</label>
</div>
<div class="col-sm-9">
<input class="form-control" name="last_name" placeholder="Last Name" class="required prefill" value="{{ $last_name }}"><span class="error"></span></br>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<h4><b>Account Login</b></h4>
</div>
</div>
<div class="row">
<div class="col-sm-3">
<label class="pull-right">Email</label>
</div>
<div class="col-sm-9">
<input class="form-control" name="email" placeholder="Email" class="required prefill" value="{{ $email }}"><span class="error"></span></br>
</div>
</div>
@if($action_name!="Add")
<div class="row">
<div class="col-sm-3">
<label class="pull-right">
<a href="javascript:void(0);" data-toggle="collapse" data-target="#password_field">Edit Password</a>
</label>
</div>
<div class="col-sm-9">
&nbsp;
</div>
</div>
@endif
<div id="password_field" @if($action_name!="Add") class="collapse" @endif>
<div class="row">
<div class="col-sm-3">
<label class="pull-right">Password</label>
</div>
<div class="col-sm-9">
<input class="form-control" name="password" type="password" class="required prefill"><span class="error"></span></br>
</div>
</div>
<div class="row">
<div class="col-sm-3">
<label class="pull-right">Confirm</label>
</div>
<div class="col-sm-9">
<input class="form-control" name="confirm" type="password" class="required prefill"><span class="error"></span></br>
</div>
</div>
</div>
</fieldset>
</div>
</div>
{!! csrf_field() !!}
<div class="row">
<div class="col-sm-12">
<input type="submit" class="btn btn-lg btn-primary" style="background-color:#084E9A; margin-left:10px;" value="Submit">
<a href="{{ route('users')}}"><input type="button" class="btn btn-lg btn-danger" value="Cancel"></a>
</div>
</div>
</div>
<div class="col-sm-3">
&nbsp;
</div>
</div> -->
</div>
</div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/lib/jquery.validate.js') }}"></script>
<script src="{{ asset('js/user_form.js') }}"></script>
@stop
