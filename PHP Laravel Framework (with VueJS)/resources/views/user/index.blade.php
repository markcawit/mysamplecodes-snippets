@extends('layouts.default.master')

@section('title', 'Users | ')
@section('pageTitle', 'User Management' . Helpers::breadcrumbs(['users'=>'User Management'], 'users'))

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<a href="#" v-link="#" class="btn btn-primary">
							<i class="icon-plus"></i> Add User
						</a>
					</div>
				</div>
				<div class="col-sm-6">

					<form method="get" action="{{ route('users')}}">
						<div class="input-group">
							<input type="text" class="form-control" name="search" placeholder="Search for..." value="{{ $search }}">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button">Search</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="action-controls"></th>
						<th><a href="{{ route('users').'?page='.$page_number.'&search='.$search.'&order_by=email&sort='.$sort }}">Email</a></th>
						<th><a href="{{ route('users').'?page='.$page_number.'&search='.$search.'&order_by=first_name&sort='.$sort }}">First Name</a></th>
						<th><a href="{{ route('users').'?page='.$page_number.'&search='.$search.'&order_by=last_name&sort='.$sort }}">Last Name</a></th>
						<th><a href="">Position</a></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $row)
					<tr>
						<td>
							<span data-toggle="tooltip" data-placement="top" title="View user">
								<a href = "{{ route('user_show', $row->id) }}">
									<i class="icon-eye"></i>
								</a>
							</span>
							@can('assign_role')
							<span data-toggle="tooltip" data-placement="top" title="Assign role">
								<a title="Assign role to {{ $row->full_name  }}" href="" data-toggle="modal" data-target="#{{ $row->id  }}_role_modal">
									<i class="icon-settings"></i>
								</a>
							</span>
							@endcan
							@can('update_user')
							<span data-toggle="tooltip" data-placement="top" title="Edit user">
								<a title="Edit User {{ $row->full_name  }}" href = "{{ route('user_edit', $row->id) }}">
									<i class="icon-note"></i>
								</a>
							</span>
							@endcan
							@can('delete_user')
							@if(Auth::user()->id != $row->id)
							<span data-toggle="tooltip" data-placement="top" title="Delete user">
								<a title="Delete User {{ $row->full_name  }}" href = "{{ route('user_destroy', $row->id) }}" class="delete_confirmation">
									<i class="icon-trash"></i>
								</a>
							</span>
							@endif
							@endcan
							<!-- Modal -->
							<div id="{{ $row->id  }}_role_modal" class="modal fade" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title"><label for="status">Assign Role</label></h4>
										</div>
										<div class="modal-body">
											<form action="{{ route('user_assign_role', $row->id) }}" method="post">
												<select name="role" id="role" class="form-control">
													<option value="" selected disabled>-Choose Role-</option>
													@foreach(App\Role::all() as $role)
													<option value="{{ $role->id  }}" @if($row->Role()->first()->id == $role->id))) selected @endif>{{ $role->name }}</option>
													@endforeach
												</select>

												<br/>
												<button class="btn btn-primary" type="submit">Submit &nbsp; <i class="icon-check"></i></button>
												{!! csrf_field() !!}
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</td>
						<td>{{ $row->email }}</td>
						<td>{{ $row->first_name  }}</td>
						<td>{{ $row->last_name  }}</td>
						<td>{{ $row->PersonalInfo()->position or "None" }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
				@if($users->count() == 0)
				<div style="text-align:center; color:#333; font-size:18px">No records to show</div>
				@if($search)
				<div style="text-align:center; color:#333; font-size:18px">for {{ $search }}</div>
				@endif
				@endif
			</div>
		</div>
	</div>
	@endsection

	@section('scripts')
	<script src="{{ asset("js/jquery-confirm/js/jquery-confirm.js") }}"></script>
	<script>
	//        $.confirm();
	$('.delete_confirmation').on('click',function(){
		confirm('Are you sure you want to delete this User?');
	});
	</script>
	@stop
