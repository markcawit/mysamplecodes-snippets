@extends('layouts.default.master')

@section('title', 'User Profile | ')
@section('pageTitle', 'User Management')

@section('content')
<div class="container-fluid">
	@include('partials.alert.messages')

	<form action="{{ route('user_update', $user->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<button type="submit" class="btn btn-primary pull-right margin-bottom">Save Changes</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<div class="panel panel-default no-pad auto-height">
				<div class="panel-body">
					<span class="user-status">{{ $user->status  }}</span>
					<input type="hidden" 
								v-model="image" 
								value="{{ (empty($personalInfo->image)) 
									? asset('images/user_default.jpg') 
									: $personalInfo->image }}">
					<div class="user-image">
						<img v-bind:src="image">
						<a href="#" id="upload-photo"><i class="icon-camera"></i></a>
					</div>
					<div class="user-basic-info text-center">
						<input type="file" class="form-control inputForm" style="display:none;" 
                                            v-el:image-input id="imageInput" name="imageInput"
                                            @change="previewThumbnail"
                                            >
						<span class="user-name">{{ $user->first_name." ".$user->last_name }}</span>
						<br>
						<span class="user-email">{{ $user->email }}</span>
					</div>
					{{-- <div class="user-add-info clearfix">
						<div class="row">
							<div class="col-xs-4">
								<span class="user-add-info-item user-current-rate">8000</span><br/>Current Rate
							</div>
							<div class="col-xs-4">
								<span class="user-add-info-item  user-leaves">3 | 12</span><br/>Leaves
							</div>
							<div class="col-xs-4">
								<span class="user-add-info-item user-attendance">119 | 200</span><br/>
								Attendance
							</div>
						</div>
					</div> --}}
					<br>
				</div>
			</div>
		</div>
		<!-- Personal Information -->
		<div class="col-md-4 col-sm-6">
			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<i class="icon-info"></i> Personal Information
				</div>
				<div class="panel-body">
					<ul class="info-list">
						<li><span class="info-meta">First Name</span> 
							<input type="text" class="form-control inline-edit"
								id="first_name" name="first_name" value="{{ $user->first_name  }}"></li>
						<li><span class="info-meta">Last Name</span> 
							<input type="text" class="form-control inline-edit"
								id="last_name" name="last_name" value="{{ $user->last_name  }}"></li>
						<li><span class="info-meta">Age</span> 
							<input type="text" class="form-control inline-edit" placeholder="You Age Here"
								id="age" name="age" value="{{ empty($personalInfo->age) ? '' : $personalInfo->age }}"></li>
						<li><span class="info-meta">Gender</span> 
							<select name="gender" id="gender" class="form-control inline-edit">
								<option value="">Select Gender</option>
								<option value="Male" @if ($personalInfo->gender == 'Male') selected @endif>Male</option>
								<option value="Female" @if ($personalInfo->gender == 'Female') selected @endif>Female</option>
							</select>
						<li><span class="info-meta">Occupation</span>
							<input type="text" class="form-control inline-edit" placeholder="Your Occupation Here"
								id="occupation" name="occupation" value="{{ $personalInfo->occupation }}"></li>
					</ul>
				</div>
			</div>
		</div>
		<!--  Family Information -->
		<div class="col-md-4 col-sm-6">
			<div class="panel panel-default panel-toggle">
				<div class="panel-heading">
					<i class="icon-info"></i> Family Information
				</div>
				<div class="panel-body">
					<ul class="info-list">
						<li><span class="info-meta">Married? </span>
							<select name="married" id="married" class="form-control inline-edit">
								<option value="">Select Option</option>
								<option value="Yes" @if ($personalInfo->married == 'Yes') selected @endif>Yes, I'm married</option>
								<option value="No" @if ($personalInfo->married == 'No') selected @endif>No, I'm single</option>
							</select>
						<li><span class="info-meta">Spouse Name:</span>
							<input type="text" class="form-control inline-edit" placeholder="Your Spouse Name"
								id="spouse_name" name="spouse_name" value="{{ $personalInfo->spouse_name }}"></li>
						<li><span class="info-meta">Spouse Age:</span>
							<input type="text" class="form-control inline-edit" placeholder="Your Spouse Age"
								id="spouse_age" name="spouse_age" value="{{ $personalInfo->spouse_age }}"></li>
						<li><span class="info-meta">Children</span>
							<input type="text" class="form-control inline-edit" placeholder="Number of Children"
								id="children" name="children" value="{{ $personalInfo->children }}"></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>


@endsection
